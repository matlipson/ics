    Title:      README
    Project:    ICS01 - Interface Conduction Scheme - Submission 01
    Purpose:    Describes supplementary material for the paper
    Manuscript: Efficiently modelling urban heat storage: an interface conduction scheme in an urban land surface model (aTEB v2.0)
    Authors:    Mathew Lipson, Melissa Hart, Marcus Thatcher
    Journal:    Geoscientific Model Development
    Revision:   12 January 2017
    Developer:  Mathew Lipson <m.lipson@unsw.edu.au> (unless otherwise specified)

Supplementary material includes code to run idealised conduction studies, the urban model aTEB, and analysis/ plotting code.

We include the evaluated conduction schemes in three forms:
- as standalone code with sinusoidal forcing in Fortran (conduction.f90)
- as standalone code with sinusoidal forcing in Python to streamline the plotting process (functions.py)
- within an urban land surface model in Fortran (ateb.f90)

Supplementary material is structured as follows:
--------------------------------------------------------------------------------------------------------------------------------
conduction/ contains analysis Python 3 code. Files are:
    ICS01-idealised.py:         
        Runs idealised evaluation code (Section 3.1).
        Uses conduction/analysis functions in 'functions.py', and assembly characteristics from 'materials_interface.py'
        Includes plotting code for Figures 2, 3, 4, 5 and D1.
    ICS01-observation.py:       
        Runs observation evaluation code (Section 3.2), conforming to PILPS-Urban methodology (Grimmond et al., 2011)
        Includes plotting code for Figure 6, 7 and Table 2 and 3.
        For analysis, observational and anthropogenic data must be acquired from owner Andrew Coutts <andrew.coutts@monash.edu>. 
        Required files are:
        - alpha01.dat (used as forcing data for the urban model, and observation quality flags)
        - observation_preston.csv (flux observations to assess model performance)
        - QF.txt (estimates of anthropogenic heat sources)
    functions.py:               
        Module for shared functions used in ICS01-observation.py and ICS01-idealised.py
        Includes isolated interface and conduction schemes run with idealised forcing.
    materials_interface.py:     
        Assemblies used for idealised evaluation.
        Includes 32x9=288 wall/roof representations based on the CLMU database (Jackson et al., 2010)
        Also includes 8 walls/roofs from other databases later used in observational evaluation.
    taylordiagram.py
        Module for plotting Taylor Diagram (Fig. 7) written by Yannick Copin <yannick.copin@laposte.net>

conduction/fortran/ contains Fortran 90 code and output for the two discrete schemes at high resolution for Table 1.
    conduction.f90:
        fortran code equivalent to python code in functions module
    half_*sec.dat/ interface_*sec.dat:
        pre-calculated output of fortran code for high temporal and spatial resolution runs, output at 30 minute intervals.

--------------------------------------------------------------------------------------------------------------------------------
ateb/ contains the urban land surface model aTEB, developed by Marcus Thatcher <Marcus.Thatcher@csiro.au>. 
Within the directory, use ‘make’ to compile the urban model with ifort, then the shell script ‘run_paper.sh’ to run experiments.
Key files include:
    ateb.f90:       source code for aTEB
    atebwrap.f90:   wrapper for aTEB allowing offline simulation and writing output files (use run_paper.sh)
    pres_*.nml:     namelist files for four material databases. Includes site information from stage 4 of PILPS-Urban.
    run_paper.sh:   shell script wrapper for running 24 experiments where:
                        DATA=pres_aTEBds for aTEB default materials (from Thatcher and Hurley, 2012)
                        DATA=pres_WRFds for WRF/urban low intensity residental default materials (from Loridan and Grimmond, 2012)
                        DATA=pres_UZEds for UZE medium intensity residental default materials (from Loridan and Grimmond, 2012)
                        DATA=pres_SITEds for Melbourne site materials (from Grimmond et al., 2011)
                        RM=0 is Rowley resistance method for canyon aerodynamic heat transfer
                        RM=1 is Harman resistance method for canyon aerodynamic heat transfer
                        RM=2 is Jurges resistance method for canyon aerodynamic heat transfer
                        CM=0 is Half-layer conduction method
                        CM=1 is Interface conduction method
                    run_paper makes changes to relevant namelist file using sed
                    then runs atebwrap and copies output files to relevant directories where:
                        out/ saves flux simulation output
                        nml/ saves copy of namelist file used
                        energy/ saves storage, anthropogenic heat fluxes and any non-closure of energy budget
                        variables/ saves other output from ateb.f90
    other files:    other files not listed are needed to run aTEB through the framework of
                    Australian Community Atmosphere Biosphere Land Exchange (CABLE) model.
    The site observational forcing file alpha04.dat has NOT been included as we don't have authorisation to distribute it.
    alpha04.dat may be sourced from Andrew Coutts <andrew.coutts@monash.edu>

ateb/out/ contains an archive of 24 pre-simulated flux outputs of aTEB used in the study. It is unzipped with analysis code.
ateb/energy/ contains an archive of 24 pre-simulated energy closure calculations and anthropogenic inputs.
ateb/nml for saving namelists for each simulation of aTEB
ateb/variables for saving other variable output for each simulation of aTEB

--------------------------------------------------------------------------------------------------------------------------------
figures/ output folder for plotted figures

figures/pickle/ contains pre-calculated output for 288 assemblies used in ICS01-idealised.py






