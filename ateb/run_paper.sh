#!/bin/bash
# Wrapper for aTEB: Interface Conduction Manuscript 
# This script runs 24 x urban land surface experiments used in the paper.
# it first asks for experiment prefix
# it then cycles through material database experiments, copying relevant nml to ateb.nml 
# it then uses sed to update parameters in the namelist file
# it then runs aTEB with the current parameter namelist ateb.nml
# it then adds git log info to output files (if within a repository)
# It then archives the namelist (nml), output (out), variables and energy files

# For sed: looping for series through sed (in double quotes for bash variable)
# where -i pushes to input file; /.... =/finds line that contain this string
# s/OLD/NEW subsitution with .*wildcard
# eg for i in 0.10 0.20 0.30 0.40; do sed -i "/croofemiss =/ s/=.....,/= $i,/" ateb.nml; done
	
mkdir -p out
mkdir -p nml
mkdir -p energy
mkdir -p variables

if [ ! -e "alpha04.dat" ]; then
	echo "alpha04.dat not found. Contact forcing data owner <andrew.coutts@monash.edu>"
	exit
fi

ZC=0.01
ZR=0.100
ZV=0.250

# echo "Enter experiment prefix (eg ICS01-V28):"
# read EXPID
EXPID=ICS01-V28

for DATA in pres_aTEBds pres_WRFds pres_UZEds pres_SITEds 
do
	cp ${DATA}.nml ateb.nml
	for RM in 0 1 2
	do
		sed -i "/resmeth = / s/= ./= ${RM}/" ateb.nml
		for CM in 0 1
		do

			sed -i "/conductmeth = / s/= ./= ${CM}/" ateb.nml
			EXP=${EXPID}_${DATA}_su0_cm${CM}_rm${RM}_zm0_zc${ZC}_zr${ZR}_zv${ZV}
			echo "EXP == $EXP"

			./atebwrap > variables.out
			echo "Now copying ateb.nml to nml/$EXP.nml"
			cp ateb.nml nml/$EXP.nml
			
			# add git log info to .dat file
			if git rev-parse --is-inside-work-tree >/dev/null; then
				echo "-----------------git build version-------------------" > out/$EXP.dat
				git log --date=short --max-count=1 >> out/$EXP.dat
		
				# add diff information, piped through grep to only include + or - lines
				echo '    with the following DIFF to ateb.f90:' >> out/$EXP.dat
				echo '' >> out/$EXP.dat
				git diff --ignore-space-at-eol ateb.f90 | grep '^+\|^-' >> out/$EXP.dat
				echo '-----------------------------------------------------' >> out/$EXP.dat
			else
				echo 'no git version information - not in a repository' >  out/$EXP.dat
			fi

			echo "Now copying flux output data to out/$EXP.dat"
			cat atebout.dat >> out/$EXP.dat
			# insert (i) comment character (#) start of each line (^) until pattern 'Year' reached
			sed -i '1,/Year/ s/^/# /' out/$EXP.dat
			sed -i '1,/Year/ s/# Year/ Year/' out/$EXP.dat
			sed -i '/^     /d' out/$EXP.dat
		
			echo "Now copying energyout.dat to energy/$EXP.dat"
			mv energyout.dat energy/$EXP.dat
			echo "Now copying variables.out to variables/$EXP.var"
			mv variables.out variables/$EXP.var
		done
	done
done
echo "Done!"
exit
