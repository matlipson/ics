program atebwrap

use ateb
use cable_air_module
use cable_albedo_module
use cable_canopy_module
use cable_carbon_module
use cable_common_module
use cable_data_module
use cable_def_types_mod
use cable_radiation_module
use cable_roughness_module
use cable_soil_snow_module
use zenith_m

implicit none

integer i,ierr,iyr,iday,itime,k,diag
real dectime,swdown,lwdown,windv,windu,psurf,tair
real qair,rainf,dt,vmodmin,bpyear
real swup,lwup,fgup,egup,fjd,slag,dlt,dhr,r1,alp
real rnet
real :: start_time, stop_time
real, dimension(1) :: storagetot,atmoserr,atmoserr_bias,surferr,surferr_bias,bldheat,bldcool,traf
real, dimension(1) :: sigmu,fg,eg,tss,wetfac,ps,alb
real, dimension(1) :: newrunoff,t,qg,sg,azmin,rg,rnd,snd
real, dimension(1) :: rho,uzon,vmer,coszro2,rlongg,rlatt,taudar2
real, dimension(1) :: fbeam
character(len=80) dum

real, parameter :: sbconst=5.67e-8    ! Stefan-Boltzmann constant
real, parameter :: soil_albedo = 0.2

logical :: need_cable=.FALSE.

type (air_type), save :: air
type (bgc_pool_type), save :: bgc
type (met_type), save :: met
type (balances_type), save :: bal
type (radiation_type), save :: rad
type (roughness_type), save :: rough
type (soil_parameter_type), save :: soil
type (soil_snow_type), save :: ssnow
type (sum_flux_type), save :: sum_flux
type (veg_parameter_type), save :: veg
type (canopy_type), save :: canopy
type (physical_constants), save :: c

diag=2                          ! diag = diagnostic message mode (0=off, 1=basic, 2=detailed, etc)
sigmu=1.                        ! fraction of urban
dt=30.*60.                      ! timestep (sec)
azmin=40.                       ! height to met data (assume 2m)
vmodmin=0.1                     ! min wind speed
rlongg(1)=144.97*3.1415927/180. ! lon
rlatt(1)=-37.81*3.1415927/180.  ! lat
dhr = dt/3600.                  ! timestep in hours (for zenith_m)
bpyear=0.                       ! zenith_m mode 0=present day

call cpu_time(start_time)
! initialise urban model
call atebinit(1,sigmu,diag)

mp=1
call alloc_cbm_var(air, mp)
call alloc_cbm_var(bgc, mp)
call alloc_cbm_var(canopy, mp)
call alloc_cbm_var(met, mp)
call alloc_cbm_var(bal, mp)
call alloc_cbm_var(rad, mp)
call alloc_cbm_var(rough, mp)
call alloc_cbm_var(soil, mp)
call alloc_cbm_var(ssnow, mp)
call alloc_cbm_var(sum_flux, mp)
call alloc_cbm_var(veg, mp)

! Cable configuration
cable_user%ssnow_POTEV = ""
cable_user%fwsoil_switch="standard"
kwidth_gl=nint(dt)
  
! soil parameters
soil%zse        = (/.022, .058, .154, .409, 1.085, 2.872 /) ! soil layer thickness
soil%zshh(1)    = 0.5 * soil%zse(1)
soil%zshh(6+1) = 0.5 * soil%zse(6)
soil%zshh(2:6) = 0.5 * (soil%zse(1:6-1) + soil%zse(2:6))

! CABLE veg data (bare soil)
veg%meth      = 1
veg%hc        = 0.2
veg%canst1    = 0.1
veg%ejmax     = 2.*17.E-6
veg%tminvj    = -15.
veg%tmaxvj    = -10.
veg%vbeta     = 1.
veg%rp20      = 1.
veg%rpcoef    = 0.0832
veg%shelrb    = 2.
veg%vcmax     = 17.E-6
veg%xfang     = 0.
veg%dleaf     = 0.
veg%xalbnir   = 1. ! not used
veg%taul(:,1) = 0.029
veg%taul(:,2) = 0.126
veg%refl(:,1) = 0.175
veg%refl(:,2) = 0.336
veg%extkn     = 0.001 ! new definition for nitrogen (since CABLE v1.9b)
veg%rs20      = 0.
veg%vegcf     = 1.
veg%frac4     = 0.
veg%froot(:,1)=0.05
veg%froot(:,2)=0.20
veg%froot(:,3)=0.20
veg%froot(:,4)=0.20
veg%froot(:,5)=0.20
veg%froot(:,6)=0.15

! Load CABLE soil data (coarse-medium-fine sandy-clay-loam)
soil%bch     = 7.12
soil%css     = 850.
soil%rhosoil = 2600.
soil%clay    = 0.27
soil%sand    = 0.58
soil%silt    = 0.15
soil%cnsd    = 0.3*soil%sand+0.25*soil%clay+0.265*soil%silt 
soil%hyds    = 6.e-6
soil%sucs    = -.299
soil%hsbh    = soil%hyds*abs(soil%sucs)*soil%bch
soil%sfc     = .255
soil%ssat    = .420
soil%swilt   = .175
soil%ibp2    = nint(soil%bch)+2
soil%i2bp3   = 2*nint(soil%bch)+3
soil%pwb_min = (soil%swilt/soil%ssat)**soil%ibp2
bgc%ratecp(:) = (/ 1., 0.03, 0.14 /)
bgc%ratecs(:) = (/ 2., 0.5 /)

! store bare soil albedo and define snow free albedo
soil%albsoil(:,1)=soil_albedo
soil%albsoil(:,2)=soil_albedo
soil%albsoil(:,3)=0.05
    
ssnow%albsoilsn=soil%albsoil
ssnow%t_snwlr=0.05
ssnow%pudsmx=0.
rad%albedo_T=soil_albedo
rad%trad=293.16
rad%latitude=rlatt(1)*180./3.1415927
rad%longitude=rlongg(1)*180./3.1415927

canopy%oldcansto=0.  
canopy%ghflux=0.
canopy%sghflux=0.
canopy%ga=0.
canopy%dgdtg=0.
canopy%fhs_cor=0.
canopy%fes_cor=0.
canopy%ga=0.
canopy%dgdtg=0.
canopy%us=0.01
ssnow%wb_lake=0. ! not used when mlo.f90 is active
ssnow%fland=1.
ssnow%ifland=soil%isoilm
 
! Initialise sum flux variables
sum_flux%sumpn=0.
sum_flux%sumrp=0.
sum_flux%sumrs=0.
sum_flux%sumrd=0.
sum_flux%sumrpw=0.
sum_flux%sumrpr=0.
sum_flux%dsumpn=0.
sum_flux%dsumrp=0.
sum_flux%dsumrs=0.
sum_flux%dsumrd=0.
 
bal%evap_tot=0.
bal%precip_tot=0.
bal%ebal_tot=0.
bal%rnoff_tot=0.

do k=1,6
  ssnow%tgg(:,k)   = 293.
  ssnow%wb(:,k)    = 0.5*(soil%swilt+soil%sfc)
  ssnow%wbice(:,k) = 0.
end do
ssnow%tggsn = 273.
ssnow%smass = 0.
ssnow%ssdn  = 140.
ssnow%isflag=0
ssnow%snowd=0.
ssnow%snage=0.
ssnow%rtsoil=50.
canopy%cansto=0.
canopy%us=0.01
ssnow%pudsto=0.
ssnow%wetfac=0.
bgc%cplant = 0.
bgc%csoil = 0.

ssnow%osnowd=ssnow%snowd
bal%osnowd0=ssnow%snowd 
ssnow%owetfac=ssnow%wetfac
ssnow%wbtot=0.
ssnow%wbtot1=0.
ssnow%wbtot2=0.
ssnow%tggav=0.
do k = 1,6
    ssnow%gammzz(:,k)=max((1.-soil%ssat)*soil%css* soil%rhosoil &
       & +real(ssnow%wb(:,k)-ssnow%wbice(:,k))*4.218e3* 1000.       &
       & +real(ssnow%wbice(:,k))*2.100e3*1000.*0.9,soil%css*soil%rhosoil)*soil%zse(k)
end do
bal%wbtot0=ssnow%wbtot



! open files for input and output
open(1,file="alpha04.dat")
open(2,file="atebout.dat")
open(3,file="cableout.dat")
open(4,file="energyout.dat")

do i=1,7
  read(1,*) dum
end do
write(2,'(A89)') "Year  Dectime  Day  Time     NetRad    SWup      LWup      QHup      QEup   SWdown LWdown"
write(2,*)       "                             W/m2      W/m2      W/m2      W/m2      W/m2"

write(3,*) "Year  Dectime  Day  Time     NetRad    SWup      LWup      QHup      QEup"
write(3,*) "                             W/m2      W/m2      W/m2      W/m2      W/m2"

write(4,'(A102)') "Year  Dectime  Day  Time  Storage       Atmos   Surf.     Atmos   Surf.    Heating   Cooling   Traffic "
write(4,'(A102)') "                          W/m2          error   error     bias    bias     W/m2      W/m2      W/m2    "


ierr=0

do while (ierr==0)

  read(1,*,iostat=ierr) iyr,dectime,iday,itime,swdown,lwdown,windv,windu,psurf,tair, &
                        qair,rainf

  if (ierr==0) then

    ! write(6,*) iyr,dectime,swdown,rainf !mjl
    
    ! set-up solar angles
    fjd=dectime-rlongg(1)*0.5/3.1415927
    call solargh(fjd,bpyear,r1,dlt,alp,slag)
    call zenith(fjd,r1,dlt,slag,rlatt,rlongg,dhr,1,coszro2,taudar2)
    call atebccangle(1,1,coszro2,rlongg,rlatt,fjd,slag,dt,sin(dlt))
    
    ! Extract albedo
    call atebalb1(1,1,alb,diag,raw=.true.,split=0)
    
    ! Update urban prognostic variables
    t(1)=tair     ! air temperature
    qg(1)=qair    ! mixing ratio of water vapour
    ps(1)=psurf   ! surface pressure
    sg(1)=swdown  ! downwelling shortwave
    rg(1)=lwdown  ! downwelling longwave
    rnd(1)=rainf  ! rainfall
    snd(1)=0.     ! snowfall
    rho(1)=1.     ! air density
    uzon(1)=windu ! wind U
    vmer(1)=windv ! wind V
    call atebcalc(fg,eg,tss,wetfac,newrunoff,dt,azmin,sg,rg, &
                  rnd,snd,rho,t,qg,ps,uzon,vmer,vmodmin,diag)

    ! store flux data                  
    fgup=fg(1)
    egup=eg(1)
    lwup=sbconst*tss(1)**4
    swup=swdown*alb(1)
    rnet=swdown-swup+lwdown-lwup

    write(2,'(I5,F9.4,I5,I5,5F10.4,F9.2,F7.2)') iyr,dectime,iday,itime,rnet,swup,lwup,fgup,egup,swdown,lwdown

    ! store energy flux data
    call energyrecord(storagetot,atmoserr,atmoserr_bias,surferr,surferr_bias,bldheat,bldcool,traf)
    write(4,'(I5,F9.4,I5,I5,ES14.5,2F9.5,2F9.2,2F10.4,F8.4)') iyr,dectime,iday,itime,storagetot,atmoserr,surferr,atmoserr_bias,surferr_bias,bldheat,bldcool,traf

call spitter(1,fjd, coszro2, swdown,fbeam)

met%doy         =fjd
met%tk          =tair 
met%tvair       =met%tk
met%tvrad       =met%tk
met%ua          =sqrt(windu*windu+windv*windv)
met%ua          =max(met%ua,c%umin)
met%ca          =1.e-6*330.
met%coszen      =max(1.e-8,coszro2) ! use instantaneous value
met%qv          =qair               ! specific humidity in kg/kg
met%pmb         =0.01*psurf         ! pressure in mb at ref height
met%precip      =rainf*dt 	    ! in mm not mm/sec
met%precip_sn   =0.                 ! in mm not mm/sec
met%hod         =real(itime/100)+real(mod(itime,100))/60.
met%hod         =mod(met%hod,24.)
rough%za_tq     =azmin ! reference height
rough%za_uv     =azmin
met%fsd(:,1)    =0.5*swdown ! VIS
met%fsd(:,2)    =0.5*swdown ! NIR
rad%fbeam(:,1)  =fbeam
rad%fbeam(:,2)  =fbeam
rad%fbeam(:,3)  =0.     ! dummy for now
met%fld         =lwdown ! long wave down (positive) W/m^2
rough%hruff     =max(0.01,veg%hc-1.2*ssnow%snowd/max(ssnow%ssdnn,100.))
rough%hruff_grmx=rough%hruff ! Does nothing in CABLE v2.0
veg%vlai(:)=0.

!--------------------------------------------------------------
! CABLE
IF (need_cable) THEN

  ktau_gl          = 900
  kend_gl          = 999
  ssnow%owetfac    = ssnow%wetfac
  canopy%oldcansto = canopy%cansto
  call ruff_resist(veg,rough,ssnow,canopy)
  call define_air(met,air)
  call init_radiation(met,rad,veg,canopy)
  call surface_albedo(ssnow,veg,met,rad,soil,canopy)
  call define_canopy(bal,rad,rough,air,met,dt,ssnow,soil,veg,canopy)
  ssnow%otss_0  = ssnow%otss
  ssnow%otss    = ssnow%tss
  ssnow%owetfac = ssnow%wetfac
  call soil_snow(dt,soil,ssnow,canopy,met,bal,veg)

END IF
! adjust for new soil temperature
ssnow%deltss   = ssnow%tss - ssnow%otss
canopy%fhs     = canopy%fhs + ssnow%deltss*ssnow%dfh_dtg
canopy%fes     = canopy%fes + ssnow%deltss*ssnow%cls*ssnow%dfe_ddq*ssnow%ddq_dtg
canopy%fhs_cor = canopy%fhs_cor + ssnow%deltss*ssnow%dfh_dtg
canopy%fes_cor = canopy%fes_cor + ssnow%deltss*ssnow%cls*ssnow%dfe_ddq*ssnow%ddq_dtg
canopy%fh      = canopy%fhv + canopy%fhs
canopy%fev     = canopy%fevc + canopy%fevw
canopy%fe      = canopy%fev + canopy%fes
canopy%rnet    = canopy%fns + canopy%fnv
rad%trad       = ( (1.-rad%transd)*canopy%tv**4 + rad%transd*ssnow%tss**4 )**0.25

! EK suggestion
!canopy%cdtq =  max( 0.1*canopy%cduv, canopy%cdtq )
! MJT suggestion
canopy%cdtq =  max( 0., canopy%cdtq )


    ! store flux data                  
    fgup=canopy%fh(1)
    egup=canopy%fe(1)
    lwup=sbconst*rad%trad(1)**4
    swup=swdown*(0.5*rad%albedo(1,1)+0.5*rad%albedo(1,2))
    rnet=swdown-swup+lwdown-lwup

    write(3,'(I5,F9.4,I5,I5,5F10.4)') iyr,dectime,iday,itime,rnet,swup,lwup,fgup,egup

  end if
end do

close(1)
close(2)
close(3)
close(4)

call cpu_time(stop_time)
print *, "Time:", stop_time - start_time, "seconds"

stop
end

      !--------------------------------------------------------------
      ! from CABLE code 1.4
      subroutine spitter(mp,doy, coszen, fsd,fbeam)
      ! Calculate beam fraction
      ! See spitters et al. 1986, agric. for meteorol., 38:217-229
      integer, intent(in) :: mp
      REAL, INTENT(IN) :: doy ! day of year
      REAL, DIMENSION(mp), INTENT(IN) :: coszen ! cos(zenith angle of sun)
      REAL, DIMENSION(mp), INTENT(IN) :: fsd	! short wave down (positive) w/m^2
      REAL, DIMENSION(mp), intent(out) :: fbeam	! beam fraction (result)
      REAL, PARAMETER :: solcon = 1370.0
      REAL, DIMENSION(mp) :: tmpr !
      REAL, DIMENSION(mp) :: tmpk !
      REAL, DIMENSION(mp) :: tmprat !
      real, parameter :: two_pi = 2. * 3.1415927
      fbeam = 0.0
      tmpr = 0.847 + coszen * (1.04 * coszen - 1.61)
      tmpk = (1.47 - tmpr) / 1.66
      WHERE (coszen > 1.0e-10 .AND. fsd > 10.0)
       tmprat = fsd / (solcon * (1.0 + 0.033 * COS(two_pi * (doy-10.0) / 365.0)) * coszen)
      ELSEWHERE
       tmprat = 0.0
      END WHERE
      WHERE (tmprat > tmpk)
        fbeam = MAX(1.0 - tmpr, 0.0)
      ELSEWHERE (tmprat > 0.35)
        fbeam = MIN(1.66 * tmprat - 0.4728, 1.0)
      ELSEWHERE (tmprat > 0.22)
        fbeam = 6.4 * (tmprat - 0.22) ** 2
      ELSEWHERE
        fbeam = 0.
      END WHERE
      
      END subroutine spitter
