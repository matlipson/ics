!!!!!!!!!!!!!!! conduction.f90 for Fortran 90 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Purpose:      For comparing discrete conduction schemes for use in aTEB urban scheme (Thatcher and Hurley, 2012)
! Developer:    Mathew Lipson <m.lipson@unsw.edu.au>
! Manuscript:   Efficiently modelling urban heat storage: an interface conduction scheme in the aTEB urban land surface model
! Journal:      Geophysical Model Development
! Revision:     18 August 2016
! Instructions: Use to simulate half-layer and interface schemes at high spatial/ temporal resolution.
!               This code is faster than the python code, but output is equivalent.
!               Therefore it was used for data in Table 1 (1mm layers).
!               User input required for timestep, 1 sec, 60 sec and 1800 sec.
!               Current setup has 10 dummy loops for runtime of schemes before output
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

program conduction
implicit none

integer, parameter :: dp = SELECTED_REAL_KIND(8)
! derived type of material properties
type materialdata
    real(kind=dp), dimension(:), allocatable :: depth, heatcp, lambda, temp_int, temp_half
end type materialdata

! constants
real(kind=dp) :: ddt=1.                     ! timestep length [s], updated by user
real, parameter :: period = 60.*60*24       ! forcing period [s] (default is one day)
real, parameter :: spinup = 5.              ! number of days spin up for discrete models
real, parameter :: T0 = 290.                ! initial temperature [K]
real, parameter :: T_int = 290.             ! internal temperature [K]
real(kind=dp), parameter :: r_se = 0.04     ! external surface res [m^2K/W] Default 0.04 per ISO6946 horizontal
real(kind=dp), parameter :: r_si = 0.13     ! internal surface res [m^2K/W] Default 0.13 per ISO6946 horizontal
integer :: layers = 200                     ! number of layers in material (default 200)
integer :: timesteps=1                      ! total timesteps in simulation, updated based on ddt
real(kind=dp), parameter :: pi = 4 * atan (1.0_8)       ! pi double precision
real(kind=dp), save :: storage_prev_half                ! heat storage of previous timestep for closure calculation
real(kind=dp), save :: storage_prev_interface           ! heat storage of previous timestep for closure calculation
integer :: t, k                                         ! loop integers
real(kind=dp) :: T_ext                      ! sinusoidally varying external temperature
real :: start_time, stop_time               ! timer testing
real(kind=dp) :: g_flux                     ! heat storage flux  
character(len=25) :: filename               ! variable filename
character(len=8) :: dtchar                  ! variable timestep name

type(materialdata), save :: material

!!!!!!!!!! user input !!!!!!!!!!!
! print *, "Enter number of layers [4 or 200]: "
! read *,layers
print *, "Layers in material: ", layers
print *, "Enter timesteps in seconds: "
read *,ddt
! convert and adjust ddt for use in filename
write(dtchar, '(I4)') int(ddt)
dtchar = adjustl(dtchar)
print *, "Timestep: [s] ",dtchar
timesteps = (spinup+1)*(24*3600)/ddt

!!!!!!!!!! allocation !!!!!!!!!!!
allocate(material%depth(layers))
allocate(material%heatcp(layers))
allocate(material%lambda(layers))

if (layers==4) then
    ! 4 layered wall based on aTEB default wall parameters (to compare with python output)
    material%depth=(/ 0.01, 0.04, 0.10, 0.05 /)
    material%heatcp=(/ 1.55E6, 1.55E6, 1.55E6, 0.29E6 /)
    material%lambda=(/ 0.9338, 0.9338, 0.9338, 0.050 /)
else if (layers==200) then
    ! ! 200 x 1mm layered wall based on aTEB default wall parameters
    do k = 1,10
        material%depth(k)   = 0.001
        material%heatcp(k)  = 1.55E6
        material%lambda(k)  = 0.9338
    end do
    do k = 11,50
        material%depth(k)   = 0.001
        material%heatcp(k)  = 1.55E6
        material%lambda(k)  = 0.9338
    end do
    do k = 51,150
        material%depth(k)   = 0.001
        material%heatcp(k)  = 1.55E6
        material%lambda(k)  = 0.9338
    end do
    do k = 151,200
        material%depth(k)   = 0.001
        material%heatcp(k)  = 0.29E6
        material%lambda(k)  = 0.050
    end do
else
    write(6,*) "ERROR: input layers must be 4 or 200."
    stop
end if

!!!!!!!!!! main program !!!!!!!!!!!

!!!!!!!!! Time test !!!!!!!!!!!!
call cpu_time(start_time)
do k = 1, 10
! print *, "Loop: ", k
    do t = 1, timesteps
        call external_forcing(T_ext, ddt, period, pi, t)
        call half_rs(material%depth, material%heatcp, material%lambda, T_ext, T_int, T0, ddt, g_flux)
    end do
end do
call cpu_time(stop_time)
print *, "Time half_rs:", stop_time - start_time, "seconds"
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
write(filename, '(A,A,A)') 'half_',trim(dtchar),'sec.dat'
print *, 'output: ',filename
open(1,file=filename,action="write",status="replace")
write(1,*) "Timestep    Ext_T            G_sum"
! write(1,*) "Timestep    Ext_T    T1     T2"   ! for boundary temperature test
do t = 1, timesteps
    call external_forcing(T_ext, ddt, period, pi, t)
    call half_rs(material%depth, material%heatcp, material%lambda, T_ext, T_int, T0, ddt, g_flux)
    ! output every 30 minutes (1800s)
    if ((ddt<1800) .and. (t>=spinup*period/ddt) .and. MOD(t,1800/int(ddt))==0) write(1,'(I8,F15.8,F15.8)') t, T_ext, g_flux
    if ((ddt>=1800) .and. (t>=spinup*period/ddt)) write(1,'(I8,F15.8,F15.8)') t, T_ext, g_flux
    !!!!!!!!!!!!! for boundary temperature test !!!!!!!!!!!!!!!
    ! if ((t>=spinup*period/ddt) .and. MOD(t,1800/int(ddt))==0) write(1,'(I8,F15.8,2F15.8)') &
    !       t, T_ext, material%temp_half(1), material%temp_half(ubound(material%temp_half))
end do
close(1)

!!!!!!!!! Time test !!!!!!!!!!!!
call cpu_time(start_time)
do k = 1, 10
! print *, "Loop: ", k
    do t = 1, timesteps
        call external_forcing(T_ext, ddt, period, pi, t)
        call interface_rs(material%depth, material%heatcp, material%lambda, T_ext, T_int, T0, ddt, g_flux)
    end do
end do
call cpu_time(stop_time)
print *, "Time interface_rs:", stop_time - start_time, "seconds"
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
write(filename, '(A,A,A)') 'interface_',trim(dtchar),'sec.dat'
print *, 'output: ',filename
open(1,file=filename,action="write",status="replace")
write(1,*) "Timestep    Ext_T            G_sum"
! write(1,*) "Timestep    Ext_T    T1     T2"   ! for boundary temperature test
do t = 1, timesteps
    call external_forcing(T_ext, ddt, period, pi, t)
    call interface_rs(material%depth, material%heatcp, material%lambda, T_ext, T_int, T0, ddt, g_flux)
    if ((ddt<1800) .and. (t>=spinup*period/ddt) .and. MOD(t,1800/int(ddt))==0) write(1,'(I8,F15.8,F15.8)') t, T_ext, g_flux
    if ((ddt>=1800) .and. (t>=spinup*period/ddt)) write(1,'(I8,F15.8,F15.8)') t, T_ext, g_flux
    !!!!!!!!!!!!! for boundary temperature test !!!!!!!!!!!!!!!
    ! if ((t>=spinup*period/ddt) .and. MOD(t,1800/int(ddt))==0) write(1,'(I8,F15.8,2F15.8)') & 
    !       t, T_ext, material%temp_int(0), material%temp_int(ubound(material%temp_int))
end do
close(1)

deallocate(material%temp_int,material%temp_half)

contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine external_forcing(T_ext, ddt, period, pi, t)
! Sets external temperature forcing into T_ext array
implicit none

integer, intent(in) :: t
real, intent(in) :: period
real(kind=dp), intent(in) :: pi 
real(kind=dp), intent(in) :: ddt
real(kind=dp), intent(out) :: T_ext

T_ext = T0 + sin(2*pi*t*ddt/period)

return
end subroutine external_forcing

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine half_rs(depth, heatcp, lambda, T_ext, T_int, T0, ddt, g_flux)
! half-layer conduction scheme
implicit none

real(kind=dp), intent(in) :: depth(:), heatcp(:), lambda(:), ddt, T_ext
real, intent(in) :: T_int, T0
real(kind=dp), dimension(:), allocatable, save :: gga, ggb, ggc, ggd, ggb_init, res
real(kind=dp) :: storage, h_flux, conserve_e, Gext, Gint
real(kind=dp), intent(out) :: g_flux
real(kind=dp) m
integer k, n
logical, save :: init = .TRUE.

n = size(depth)

if (init) then
    print *, "Initial call of subroutine 'half_rs'."
    allocate(material%temp_half(n))
    material%temp_half(:)=T0
    ! Conservation of energy check                          
    storage_prev_half = 0  
    do k=1,n 
        storage_prev_half = storage_prev_half +material%temp_half(k)*depth(k)*heatcp(k)
    end do       

    allocate(gga(n),ggb(n),ggc(n),ggd(n),ggb_init(n),res(n))
    do k=1,n
        res(k)=depth(k)/lambda(k)
    end do
    do k=2,n
      gga(k)=-2./(res(k-1)+res(k))
    end do
    ggb(1)=2./(res(1)+res(2)) +heatcp(1)*depth(1)/ddt +2./(res(1) +2*r_se)
    do k=2,n-1
      ggb(k)=2./(res(k-1)+res(k)) +2./(res(k)+res(k+1)) +heatcp(k)*depth(k)/ddt
    end do
    ggb(n)=2./(res(n-1)+res(n)) +heatcp(n)*depth(n)/ddt +2./(res(n) +2*r_si)
    do k=1,n-1
      ggc(k)=-2./(res(k)+res(k+1))
    end do
    ggb_init(:) = ggb(:)
    init = .FALSE.
end if
! update T_ext for timesteps > 1
ggb(:) = ggb_init(:)
ggd(1)=material%temp_half(1)*heatcp(1)*depth(1)/ddt + 2*T_ext/(res(1) +2*r_se)
do k=2,n-1
ggd(k)=material%temp_half(k)*heatcp(k)*depth(k)/ddt
end do
ggd(n)=material%temp_half(n)*heatcp(n)*depth(n)/ddt + 2*T_int/(res(n) +2*r_si)
! tridiagonal solver (Thomas algorithm) to solve for roof, road and wall temperatures
do k=2,n
    m=gga(k)/ggb(k-1)
    ggb(k)=ggb(k)-m*ggc(k-1)
    ggd(k)=ggd(k)-m*ggd(k-1)
end do
material%temp_half(n)=ggd(n)/ggb(n)
do k=n-1,1,-1
    material%temp_half(k)=(ggd(k)-ggc(k)*material%temp_half(k+1))/ggb(k)
end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Conservation of energy check                                              !
storage = 0                                                                 !
do k=1,n                                                                    !
    storage = storage +material%temp_half(k)*depth(k)*heatcp(k)             !
end do                                                                      !
h_flux = (storage - storage_prev_half)/ddt                                  !
Gext = (T_ext - material%temp_half(1))/(0.5*depth(1)/lambda(1) + r_se)      !
Gint = (material%temp_half(n) - T_int)/(0.5*depth(n)/lambda(n) + r_si)      !
g_flux  = Gext - Gint                                                       !
conserve_e = h_flux-g_flux                                                  !    
if (conserve_e<-0.00001 .or. conserve_e>0.00001) then                       !
    print *, "Warning, half-layer energy not conserved!"                    !
    print *, conserve_e                                                     !
end if                                                                      !
! Conservation of energy check (for next timestep)                          !
storage_prev_half = storage                                                 !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

return
end subroutine half_rs

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine interface_rs(depth, heatcp, lambda, T_ext, T_int, T0, ddt, g_flux)
! interface conduction scheme
implicit none

real(kind=dp), intent(in) :: depth(:), heatcp(:), lambda(:), ddt, T_ext
real, intent(in) :: T_int, T0
real(kind=dp), dimension(:), allocatable, save :: gga, ggb, ggc, ggd, ggb_init, res
real(kind=dp) :: storage, h_flux, conserve_e, Gext, Gint
real(kind=dp), intent(out) :: g_flux
real(kind=dp) :: m
integer :: k, n
logical, save :: init = .TRUE.

n = size(depth)

if (init) then
    print *, "Initial call of subroutine 'interface_rs'."
    allocate(material%temp_int(0:n))
    material%temp_int(:)=T0
    storage_prev_interface = 0.5*depth(1)*heatcp(1)*(material%temp_int(0)+material%temp_int(1))
    do k=2,n
        storage_prev_interface = storage_prev_interface+0.5*depth(k)*heatcp(k)*(material%temp_int(k-1)+material%temp_int(k))
    end do

    allocate(gga(0:n),ggb(0:n),ggc(0:n),ggd(0:n),ggb_init(0:n),res(n))
    do k=1,n
        res(k)=depth(k)/lambda(k)
    end do
    do k=1,n
      gga(k)= -1./res(k)
    end do
    ggb(0)= 1./res(1) +0.5*heatcp(1)*depth(1)/ddt +1./r_se
    do k=1,n-1
      ggb(k)= 1./res(k) +1./res(k+1) +0.5*(heatcp(k)*depth(k) +heatcp(k+1)*depth(k+1))/ddt
    end do
    ggb(n)=1./res(n) +0.5*heatcp(n)*depth(n)/ddt +1./r_si 
    do k=0,n-1
      ggc(k)= -1./res(k+1)
    end do
    ggb_init(:) = ggb(:)
    init = .FALSE.
end if
! update T_ext for timesteps > 1
ggb(:) = ggb_init(:)
ggd(0)= material%temp_int(0)*0.5*heatcp(1)*depth(1)/ddt +T_ext/r_se
do k=1,n-1
ggd(k)= material%temp_int(k)*0.5*(heatcp(k)*depth(k) +heatcp(k+1)*depth(k+1))/ddt
end do
ggd(n)= material%temp_int(n)*0.5*heatcp(n)*depth(n)/ddt + T_int/r_si
! tridiagonal solver (Thomas algorithm) to solve for roof, road and wall temperatures
do k=1,n
    m=gga(k)/ggb(k-1)
    ggb(k)=ggb(k)-m*ggc(k-1)
    ggd(k)=ggd(k)-m*ggd(k-1)
end do

material%temp_int(n)=ggd(n)/ggb(n)
do k=n-1,0,-1
    material%temp_int(k)=(ggd(k)-ggc(k)*material%temp_int(k+1))/ggb(k)
end do
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Conservation of energy check                                                  !
storage = 0.5*depth(1)*heatcp(1)*(material%temp_int(0)+material%temp_int(1))    !
do k=2,n                                                                        !
    storage = storage +0.5*depth(k)*heatcp(k)*(material%temp_int(k-1)+material%temp_int(k))
end do                                                                          !
h_flux = (storage - storage_prev_interface)/ddt                                 !
Gext = (T_ext - material%temp_int(0))/r_se                                      !
Gint = (material%temp_int(n) - T_int)/r_si                                      !
g_flux  = Gext - Gint                                                           !
conserve_e = h_flux-g_flux                                                      !
if (conserve_e<-0.00001 .or. conserve_e>0.00001) then                           !
    print *, "Warning, interface energy not conserved!"                         !
    print *, conserve_e                                                         !
end if                                                                          !
! Conservation of energy check (for next timestep)                              !
storage_prev_interface = storage                                                !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

return
end subroutine interface_rs

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

end program conduction

