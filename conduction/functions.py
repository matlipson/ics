## FOR USE WITH PYTHON 3.4 ##
print('''
	Purpose: 	Module containing functions used by various scripts
	Developer: 	Mathew Lipson <m.lipson@unsw.edu.au>
	Manuscript: Efficiently modelling urban heat storage: an interface conduction scheme in the aTEB urban land surface model
	Journal: 	Geoscientific Model Development
	Revision: 	12 September 2016
''')

import numpy as np
import pandas as pd
from scipy.integrate import quad
from scipy import stats

mode='energy'  ## if 'energy', then additional calculation in conduction shchemes to calcluate heat storage and test conservation

def external_temp(T0, T_ext_amp, ddt, period, phase, steps):
	'''Periodic external environment temperature for discrete forcing - simple'''
	T = [T0 + T_ext_amp*(np.sin(t*ddt*2*np.pi/period + phase)) for t in range(steps+1)]
	return T

def external_temp_int(T0, T_ext_amp, ddt, period, phase, steps):
	'''Periodic external environment temperature for discrete forcing - time integrated'''
	T = []
	for t in range(steps+1):
		# integration range a, b is half timestep either side of time
		a = t*ddt - ddt/2 
		b = t*ddt + ddt/2
		# integrate exact solution over interval a,b and divide by timestep to calculate mean flux over period
		T.append(quad(lambda dt: T0 + T_ext_amp*(np.sin(dt*2*np.pi/period + phase)),a,b)[0]/ddt)
	return T

#-----------------------------------------#
# Following functions for error statistics

def RMSE(in1,in2):
	'''Function to calculate Root Mean Square Error (RMSE) for two input arrays ie (sqrt(N^-1(sum(in1 - in2)^2)).'''
	return np.sqrt(np.mean((in1-in2)**2, axis=0))

def MAE(in1,in2):
	''' Mean Absolute Error (MAE) as a %.
	Default inputs (P-O), returns mean.|P-O|'''
	return np.mean(abs(in1-in2))

def MBE(in1,in2):
	'''This function calculates Mean Bias Error  (N^-1(Sum(input1-input2)).
		Default inputs (P - O)'''
	return np.mean(in1-in2, axis=0)

def r2(in1,in2):
	'''Calculates coefficient of determination (r^2) from stats.linregress()'''
	r = in1.corrwith(in2)
	return r**2

def fit_line1(x, y):
	"""Return slope, intercept of best fit line of single OLS removing NaNs"""
	# Remove entries where either x or y is NaN.
	clean_data = pd.concat([x, y], 1).dropna(0) # row-wise
	(_, x), (_, y) = clean_data.iteritems()
	slope, intercept, r, p, stderr = stats.linregress(x, y)
	return slope, intercept # could also return stderr

def RMSEs(in1,in2):
	'''Systematic Root Mean Square Error (RMSEs) describes linear bias.
	Default inputs (P - O) returns output (P_OLS - O)'''
	in1_OLS = pd.DataFrame(index=in1.index, columns=in1.columns)
	slope_t = pd.DataFrame(index=in1.index, columns=in1.columns)
	intercept_t = pd.DataFrame(index=in1.index, columns=in1.columns)
	for i in in1.columns:
		slope_t[i], intercept_t[i] = fit_line1(in2[i],in1[i])
		in1_OLS = intercept_t + slope_t*in2
	return np.sqrt(np.mean((in1_OLS-in2)**2, axis=0))

def RMSEu(in1,in2):
	'''Unsystematic Root Mean Square Error (RMSEu) describes variability.
	Default inputs (P - O) returns output (P_OLS - P)'''
	in1_OLS = pd.DataFrame(index=in1.index, columns=in1.columns)
	slope_t = pd.DataFrame(index=in1.index, columns=in1.columns)
	intercept_t = pd.DataFrame(index=in1.index, columns=in1.columns)
	for i in in1.columns:
		slope_t[i], intercept_t[i] = fit_line1(in2[i],in1[i])
		in1_OLS = intercept_t + slope_t*in2
	return np.sqrt(np.mean((in1_OLS-in1)**2, axis=0))

def SD(in1):
	'''Standard Deviation (SD).'''
	return np.nanstd(in1,axis=0)

def NSD(in1,in2):
	''' Normalised Standard Deviation (NSD) as a %.
	Default inputs SD(prediction)/SD(observation)'''
	# return np.nanstd(in1,axis=0)/np.nanstd(in2,axis=0)
	return in1.std(ddof=0)/in2.std(ddof=0)

def centred_RMSE(in1,in2):
	'''This function calculates the centred Root Mean Square Error (RMSE) for two input arrays
		by removing the mean bias of prediction and observation: centred_RMSE = sqrt(RMSE^2-Mean Bias^2)
		See Taylor 2001 D.O.I: 10.1029/2000JD900719'''
	return np.sqrt(np.mean((in1-in2)**2, axis=0) - (np.mean(in1-in2, axis=0))**2)

def norm_centred_RMSE(in1,in2):
	'''Centred RMSE normalised by standard deviation of observation
		See Taylor 2001 D.O.I: 10.1029/2000JD900719'''
	return np.sqrt(np.mean((in1-in2)**2, axis=0) - (np.mean(in1-in2, axis=0))**2)/in2.std(ddof=0)

def norm_MAE(in1,in2):
	''' Mean Absolute Error (MAE) as a %.
	Default inputs (P-O), returns mean.|P-O|/mean(O)'''
	return np.mean(abs(in1-in2))/np.mean(abs(in2))

def combine(mtrls):
	'''Combines depth averaged properties of multiple layers into one layer
	Input ([(depth1,material1),(depth2,material2),(depthN,materialN)]) where:
		depth is layer thickness in metres; material is list of material conductivity and heat capacity
	Output list of [total depth, equivalent conductivity, equivalent heat capacity]'''
	tot_depth, eqvlnt_conduct, eqvlnt_heatc = 0,0,0
	for d,m in mtrls:
		tot_depth += d
		eqvlnt_conduct += d/m[0]
		eqvlnt_heatc += d*m[1]
	eqvlnt_conduct = tot_depth/eqvlnt_conduct
	eqvlnt_heatc = eqvlnt_heatc/tot_depth
	return [tot_depth,eqvlnt_conduct,eqvlnt_heatc]


def heat_transfer_matrix(r_se, r_si, period, wall):
	'''	Following from ISO13786 Thermal Performance of Building Components - Dynamic Thermal Characteristics - Calculation Methods
	Creates complex heat transfer matrices and matrix multiplication to calculate exact dynamic temperature and heat flow changes.'''

	# Surface resistance matrices
	Z_re = np.matrix( [[1, -r_se],[0, 1]] )
	Z_ri = np.matrix( [[1, -r_si],[0, 1]] )

	# Create multi-layer heat transfer matrix Z per ISO 13786:2007
	Z = {}
	delta0 = []
	for i in range(len(wall)):
		# Periodic penetration depth delta = sqrt((conduct*period)/(pi * vol. heat capacity))
		delta = np.sqrt((wall['conduct'][i] * period)/(np.pi * wall['heatc'][i]))
		ksi = wall['depth'][i]/delta
		delta0.append(delta)

		# Matrix elements of the heat transfer matrix Z for each layer of wall
		Z_11 = np.cosh(ksi)*np.cos(ksi) + 1j*np.sinh(ksi)*np.sin(ksi)
		Z_12 = -(delta/(2*wall['conduct'][i]))*(np.sinh(ksi)*np.cos(ksi) + np.cosh(ksi)*np.sin(ksi)
			+ 1j*(np.cosh(ksi)*np.sin(ksi) - np.sinh(ksi)*np.cos(ksi)))
		Z_21 = -(wall['conduct'][i]/delta)*(np.sinh(ksi)*np.cos(ksi) - np.cosh(ksi)*np.sin(ksi)
			+ 1j*(np.sinh(ksi)*np.cos(ksi) + np.cosh(ksi)*np.sin(ksi)))
		Z_22 = Z_11
		Z[i] = np.matrix( [[Z_11, Z_12],[Z_21, Z_22]], dtype='complex' )

	Z_in = np.matrix(np.identity(2))
	# where Z_in is solution for heat transfer from outside -> in (fixed internal T)
	for key in sorted(Z):
		Z_in = Z_in * Z[key]
	Z_out = Z_in.getI()

	Z_totalin = Z_re * Z_in * Z_ri
	Z_totalout = Z_totalin.getI()

	return Z_in, Z_out, Z_totalin, Z_totalout
#-----------------------------------------#
# Class for material characteristics using heat transfer matrix Z calculated using function heat_transfer_matrix

class Characteristics:
	'''For ISO13786 Dynamic thermal characteristics of walls. 
	This class passes self.material and thermal properties for the defined 'wall', as well as variables set in the __main__'''
	def __init__(self, r_se, r_si, period, phase, ddt, steps, material, Z_in, Z_out, Z_totalin, Z_totalout):
		self.r_se = r_se
		self.r_si = r_si
		self.period = period
		self.phase = phase
		self.ddt = ddt
		self.steps = steps
		self.material = material
		self.Z_in = Z_in
		self.Z_out = Z_out
		self.Z_totalin = Z_totalin
		self.Z_totalout = Z_totalout

	def ArealHeatCapacity(self, ext_res=True, loc='out'):
		'''Returns ISO 13786 external/internal areal heat capacity WITH surface resistances.
		Input required: loc= "in" for internal, "out" for external.
		Input required: ext_res= True for external resistance, False for none
		Units: KJ/(m^2 K)'''
		if ext_res==True and loc=='out':
			K=self.period/(2*np.pi)*abs((self.Z_totalin[1,1]-1)/self.Z_totalin[0,1])
		elif ext_res==True and loc=='in':
			K=self.period/(2*np.pi)*abs((self.Z_totalin[0,0]-1)/self.Z_totalin[0,1])
		elif ext_res==False and loc=='out':
			K=self.period/(2*np.pi)*abs((self.Z_in[1,1]-1)/self.Z_in[0,1])
		elif ext_res==False and loc=='in':
			K=self.period/(2*np.pi)*abs((self.Z_in[0,0]-1)/self.Z_in[0,1])
		else:
			print('input required ext_res=[True/False],loc=[in/out]')		
		return K

	def PeriodicAdmittance(self, loc='out'):
		'''Returns ISO 13786 external/internal thermal dynamic admittance
		Input required: "in" for internal, "out" for external.
		Sometimes taken as thermal storage, but is only true at one surface. Better Areal Heat Capacity.
		Units: W/(m^2 K)'''
		Y_11 = -(self.Z_totalin[0,0]/self.Z_totalin[0,1])
		Y_22 = -(self.Z_totalin[1,1]/self.Z_totalout[0,1])
		if loc == 'in':
			return abs(Y_11)
		elif loc == 'out':
			return abs(Y_22)
		else:
			print('input required "in" or "out"')

	def PeriodicTransmittance(self):
		'''Returns ISO 13786 periodic thermal transmittance.
		Units: W/(m^2 K)'''
		Y_12 = -1/self.Z_totalin[0,1]
		return abs(Y_12)

	def TotalResistance(self, ext_res=True):
		'''Resistance = depth/conductivity, and resistance add in series, so total R = tot_depth/tot_resistance'''
		tot_depth = sum(self.material['depth'])
		if ext_res == True:
			tot_resistance = self.r_se + self.r_si
		else:
			tot_resistance = 0.
		for i in range(len(self.material)):
			tot_resistance = tot_resistance + self.material['depth'][i] / (self.material['conduct'][i])
		return tot_resistance

	def DecrementFactor(self):
		'''Returns ISO 13786 Decriment Factor. 
		The dimensionless ratio of periodic thermal transmittance to steady-state thermal transmittance (U),
		or peak heat flow out (per K change outside) to heat flow through entire element (per K difference b/w inside and out)'''
		Y_12 = -1/self.Z_totalin[0,1]
		f = abs(Y_12)*self.TotalResistance()
		return f

	def DecrementDelay(self):
		'''Returns ISO 13786 Decrement Delay, the argument of complex periodic thermal transmittance, (Thermal Lag) in hours
		Units: Hours'''
		Y_12 = -1/self.Z_totalin[0,1]
		lag = -(self.period/(2*np.pi)*np.angle(Y_12))/3600
		if lag > 0:
			return lag
		if lag < 0:
			return lag + 24

	def exact_function_integrated(self):
		'''Function to calculate exact heat storage values at each timestep based on heat transfer matrix.
		Converting from the complex plane to the time domain. From the sinusoidal wave equation A=A_0*sin(wt + theta),
		 where A_0 = |Y_22|, angular frequency w=2*pi/P and theta = arg(Y_22)
		 INTEGRATED EXACT VALUES over each timestep'''
		exact_gout, exact_gin, exact_gsum = [],[],[]
		Y_12 = -1/self.Z_totalin[0,1]
		Y_22 = -self.Z_totalin[1,1]/self.Z_totalin[0,1]
		for t in range(self.steps+1):
			# integration range a, b is half timestep either side of time
			a = t*self.ddt - self.ddt/2 
			b = t*self.ddt + self.ddt/2
			# integrate exact solution over interval a,b and divide by timestep to calculate mean flux over period
			exact_gin.append(quad(lambda dt: np.abs(Y_22)*np.sin((2*np.pi/self.period)*dt + np.angle(Y_22)), a, b)[0]/(self.ddt))
			exact_gout.append(quad(lambda dt: np.abs(Y_12)*np.sin((2*np.pi/self.period)*(dt) + np.angle(Y_12)), a, b)[0]/(self.ddt))
			exact_gsum.append(exact_gin[-1] - exact_gout[-1])
		return pd.DataFrame({'G_in':exact_gin,'G_out':exact_gout, 'G_sum':exact_gsum})

	def exact_paper(self,ext_res=True):
		'''Function to calculate exact heat storage values at each timestep based on heat transfer matrix of ISO 13786:2007.
		Admittance flux - transmittance flux: -Z_{22}/Z_{12} + 1/Z_{12} = (1 - Z_{22})/Z_{12}
		Converting from the complex plane to the time domain. From the sinusoidal wave equation A=A_0*sin(wt + theta),
		 where A_0 = |complexG|, angular frequency w=2*pi/P and theta = arg(complexG)'''
		if ext_res == True:
			complex_gsum = (1-self.Z_totalin[1,1])/self.Z_totalin[0,1] # admittance-transmitance from outside in with external surface resistance 
		else:
			complex_gsum = (1-self.Z_in[1,1])/self.Z_in[0,1]  # without external surface resistance
		real_gsum = [np.abs(complex_gsum)*np.sin((2*np.pi/self.period)*(t)*self.ddt +np.angle(complex_gsum) + self.phase) for t in range(self.steps+1)]
		return pd.DataFrame({'G_sum':real_gsum})

	def exact_integrated(self,ext_res=True):
		'''Function to calculate exact heat storage values at each timestep based on heat transfer matrix of ISO 13786:2007.
		Admittance flux - transmittance flux: -Z_{22}/Z_{12} + 1/Z_{12} = (1 - Z_{22})/Z_{12}
		Then integrated through -dt/2 -> +dt/2'''
		int_gsum = []
		if ext_res == True:
			complex_gsum = (1-self.Z_totalin[1,1])/self.Z_totalin[0,1] # admittance-transmitance from outside in with external surface resistance 
		else:
			complex_gsum = (1-self.Z_in[1,1])/self.Z_in[0,1]  # without external surface resistance
		for t in range(self.steps+1):
			# integration range a, b is half timestep either side of time
			a = t*self.ddt - self.ddt/2 
			b = t*self.ddt + self.ddt/2
			int_gsum.append(quad(lambda dt: np.abs(complex_gsum)*np.sin((2*np.pi/self.period)*dt + np.angle(complex_gsum) + self.phase), a, b)[0]/(self.ddt))
		return pd.DataFrame({'G_sum':int_gsum})

	### TEST INTEGRATION OF SIN WAVE IN PHASOR FORM ###
	# intervals = 8
	# ddt = (2*np.pi)/intervals
	# integrated = []
	# z = complex(3,4) # z = 3 + 4i
	# for t in range(intervals+1):
	# 	a = t*ddt - ddt/2
	# 	b = t*ddt + ddt/2
	# 	integrated.append(quad(lambda dt: np.abs(z)*np.sin(dt + np.angle(z)), a, b)[0]/(ddt))
	# plt.plot([t for t in np.linspace(0,2*np.pi,intervals+1)], [np.abs(z)*np.sin(t + np.angle(z)) for t in np.linspace(0,2*np.pi,intervals+1)], color='r', label='exact')
	# plt.plot([t for t in np.linspace(0,2*np.pi,intervals+1)], integrated, color='b', label='integrated')
	# plt.plot([t for t in np.linspace(0,2*np.pi, 192)], [np.abs(z)*np.sin(t + np.angle(z)) for t in np.linspace(0,2*np.pi,192)], color='0.7', label='sin(z)')
	# plt.legend()
	# plt.show()

	#----------------------------------------------------#
	# Following functions for other wall characteristics used in conduction_interface

	def CyclicThickness(self):
		'''Calculates 'Cyclic Thickness', where:
		tau = sqrt((pi * vol heat capacity * depth^2)/(period of excitation * conductivity))
		as defined in M. Davies (1983), DOI: 10.1016/0360-1323(83)90015-X.
		Cyclic Thickness is dimensionless'''
		tau = 0
		for i in range(len(self.material)):
			Cv = self.material.ix[i]['heatc']	# Volumetric heat capacity of layer
			depth = self.material.ix[i]['depth']
			conductivity = self.material.ix[i]['conduct']
			tau = tau + np.sqrt((np.pi * Cv * depth**2)/(self.period * conductivity))
		return tau

	def ThermalDiffusivity(self):
		'''Calculates 'Thermal Diffusivity', 
		alpha = conductivity / vol. heat capacity
		Dimensions: m^2/s x10^6'''
		out = []
		for i in range(len(self.material)):
			out.append((self.material.ix[i]['conduct'] / self.material.ix[i]['heatc'])*1E6)
		return out

	def ThermalLag(self):
		'''Calculates 'Thermal Lag' '''
		lag = 0
		for i in range(len(self.material)):
			lag = lag + np.sqrt((self.material.ix[i]['heatc'] * self.period)/(4 * np.pi * self.material.ix[i]['conduct'])) * self.material.ix[i]['depth']
		return lag/(60*60)

	def ThermalAdmittance(self):
		''' Calculates steady-state Thermal Admittance (or effusivity) of first layer.
		Units: J /(m^2 s^(1/2) K^1)'''
		return np.sqrt(self.material.ix[0]['conduct'] * self.material.ix[0]['heatc'])

	def CyclicAdmittance(self):
		'''Calculates 'Cyclic Admittance', where:
		alpha = sqrt((2 * pi * conductvity * vol. heat capacity)/ period of excitation)
		as defined in M. Davies (1983), DOI: 10.1016/0360-1323(83)90015-X.
		Cyclic Thickness is a property of the surface'''
		out = []
		for i in range(len(self.material)):
			out.append(np.sqrt((2 * np.pi * self.material.ix[i]['conduct'] * self.material.ix[i]['heatc'])/self.period))
		return out

	def EffectiveConductivity(self):
		'''Calculates Effective Conductivity for a composite wall .
		Resistance = depth/conductivity, and resistance add in series, so total R = tot_depth/tot_resistance'''
		tot_depth = sum(self.material['depth'])
		tot_resistance = 0
		for i in range(len(self.material)):
			tot_resistance = tot_resistance + self.material['depth'][i] / (self.material['conduct'][i])
		out = tot_depth/tot_resistance
		return out

#----------------Class for discrete conduction schemes------------------------------------#

class Conduction:
	'''This class passes self.material and thermal properties for the defined 'wall', as well as variables set in __main__ (conduction_interface.py)'''
	def __init__(self, r_se, r_si, period, ddt, steps, T0, T_int, T_ext, material):
		self.r_se = r_se
		self.r_si = r_si
		self.period = period
		self.ddt = ddt
		self.steps = steps
		self.T0 = T0
		self.T_int = T_int
		self.T_ext = T_ext
		self.material = material
		self.res = np.array([self.material.ix[i,0]/self.material.ix[i,1] for i in self.material.index]) # layer thermal resistances
		self.cap = np.array([self.material.ix[i,0]*self.material.ix[i,2] for i in self.material.index]) # layer heat capacity

	#----------------------------------------------------#
	# Discrete heat conduction functions

	# Tri-diagonal solver for N layer conduction (Thomas Algorithm).
	# All conduction functions use the following layout for the solver (shown with N=7 in this case).

	# 	#  T0  T1  T2   3   4   5  N-1 
	# 	1 [b0, c0,  0,| __, __, __, 0 ][T0] = [d0]
	# 	2 [a1, b1, c1,|  0, __, __, 0 ][T1] = [d1]
	# 	3 [ 0, a2, b2,| c2,  0, __, 0 ][T2] = [d2]
	# 	4 [__,  0, a3,| b3, c3,  0, 0 ][T3] = [d3]
	# 	5 [__, __,  0,| a4, b4, c4, 0 ][T4] = [d4]
	# 	6 [__, __, __,|  0, a5, b5, c5][T5] = [d5]
	# 	7 [__, __, __,| __,  0, a6, b6][T6] = [d6]

	def tridiag_solver(self,a,b,c,d):
		'''Tridiagonal matrix solver using Thomas Algorith from https://gist.github.com/ofan666/1875903'''
		nf = len(a)
		bb, dd = map(np.array,(b,d))		# copy arrays for use in loop
		for k in range(1,nf):
			eq = a[k]/bb[k-1]
			bb[k] = bb[k] - eq*c[k-1]
			dd[k] = dd[k] - eq*dd[k-1]
		x = np.zeros(nf)
		x[-1] = dd[-1]/bb[-1]
		for k in range(nf-2, -1, -1):
			x[k] = (dd[k] - c[k]*x[k+1])/bb[k]
		return x

	def half_layer(self):
		'''Half-layer model (standard model) with surface resistances'''
		n = len(self.material)		# Number of layers	
		T = np.full([self.steps+1,n],self.T0) # Set initial temperature in array

		# T^m+1 terms of Eq. (7) where a: T_k-1, b: T_k, c: T_k+1
		a_constant = np.array([0.]+[-2/(self.res[k-1] +self.res[k]) for k in range(1,n)])

		b_constant = np.array([0.]+[2/(self.res[k-1] + self.res[k]) + self.cap[k]/self.ddt + 2/(self.res[k] + self.res[k+1]) for k in range(1,n-1)]+[0])
		b_constant[0] = 2/(self.res[0] + self.res[1]) + self.cap[0]/self.ddt + 1/(0.5*self.res[0] + self.r_se )
		b_constant[-1] = 2/(self.res[-2] + self.res[-1]) + self.cap[-1]/self.ddt + 1/(0.5*self.res[-1] + self.r_si)

		c_constant = np.array([-2/(self.res[k] + self.res[k+1]) for k in range(n-1)]+[0])

		for m in range(self.steps):
			print('timestep: %s of %s' %(m,self.steps), end='\r')
			d_star = np.array([0.]+[T[m,k]*self.cap[k]/self.ddt for k in range(1,n-1)]+[0])
			d_star[0] = T[m,0]*self.cap[0]/self.ddt + self.T_ext[m+1]/(0.5*self.res[0] + self.r_se)
			d_star[-1] = T[m,-1]*self.cap[-1]/self.ddt + self.T_int/(0.5*self.res[-1] + self.r_si)
			########### TRIDIAGONAL SOLVER FOR NEXT TIMESTEP m+1 ###############
			T[m+1] = self.tridiag_solver(a_constant,b_constant,c_constant,d_star)

		# Energy: create array with columns=['energy_sum', 'delta_energy','flux_in','flux_out','net_flux','closure_error']
		energy_arr = np.zeros([self.steps+1,6])
		for i in range(n):
			energy_arr[0,0] += self.cap[i]*self.T0

		for m in range(self.steps):
			# Sum total E across layers [i] at timestep [t] by c[i]*d[i]*T[t]
			for i in range(n):
				energy_arr[m+1,0] += self.cap[i]*T[m+1,i]
			# Columns: 'dE': difference in energy between this and last step, 'G_in': Input flux, 'G_out': output flux, 'G_sum': in-out, 'E-G': error
			energy_arr[m+1,1] = energy_arr[m+1,0] - energy_arr[m,0]
			energy_arr[m+1,2] = (self.T_ext[m+1] - T[m+1,0])/(0.5*self.res[0] + self.r_se)*self.ddt
			energy_arr[m+1,3] = (T[m+1,-1] - self.T_int)/(0.5*self.res[-1] + self.r_si)*self.ddt
			energy_arr[m+1,4] = energy_arr[m+1,2] - energy_arr[m+1,3]
			energy_arr[m+1,5] = energy_arr[m+1,1] - energy_arr[m+1,4]
		if np.abs(energy_arr).sum(axis=0)[5] >= 1E-3:
			print('Warning: Energy not conserved (half-layer). Cumulative error: %f' %np.abs(energy_arr).sum(axis=0)[5])

		energy_pd = pd.DataFrame(data=energy_arr/self.ddt, index=range(len(energy_arr)), columns=['E_sum', 'dE','G_in','G_out','G_sum','E-G'])
		temp_pd = pd.DataFrame(data=T, columns=['Node'+str(i+1) for i in range(n)])
		return energy_pd #, temp_pd

	def interface(self):
		'''Interface conduction model (proposed).'''
		n = len(self.material)			# number of layers
		nf = n+1						# number of equations
		T = np.full([self.steps+1,nf],self.T0) # Set initial temperature in array

		# T^m+1 terms of Eq. (7) where a: T_k-1, b: T_k, c: T_k+1
		a_constant = np.array([0.]+[-1/self.res[k-1] for k in range(1,nf)])

		b_constant = np.array([0.]+[1/self.res[k-1] + 1/self.res[k] + 0.5*(self.cap[k-1] + self.cap[k])/self.ddt for k in range(1,nf-1)]+[0])
		b_constant[0] = 1/self.r_se + 1/self.res[0] + 0.5*self.cap[0]/self.ddt
		b_constant[-1] = 1/self.r_si + 1/self.res[-1] + 0.5*self.cap[-1]/self.ddt

		c_constant = np.array([-1/self.res[k] for k in range(nf-1)]+[0])

		for m in range(self.steps):
			print('timestep: %s of %s' %(m,self.steps), end='\r')
			d_star = np.array([0.]+[T[m,k]*0.5*(self.cap[k-1] + self.cap[k])/self.ddt for k in range(1,nf-1)]+[0])
			d_star[0] = T[m,0]*0.5*self.cap[0]/self.ddt + self.T_ext[m+1]/self.r_se
			d_star[-1] = T[m,-1]*0.5*self.cap[-1]/self.ddt + self.T_int/self.r_si
			########### TRIDIAGONAL SOLVER FOR NEXT TIMESTEP ###############
			T[m+1] = self.tridiag_solver(a_constant,b_constant,c_constant,d_star)
			
		# Energy: create array with columns=['energy_sum', 'delta_energy','flux_in','flux_out','net_flux','closure_error']
		energy_arr = np.zeros([self.steps+1,6])
		for i in range(n):
			energy_arr[0,0] += self.cap[i]*self.T0

		for m in range(self.steps):
			# Sum total E across layers [i] at timestep [t] by c[i]*d[i]*T[t]
			for i in range(n):
				energy_arr[m+1,0] += 0.5*(self.cap[i]*T[m+1,i] + self.cap[i]*T[m+1,i+1])
			# Columns: 'dE': difference in energy between this and last step, 'G_in': Input flux, 'G_out': output flux, 'G_sum': in-out, 'E-G': error
			energy_arr[m+1,1] = energy_arr[m+1,0] - energy_arr[m,0]
			energy_arr[m+1,2] = (self.T_ext[m+1] - T[m+1,0])/self.r_se*self.ddt
			energy_arr[m+1,3] = (T[m+1,-1] - self.T_int)/self.r_si*self.ddt
			energy_arr[m+1,4] = energy_arr[m+1,2] - energy_arr[m+1,3]
			energy_arr[m+1,5] = energy_arr[m+1,1] - energy_arr[m+1,4]
		if np.abs(energy_arr).sum(axis=0)[5] >= 1E-3:
			print('Warning: Energy not conserved (interface). Cumulative error: %f' %np.abs(energy_arr).sum(axis=0)[5])

		energy_pd = pd.DataFrame(data=energy_arr/self.ddt, index=range(len(energy_arr)), columns=['E_sum', 'dE','G_in','G_out','G_sum','E-G'])
		temp_pd = pd.DataFrame(data=T, columns=['Node'+str(i+1) for i in range(nf)])
		return energy_pd #, temp_pd

