## FOR USE WITH PYTHON 3.4 ##
print('''
	Purpose: 	1) Store roof and walls characteristics for use in conduction_interface.py
	Developer: 	Mathew Lipson <m.lipson@unsw.edu.au>
	Manuscript: Efficiently modelling urban heat storage: an interface conduction scheme in the aTEB urban land surface model
	Journal: 	Geoscientific Model Development
	Revision: 	3 August 2016
''')

import numpy as np
import pandas as pd

def combine(mtrls):
	'''Combines depth averaged properties of multiple layers into one layer
	Input ([(depth1,material1),(depth2,material2),(depthN,materialN)]) where:
		depth is layer thickness in metres; material is list of material conductivity and heat capacity
	Output list of [total depth, equivalent conductivity, equivalent heat capacity]'''
	tot_depth, eqvlnt_conduct, eqvlnt_heatc = 0,0,0
	for d,m in mtrls:
		tot_depth += d
		eqvlnt_conduct += d/m[0]
		eqvlnt_heatc += d*m[1]
	eqvlnt_conduct = tot_depth/eqvlnt_conduct
	eqvlnt_heatc = eqvlnt_heatc/tot_depth
	return [tot_depth,eqvlnt_conduct,eqvlnt_heatc]

#--------------Materials---------------#
#----values from Jackson et al 2010----#

wf=5 # window factor  	# Jackson defaul = 5
gf=1 # glass factor		# Jackson default = 1
lf=2 # lotec factor		# Jackson default = 2

##### [conductivity, heat capcity] #####

cncrt_pnl = [wf*1.28,		2121000]
air = 		[wf*0.0575,		259405]
cncrt_blk = [wf*0.86,		781200]
XPS = 		[wf*0.05675,	289700]
drywall = 	[wf*0.16,		609000]
clay_brck =	[wf*0.5,		1520000]
brck_reinfc=[wf*1.1,		1612800]
stone = 	[wf*2.56,		2178120]
plaster = 	[wf*0.6,		1141250]
bldg_ppr = 	[wf*0.109,		1635600]
ext_drwll = [wf*0.16,		609000]
insulation =[wf*0.065,		266060]
EIFS = 		[wf*0.59,		9660000]
EPS = 		[wf*0.0607,		280220]
cmnt_brd = 	[wf*0.082,		455000]
fiberboard =[wf*0.0535,		601600]
hb_siding = [wf*0.12,		1723960]
OSB = 		[wf*0.0934,		1180640]
hb_sid =	[wf*0.12,		1723960]
wood_sid =	[wf*0.14,		1045500]
plywood = 	[wf*0.0905,		940000]
vnyl_sid =	[wf*0.70,		2376000]
stucco = 	[wf*0.60,		1141250]
metal = 	[wf*68.5,		2969500]
galv_steel =[wf*45.0,		3744000]
air_brgd_wf=[wf*2.2785,		188346]

glass = 	[gf*1.29,		2188620]
air_brgd=	[gf*2.2785,		188346]

stone_lf = 	[lf*2.56,		2178120]
rubble = 	[lf*0.80,		950000]
mudadobe =  [lf*0.60,		1408000]

### roof materials ###

asphalt 	= [1.2, 	1932000]
felt 		= [0.19,	912000]
bitumen 	= [1.2, 	1932000]
ins_slab 	= [0.036,	96600]
cllr_cncrt  = [0.70,	840000]
gravel		= [1.44,	1478400]
pvceps 		= [0.0343, 	28959]
plywood_rf  = [0.15, 	994000]
insulation_rf=[2.2842,	278970]
steeldeck  	= [45.0,	3744000]
epdm 		= [0.33, 	2737800]
galvsteel_rf= [45.0,	3744000]
metal_rf 	= [68.5,	2969500]
shingle 	= [1.15,	1957200]
cer_tile	= [1.2,		1700000]
thatch 		= [0.07,	43200]
timber 		= [0.17,	1034000]
slate 		= [1.72,	2310000]
mudadobe_rf = [0.60,	1408000]

air_rf      = [0.0575,	259405]
air_brgd_rf = [2.2785,	188346]

ISO13786_example1 = pd.DataFrame([
	[0.005, 1.80, 2400*1000],
	[0.005, 1.80, 2400*1000]],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

ISO13786_example2 = pd.DataFrame([
	[0.005, 1.00, 1200*1500],
	[0.100, 0.04, 30*1400],
	[0.200, 1.80, 2400*1000]],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

## Site data from Grimmond et al., 2011 (10.1002/joc.2227)

SITE_wall_4l = pd.DataFrame([
	[0.0404, 0.61,  1.25E6],
	[0.054,  0.43,  1.4E6],
	[0.042,  0.024, 0.0013E6],
	[0.0125, 0.16,  0.67E6]],	
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

SITE_roof_4l = pd.DataFrame([
	[0.0116, 6.53,  2.07E6],		
	[0.0500, 0.025, 0.0071E6],
	[0.0400, 0.230, 1.50E6],
	[0.0125, 0.160, 0.67E6]],			
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

## aTEB characteristics from Thather and Hurley, 2012 (10.1007/s10546-011-9663-8)

aTEB_roof_4l = pd.DataFrame([
	[0.01, 1.51, 2.11E6], 		# _Dense_Concrete 	(Oke 87)
	[0.09, 1.51, 2.11E6],		# _Dense_Concrete 	(Oke 87) aTEB depth
	[0.40, 0.08, 0.28E6],		# _Aerated_Concrete	(Oke 87)
	[0.10, 0.05, 0.29E6]],		# _Insulation		(Oke 87)	
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

aTEB_wall_4l = pd.DataFrame([
	[0.01, 0.9338, 1.55E6],		# _Concrete 		(Mills 93)
	[0.04, 0.9338, 1.55E6],		# _Concrete 		(Mills 93) aTEB depth
	[0.10, 0.9338, 1.55E6],		# _Concrete 		(Mills 93) aTEB depth
	[0.05, 0.05,   0.29E6]],	# _Insulation	(Oke 87)	
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

## WRF characteristics from Loridan and Grimmond, 2012 (10.1002/qj.963)

WRF_roof_4l = pd.DataFrame([
	[0.05, 0.67, 1.00E6],		
	[0.05, 0.67, 1.00E6],		
	[0.05, 0.67, 1.00E6],	
	[0.05, 0.67, 1.00E6]],				
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

WRF_wall_4l = pd.DataFrame([
	[0.05, 0.67, 1.00E6],		
	[0.05, 0.67, 1.00E6],		
	[0.05, 0.67, 1.00E6],	
	[0.05, 0.67, 1.00E6]],				
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

UZE_roof_4l = pd.DataFrame([
	[0.05, 0.40, 1.20E6],		
	[0.10, 0.40, 1.20E6],		
	[0.15, 0.40, 1.20E6],	
	[0.20, 0.40, 1.20E6]],				
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

UZE_wall_4l = pd.DataFrame([
	[0.05, 1.00, 1.20E6],		
	[0.05, 1.00, 1.20E6],		
	[0.10, 1.00, 1.20E6],	
	[0.10, 1.00, 1.20E6]],				
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

aTEB_road_4l = pd.DataFrame([
	[0.01, 0.7454, 1.94E6],		# _Asphalt 		(Mills 93)
	[0.04, 0.7454, 1.94E6],		# _Asphalt 		(Mills 93)
	[0.45, 0.2513, 1.28E6],		# _Dry_Soil		(Mills 93) aTEB depth
	[3.50, 0.2513, 1.28E6]],	# _Dry_Soil 	(Oke 87)   aTEB depth
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

WRFu_road_4l = pd.DataFrame([
	[0.05, 0.4004, 1.40E6],		
	[0.25, 0.4004, 1.40E6],		
	[0.50, 0.4004, 1.40E6],	
	[0.75, 0.4004, 1.40E6]],				
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

UZEm_road_4l = pd.DataFrame([
	[0.25, 0.8, 1.50E6],		
	[0.25, 0.8, 1.50E6],		
	[0.25, 0.8, 1.50E6],	
	[0.25, 0.8, 1.50E6]],				
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

SITE_road_4l = pd.DataFrame([
	[0.02875, 1.17, 1.14E6],
	[0.1583,  0.30, 1.05E6],
	[0.1125,  0.42, 1.29E6],
	[0.65045, 3.72, 1.43E6]],	
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

## homogenous five layer versions for test halving first layer

WRF_roof_5l = pd.DataFrame([
	[0.025, 0.67, 1.00E6],
	[0.025, 0.67, 1.00E6],
	[0.05, 0.67, 1.00E6],		
	[0.05, 0.67, 1.00E6],	
	[0.05, 0.67, 1.00E6]],				
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

WRF_wall_5l = pd.DataFrame([
	[0.025, 0.67, 1.00E6],		
	[0.025, 0.67, 1.00E6],		
	[0.05, 0.67, 1.00E6],		
	[0.05, 0.67, 1.00E6],	
	[0.05, 0.67, 1.00E6]],				
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

UZE_roof_5l = pd.DataFrame([
	[0.025, 0.40, 1.20E6],		
	[0.025, 0.40, 1.20E6],		
	[0.10, 0.40, 1.20E6],		
	[0.15, 0.40, 1.20E6],	
	[0.20, 0.40, 1.20E6]],				
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

UZE_wall_5l = pd.DataFrame([
	[0.025, 0.40, 1.20E6],			
	[0.025, 0.40, 1.20E6],		
	[0.05, 1.00, 1.20E6],		
	[0.10, 1.00, 1.20E6],	
	[0.10, 1.00, 1.20E6]],				
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

##----------------CLMu WALLS----------------------------##
## Derived from Jackson et al., 2010 (10.1080/00045608.2010.497328)

CLMu_wall_concpanels_10l = pd.DataFrame([
	[0.050]+cncrt_pnl,
	[0.039]+cncrt_pnl,
	[0.025]+air,
	[0.050]+cncrt_blk,
	[0.050]+cncrt_blk,
	[0.050]+cncrt_blk,
	[0.050]+cncrt_blk,
	[0.013]+XPS,
	[0.012]+XPS,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(10)])

CLMu_wall_concpanels_9l = pd.DataFrame([
	[0.050]+cncrt_pnl,
	[0.039]+cncrt_pnl,
	[0.025]+air,
	[0.050]+cncrt_blk,
	[0.050]+cncrt_blk,
	[0.050]+cncrt_blk,
	[0.050]+cncrt_blk,
	[0.025]+XPS,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(9)])

CLMu_wall_concpanels_8l = pd.DataFrame([
	[0.050]+cncrt_pnl,
	[0.039]+cncrt_pnl,
	[0.025]+air,
	[0.060]+cncrt_blk,
	[0.070]+cncrt_blk,
	[0.070]+cncrt_blk,
	[0.025]+XPS,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(8)])

CLMu_wall_concpanels_7l = pd.DataFrame([
	[0.050]+cncrt_pnl,
	[0.039]+cncrt_pnl,
	[0.025]+air,
	[0.100]+cncrt_blk,
	[0.100]+cncrt_blk,
	[0.025]+XPS,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_wall_concpanels_6l = pd.DataFrame([
	[0.050]+cncrt_pnl,
	[0.039]+cncrt_pnl,
	[0.025]+air,
	[0.200]+cncrt_blk,
	[0.025]+XPS,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(6)])

CLMu_wall_concpanels_5l = pd.DataFrame([
	[0.089]+cncrt_pnl,
	[0.025]+air,
	[0.200]+cncrt_blk,
	[0.025]+XPS,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

CLMu_wall_concpanels_4l = pd.DataFrame([
	[0.089]+cncrt_pnl,
	[0.025]+air,
	[0.200]+cncrt_blk,
	combine([(0.025,XPS),(0.012,drywall)])], # combine XPS & drywall
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

CLMu_wall_concpanels_3l = pd.DataFrame([
	[0.089]+cncrt_pnl,
	[0.025]+air,
	combine([(0.200,cncrt_blk),(0.025,XPS),(0.012,drywall)])], # combine cncrt_blk & XPS & drywall
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

CLMu_wall_concpanels_2l = pd.DataFrame([
	[0.089]+cncrt_pnl,
	combine([(0.025,air),(0.200,cncrt_blk),(0.025,XPS),(0.012,drywall)])], # combine cncrt_blk & XPS & drywall
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

combine([(0.089,cncrt_pnl),(0.025,air),(0.200,cncrt_blk),(0.025,XPS),(0.012,drywall)])

##--------------------------------------------##

CLMu_wall_glasscurtain_10l = pd.DataFrame([
	[0.004]+glass,
	[0.003]+glass,
	[0.003]+air_brgd,
	[0.002]+air_brgd,
	[0.002]+air_brgd,
	[0.002]+air_brgd,
	[0.002]+air_brgd,
	[0.002]+air_brgd,
	[0.004]+glass,
	[0.003]+glass],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(10)])

CLMu_wall_glasscurtain_9l = pd.DataFrame([
	[0.004]+glass,
	[0.003]+glass,
	[0.003]+air_brgd,
	[0.002]+air_brgd,
	[0.002]+air_brgd,
	[0.002]+air_brgd,
	[0.002]+air_brgd,
	[0.002]+air_brgd,
	[0.007]+glass],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(9)])

CLMu_wall_glasscurtain_8l = pd.DataFrame([
	[0.004]+glass,
	[0.003]+glass,
	[0.003]+air_brgd,
	[0.002]+air_brgd,
	[0.002]+air_brgd,
	[0.002]+air_brgd,
	[0.004]+air_brgd,
	[0.007]+glass],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(8)])

CLMu_wall_glasscurtain_7l = pd.DataFrame([
	[0.004]+glass,
	[0.003]+glass,
	[0.003]+air_brgd,
	[0.002]+air_brgd,
	[0.004]+air_brgd,
	[0.004]+air_brgd,
	[0.007]+glass],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_wall_glasscurtain_6l = pd.DataFrame([
	[0.004]+glass,
	[0.003]+glass,
	[0.005]+air_brgd,
	[0.004]+air_brgd,
	[0.004]+air_brgd,
	[0.007]+glass],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(6)])

CLMu_wall_glasscurtain_5l = pd.DataFrame([
	[0.004]+glass,
	[0.003]+glass,
	[0.006]+air_brgd,
	[0.007]+air_brgd,
	[0.007]+glass],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

CLMu_wall_glasscurtain_4l = pd.DataFrame([
	[0.004]+glass,
	[0.003]+glass,
	[0.013]+air_brgd,
	[0.007]+glass],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

CLMu_wall_glasscurtain_3l = pd.DataFrame([
	[0.007]+glass,
	[0.013]+air_brgd,
	[0.007]+glass],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

CLMu_wall_glasscurtain_2l = pd.DataFrame([
	[0.007]+glass,
	combine([(0.013,air_brgd),(0.007,glass)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

combine([(0.007,glass),(0.013,air_brgd),(0.007,glass)])

##--------------------------------------------##

CLMu_wall_brickveneer_10l = pd.DataFrame([
	[0.050]+clay_brck,
	[0.039]+clay_brck,
	[0.025]+air,
	[0.050]+cncrt_blk,
	[0.050]+cncrt_blk,
	[0.050]+cncrt_blk,
	[0.050]+cncrt_blk,
	[0.013]+XPS,
	[0.012]+XPS,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(10)])

CLMu_wall_brickveneer_9l = pd.DataFrame([
	[0.050]+clay_brck,
	[0.039]+clay_brck,
	[0.025]+air,
	[0.050]+cncrt_blk,
	[0.050]+cncrt_blk,
	[0.050]+cncrt_blk,
	[0.050]+cncrt_blk,
	[0.025]+XPS,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(9)])

CLMu_wall_brickveneer_8l = pd.DataFrame([
	[0.050]+clay_brck,
	[0.039]+clay_brck,
	[0.025]+air,
	[0.060]+cncrt_blk,
	[0.070]+cncrt_blk,
	[0.070]+cncrt_blk,
	[0.025]+XPS,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(8)])

CLMu_wall_brickveneer_7l = pd.DataFrame([
	[0.050]+clay_brck,
	[0.039]+clay_brck,
	[0.025]+air,
	[0.100]+cncrt_blk,
	[0.100]+cncrt_blk,
	[0.025]+XPS,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_wall_brickveneer_6l = pd.DataFrame([
	[0.050]+clay_brck,
	[0.039]+clay_brck,
	[0.025]+air,
	[0.200]+cncrt_blk,
	[0.025]+XPS,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(6)])

CLMu_wall_brickveneer_5l = pd.DataFrame([
	[0.089]+clay_brck,
	[0.025]+air,
	[0.200]+cncrt_blk,
	[0.025]+XPS,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

CLMu_wall_brickveneer_4l = pd.DataFrame([
	[0.089]+clay_brck,
	[0.025]+air,
	[0.200]+cncrt_blk,
	combine([(0.025,XPS),(0.012,drywall)])], # combine XPS & drywall
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

CLMu_wall_brickveneer_3l = pd.DataFrame([
	[0.089]+clay_brck,
	[0.025]+air,
	combine([(0.200,cncrt_blk),(0.025,XPS),(0.012,drywall)])], # combine cncrt_blk & XPS & drywall
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

CLMu_wall_brickveneer_2l = pd.DataFrame([
	[0.089]+clay_brck,
	combine([(0.025,air),(0.200,cncrt_blk),(0.025,XPS),(0.012,drywall)])], # combine air & cncrt_blk & XPS & drywall
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

combine([(0.089,clay_brck),(0.025,air),(0.200,cncrt_blk),(0.025,XPS),(0.012,drywall)])

##--------------------------------------------##

CLMu_wall_stonecurtain_10l = pd.DataFrame([
	[0.015]+stone,
	[0.015]+stone,
	[0.020]+air,
	[0.050]+cncrt_blk,
	[0.050]+cncrt_blk,
	[0.050]+cncrt_blk,
	[0.050]+cncrt_blk,
	[0.013]+XPS,
	[0.012]+XPS,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(10)])

CLMu_wall_stonecurtain_9l = pd.DataFrame([
	[0.015]+stone,
	[0.015]+stone,
	[0.020]+air,
	[0.050]+cncrt_blk,
	[0.050]+cncrt_blk,
	[0.050]+cncrt_blk,
	[0.050]+cncrt_blk,
	[0.025]+XPS,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(9)])

CLMu_wall_stonecurtain_8l = pd.DataFrame([
	[0.015]+stone,
	[0.015]+stone,
	[0.020]+air,
	[0.060]+cncrt_blk,
	[0.070]+cncrt_blk,
	[0.070]+cncrt_blk,
	[0.025]+XPS,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(8)])

CLMu_wall_stonecurtain_7l = pd.DataFrame([
	[0.015]+stone,
	[0.015]+stone,
	[0.020]+air,
	[0.100]+cncrt_blk,
	[0.100]+cncrt_blk,
	[0.025]+XPS,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_wall_stonecurtain_6l = pd.DataFrame([
	[0.015]+stone,
	[0.015]+stone,
	[0.020]+air,
	[0.200]+cncrt_blk,
	[0.025]+XPS,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(6)])

CLMu_wall_stonecurtain_5l = pd.DataFrame([
	[0.030]+stone,
	[0.020]+air,
	[0.200]+cncrt_blk,
	[0.025]+XPS,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

CLMu_wall_stonecurtain_4l = pd.DataFrame([
	[0.030]+stone,
	[0.020]+air,
	[0.200]+cncrt_blk,
	combine([(0.025,XPS),(0.012,drywall)])], # combine XPS & drywall
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

CLMu_wall_stonecurtain_3l = pd.DataFrame([
	[0.030]+stone,
	[0.020]+air,
	combine([(0.200,cncrt_blk),(0.025,XPS),(0.012,drywall)])], # combine cncrt_blk & XPS & drywall
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

CLMu_wall_stonecurtain_2l = pd.DataFrame([
	[0.030]+stone,
	combine([(0.020,air),(0.200,cncrt_blk),(0.025,XPS),(0.012,drywall)])], # combine cncrt_blk & XPS & drywall
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

combine([(0.030,stone),(0.020,air),(0.200,cncrt_blk),(0.025,XPS),(0.012,drywall)])

##--------------------------------------------##

CLMu_wall_veneerbrickmasonry_10l = pd.DataFrame([
	[0.013]+plaster,
	[0.089]+clay_brck,
	[0.013]+air,
	[0.012]+air,
	[0.001]+bldg_ppr,
	[0.011]+ext_drwll,
	[0.050]+insulation,
	[0.039]+insulation,
	[0.006]+drywall,
	[0.006]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(10)])

CLMu_wall_veneerbrickmasonry_9l = pd.DataFrame([
	[0.013]+plaster,
	[0.089]+clay_brck,
	[0.013]+air,
	[0.012]+air,
	[0.001]+bldg_ppr,
	[0.011]+ext_drwll,
	[0.050]+insulation,
	[0.039]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(9)])

CLMu_wall_veneerbrickmasonry_8l = pd.DataFrame([
	[0.013]+plaster,
	[0.089]+clay_brck,
	[0.013]+air,
	[0.012]+air,
	[0.001]+bldg_ppr,
	[0.011]+ext_drwll,
	[0.089]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(8)])

CLMu_wall_veneerbrickmasonry_7l = pd.DataFrame([
	[0.013]+plaster,
	[0.089]+clay_brck,
	[0.025]+air,
	[0.001]+bldg_ppr,
	[0.011]+ext_drwll,
	[0.089]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_wall_veneerbrickmasonry_6l = pd.DataFrame([
	[0.013]+plaster,
	[0.089]+clay_brck,
	[0.025]+air,
	combine([(0.001,bldg_ppr),(0.011,ext_drwll)]),
	[0.089]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(6)])

CLMu_wall_veneerbrickmasonry_5l = pd.DataFrame([
	[0.013]+plaster,
	[0.089]+clay_brck,
	[0.025]+air,
	combine([(0.001,bldg_ppr),(0.011,ext_drwll)]),
	combine([(0.089,insulation),(0.012,drywall)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

CLMu_wall_veneerbrickmasonry_4l = pd.DataFrame([
	combine([(0.013,plaster),(0.089,clay_brck)]),
	[0.025]+air,
	combine([(0.001,bldg_ppr),(0.011,ext_drwll)]),
	combine([(0.089,insulation),(0.012,drywall)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

CLMu_wall_veneerbrickmasonry_3l = pd.DataFrame([
	combine([(0.013,plaster),(0.089,clay_brck)]),
	[0.025]+air,
	combine([(0.001,bldg_ppr),(0.011,ext_drwll),(0.089,insulation),(0.012,drywall)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

CLMu_wall_veneerbrickmasonry_2l = pd.DataFrame([
	combine([(0.013,plaster),(0.089,clay_brck)]),
	combine([(0.025,air),(0.001,bldg_ppr),(0.011,ext_drwll),(0.089,insulation),(0.012,drywall)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

combine([(0.013,plaster),(0.089,clay_brck),(0.025,air),(0.001,bldg_ppr),(0.011,ext_drwll),(0.089,insulation),(0.012,drywall)])

##--------------------------------------------##

CLMu_wall_veneerconcmasonry_10l = pd.DataFrame([
	[0.013]+plaster,
	[0.034]+cncrt_blk,
	[0.034]+cncrt_blk,
	[0.033]+cncrt_blk,
	[0.033]+cncrt_blk,
	[0.033]+cncrt_blk,
	[0.033]+cncrt_blk,
	[0.013]+XPS,
	[0.012]+XPS,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(10)])

CLMu_wall_veneerconcmasonry_9l = pd.DataFrame([
	[0.013]+plaster,
	[0.034]+cncrt_blk,
	[0.034]+cncrt_blk,
	[0.033]+cncrt_blk,
	[0.033]+cncrt_blk,
	[0.033]+cncrt_blk,
	[0.033]+cncrt_blk,
	[0.025]+XPS,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(9)])

CLMu_wall_veneerconcmasonry_8l = pd.DataFrame([
	[0.013]+plaster,
	[0.040]+cncrt_blk,
	[0.040]+cncrt_blk,
	[0.040]+cncrt_blk,
	[0.040]+cncrt_blk,
	[0.040]+cncrt_blk,
	[0.025]+XPS,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(8)])

CLMu_wall_veneerconcmasonry_7l = pd.DataFrame([
	[0.013]+plaster,
	[0.050]+cncrt_blk,
	[0.050]+cncrt_blk,
	[0.050]+cncrt_blk,
	[0.050]+cncrt_blk,
	[0.025]+XPS,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_wall_veneerconcmasonry_6l = pd.DataFrame([
	[0.013]+plaster,
	[0.070]+cncrt_blk,
	[0.070]+cncrt_blk,
	[0.060]+cncrt_blk,
	[0.025]+XPS,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(6)])

CLMu_wall_veneerconcmasonry_5l = pd.DataFrame([
	[0.013]+plaster,
	[0.100]+cncrt_blk,
	[0.100]+cncrt_blk,
	[0.025]+XPS,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

CLMu_wall_veneerconcmasonry_4l = pd.DataFrame([
	[0.013]+plaster,
	[0.200]+cncrt_blk,
	[0.025]+XPS,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

CLMu_wall_veneerconcmasonry_3l = pd.DataFrame([
	[0.013]+plaster,
	[0.200]+cncrt_blk,
	combine([(0.025,XPS),(0.012,drywall)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

CLMu_wall_veneerconcmasonry_2l = pd.DataFrame([
	combine([(0.013,plaster),(0.200,cncrt_blk)]),
	combine([(0.025,XPS),(0.012,drywall)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

combine([(0.013,plaster),(0.200,cncrt_blk),(0.025,XPS),(0.012,drywall)])

##--------------------------------------------##

CLMu_wall_EIFSfacade_10l = pd.DataFrame([
	[0.005]+EIFS,
	[0.038]+EPS,
	[0.010]+ext_drwll,
	[0.005]+ext_drwll,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.029]+insulation,
	[0.006]+drywall,
	[0.006]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(10)])

CLMu_wall_EIFSfacade_9l = pd.DataFrame([
	[0.005]+EIFS,
	[0.038]+EPS,
	[0.010]+ext_drwll,
	[0.005]+ext_drwll,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.029]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(9)])

CLMu_wall_EIFSfacade_8l = pd.DataFrame([
	[0.005]+EIFS,
	[0.038]+EPS,
	[0.010]+ext_drwll,
	[0.005]+ext_drwll,
	[0.030]+insulation,
	[0.030]+insulation,
	[0.029]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(8)])

CLMu_wall_EIFSfacade_7l = pd.DataFrame([
	[0.005]+EIFS,
	[0.038]+EPS,
	[0.010]+ext_drwll,
	[0.005]+ext_drwll,
	[0.045]+insulation,
	[0.044]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_wall_EIFSfacade_6l = pd.DataFrame([
	[0.005]+EIFS,
	[0.038]+EPS,
	[0.010]+ext_drwll,
	[0.005]+ext_drwll,
	[0.089]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(6)])

CLMu_wall_EIFSfacade_5l = pd.DataFrame([
	[0.005]+EIFS,
	[0.038]+EPS,
	[0.015]+ext_drwll,
	[0.089]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

CLMu_wall_EIFSfacade_4l = pd.DataFrame([
	[0.005]+EIFS,
	[0.038]+EPS,
	[0.015]+ext_drwll,
	combine([(0.089,insulation),(0.012,drywall)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

CLMu_wall_EIFSfacade_3l = pd.DataFrame([
	[0.005]+EIFS,
	[0.038]+EPS,
	combine([(0.015,ext_drwll),(0.089,insulation),(0.012,drywall)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

CLMu_wall_EIFSfacade_2l = pd.DataFrame([
	[0.005]+EIFS,
	combine([(0.038,EPS),(0.015,ext_drwll),(0.089,insulation),(0.012,drywall)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

combine([(0.005,EIFS),(0.038,EPS),(0.015,ext_drwll),(0.089,insulation),(0.012,drywall)])

##--------------------------------------------##

CLMu_wall_cementboard_10l = pd.DataFrame([
	[0.006]+cmnt_brd,
	[0.006]+cmnt_brd,
	[0.012]+air,
	[0.012]+fiberboard,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.029]+insulation,
	[0.006]+drywall,
	[0.006]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(10)])

CLMu_wall_cementboard_9l = pd.DataFrame([
	[0.006]+cmnt_brd,
	[0.006]+cmnt_brd,
	[0.012]+air,
	[0.012]+fiberboard,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.029]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(9)])

CLMu_wall_cementboard_8l = pd.DataFrame([
	[0.006]+cmnt_brd,
	[0.006]+cmnt_brd,
	[0.012]+air,
	[0.012]+fiberboard,
	[0.030]+insulation,
	[0.030]+insulation,
	[0.029]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(8)])

CLMu_wall_cementboard_7l = pd.DataFrame([
	[0.006]+cmnt_brd,
	[0.006]+cmnt_brd,
	[0.012]+air,
	[0.012]+fiberboard,
	[0.045]+insulation,
	[0.044]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_wall_cementboard_7l = pd.DataFrame([
	[0.006]+cmnt_brd,
	[0.006]+cmnt_brd,
	[0.012]+air,
	[0.012]+fiberboard,
	[0.045]+insulation,
	[0.044]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_wall_cementboard_6l = pd.DataFrame([
	[0.006]+cmnt_brd,
	[0.006]+cmnt_brd,
	[0.012]+air,
	[0.012]+fiberboard,
	[0.089]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(6)])

CLMu_wall_cementboard_5l = pd.DataFrame([
	[0.012]+cmnt_brd,
	[0.012]+air,
	[0.012]+fiberboard,
	[0.089]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

CLMu_wall_cementboard_4l = pd.DataFrame([
	[0.012]+cmnt_brd,
	[0.012]+air,
	combine([(0.012,fiberboard),(0.089,insulation)]),
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

CLMu_wall_cementboard_3l = pd.DataFrame([
	[0.012]+cmnt_brd,
	combine([(0.012,air),(0.012,fiberboard),(0.089,insulation)]),
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

CLMu_wall_cementboard_2l = pd.DataFrame([
	[0.012]+cmnt_brd,
	combine([(0.012,air),(0.012,fiberboard),(0.089,insulation),(0.012,drywall)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

combine([(0.012,cmnt_brd),(0.012,air),(0.012,fiberboard),(0.089,insulation),(0.012,drywall)])

##--------------------------------------------##

CLMu_wall_hardbrdsiding_10l = pd.DataFrame([
	[0.012]+hb_sid,
	[0.001]+air,
	[0.011]+OSB,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.009]+insulation,
	[0.006]+drywall,
	[0.006]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(10)])

CLMu_wall_hardbrdsiding_9l = pd.DataFrame([
	[0.012]+hb_sid,
	[0.001]+air,
	[0.011]+OSB,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.009]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(9)])

CLMu_wall_hardbrdsiding_8l = pd.DataFrame([
	[0.012]+hb_sid,
	[0.001]+air,
	[0.011]+OSB,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.029]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(8)])

CLMu_wall_hardbrdsiding_7l = pd.DataFrame([
	[0.012]+hb_sid,
	[0.001]+air,
	[0.011]+OSB,
	[0.030]+insulation,
	[0.030]+insulation,
	[0.029]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_wall_hardbrdsiding_6l = pd.DataFrame([
	[0.012]+hb_sid,
	[0.001]+air,
	[0.011]+OSB,
	[0.045]+insulation,
	[0.044]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(6)])

CLMu_wall_hardbrdsiding_5l = pd.DataFrame([
	[0.012]+hb_sid,
	[0.001]+air,
	[0.011]+OSB,
	[0.089]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

CLMu_wall_hardbrdsiding_4l = pd.DataFrame([
	[0.012]+hb_sid,
	combine([(0.001,air),(0.011,OSB)]),
	[0.089]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

CLMu_wall_hardbrdsiding_3l = pd.DataFrame([
	[0.012]+hb_sid,
	combine([(0.001,air),(0.011,OSB)]),
	combine([(0.089,insulation),(0.012,drywall)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

CLMu_wall_hardbrdsiding_2l = pd.DataFrame([
	[0.012]+hb_sid,
	combine([(0.001,air),(0.011,OSB),(0.089,insulation),(0.012,drywall)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

combine([(0.012,hb_sid),(0.001,air),(0.011,OSB),(0.089,insulation),(0.012,drywall)])

##--------------------------------------------##

CLMu_wall_woodsiding_10l = pd.DataFrame([
	[0.012]+wood_sid,
	[0.001]+air,
	[0.011]+plywood,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.009]+insulation,
	[0.006]+drywall,
	[0.006]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(10)])

CLMu_wall_woodsiding_9l = pd.DataFrame([
	[0.012]+wood_sid,
	[0.001]+air,
	[0.011]+plywood,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.009]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(9)])

CLMu_wall_woodsiding_8l = pd.DataFrame([
	[0.012]+wood_sid,
	[0.001]+air,
	[0.011]+plywood,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.029]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(8)])

CLMu_wall_woodsiding_7l = pd.DataFrame([
	[0.012]+wood_sid,
	[0.001]+air,
	[0.011]+plywood,
	[0.030]+insulation,
	[0.030]+insulation,
	[0.029]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_wall_woodsiding_6l = pd.DataFrame([
	[0.012]+wood_sid,
	[0.001]+air,
	[0.011]+plywood,
	[0.045]+insulation,
	[0.044]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(6)])

CLMu_wall_woodsiding_5l = pd.DataFrame([
	[0.012]+wood_sid,
	[0.001]+air,
	[0.011]+plywood,
	[0.089]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

CLMu_wall_woodsiding_4l = pd.DataFrame([
	[0.012]+wood_sid,
	[0.001]+air,
	[0.011]+plywood,
	combine([(0.089,insulation),(0.012,drywall)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

CLMu_wall_woodsiding_3l = pd.DataFrame([
	[0.012]+wood_sid,
	combine([(0.001,air),(0.011,plywood)]),
	combine([(0.089,insulation),(0.012,drywall)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

CLMu_wall_woodsiding_2l = pd.DataFrame([
	[0.012]+wood_sid,
	combine([(0.001,air),(0.011,plywood),(0.089,insulation),(0.012,drywall)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

combine([(0.012,wood_sid),(0.001,air),(0.011,plywood),(0.089,insulation),(0.012,drywall)])

##--------------------------------------------##

CLMu_wall_uninswoodsiding_10l = pd.DataFrame([
	[0.004]+wood_sid,
	[0.004]+wood_sid,
	[0.004]+wood_sid,
	[0.002]+wood_sid,
	[0.002]+wood_sid,
	[0.001]+air,
	[0.001]+air,
	[0.011]+plywood,
	[0.006]+drywall,
	[0.006]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(10)])

CLMu_wall_uninswoodsiding_9l = pd.DataFrame([
	[0.004]+wood_sid,
	[0.004]+wood_sid,
	[0.004]+wood_sid,
	[0.002]+wood_sid,
	[0.002]+wood_sid,
	[0.001]+air,
	[0.001]+air,
	[0.011]+plywood,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(9)])

CLMu_wall_uninswoodsiding_8l = pd.DataFrame([
	[0.004]+wood_sid,
	[0.004]+wood_sid,
	[0.004]+wood_sid,
	[0.002]+wood_sid,
	[0.002]+wood_sid,
	[0.002]+air,
	[0.011]+plywood,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(8)])

CLMu_wall_uninswoodsiding_7l = pd.DataFrame([
	[0.004]+wood_sid,
	[0.004]+wood_sid,
	[0.004]+wood_sid,
	[0.004]+wood_sid,
	[0.002]+air,
	[0.011]+plywood,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_wall_uninswoodsiding_6l = pd.DataFrame([
	[0.005]+wood_sid,
	[0.005]+wood_sid,
	[0.006]+wood_sid,
	[0.002]+air,
	[0.011]+plywood,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(6)])

CLMu_wall_uninswoodsiding_5l = pd.DataFrame([
	[0.008]+wood_sid,
	[0.008]+wood_sid,
	[0.002]+air,
	[0.011]+plywood,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

CLMu_wall_uninswoodsiding_4l = pd.DataFrame([
	[0.016]+wood_sid,
	[0.002]+air,
	[0.011]+plywood,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

CLMu_wall_uninswoodsiding_3l = pd.DataFrame([
	[0.016]+wood_sid,
	[0.002]+air,
	combine([(0.011,plywood),(0.012,drywall)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

CLMu_wall_uninswoodsiding_2l = pd.DataFrame([
	[0.016]+wood_sid,
	combine([(0.002,air),(0.011,plywood),(0.012,drywall)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

combine([(0.016,wood_sid),(0.002,air),(0.011,plywood),(0.012,drywall)])

##--------------------------------------------##

CLMu_wall_vinylsiding_10l = pd.DataFrame([
	[0.002]+vnyl_sid,
	[0.002]+air,
	[0.011]+OSB,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.009]+insulation,
	[0.006]+drywall,
	[0.006]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(10)])

CLMu_wall_vinylsiding_9l = pd.DataFrame([
	[0.002]+vnyl_sid,
	[0.002]+air,
	[0.011]+OSB,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.009]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(9)])

CLMu_wall_vinylsiding_8l = pd.DataFrame([
	[0.002]+vnyl_sid,
	[0.002]+air,
	[0.011]+OSB,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.029]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(8)])

CLMu_wall_vinylsiding_7l = pd.DataFrame([
	[0.002]+vnyl_sid,
	[0.002]+air,
	[0.011]+OSB,
	[0.030]+insulation,
	[0.030]+insulation,
	[0.029]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_wall_vinylsiding_6l = pd.DataFrame([
	[0.002]+vnyl_sid,
	[0.002]+air,
	[0.011]+OSB,
	[0.045]+insulation,
	[0.044]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(6)])

CLMu_wall_vinylsiding_5l = pd.DataFrame([
	[0.002]+vnyl_sid,
	[0.002]+air,
	[0.011]+OSB,
	[0.089]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

CLMu_wall_vinylsiding_4l = pd.DataFrame([
	[0.002]+vnyl_sid,
	combine([(0.002,air),(0.011,OSB)]),
	[0.089]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

CLMu_wall_vinylsiding_3l = pd.DataFrame([
	[0.002]+vnyl_sid,
	combine([(0.002,air),(0.011,OSB)]),
	combine([(0.089,insulation),(0.012,drywall)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

CLMu_wall_vinylsiding_2l = pd.DataFrame([
	[0.002]+vnyl_sid,
	combine([(0.002,air),(0.011,OSB),(0.089,insulation),(0.012,drywall)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

combine([(0.002,vnyl_sid),(0.002,air),(0.011,OSB),(0.089,insulation),(0.012,drywall)])

##--------------------------------------------##

CLMu_wall_woodstucco_10l = pd.DataFrame([
	[0.022]+stucco,
	[0.022]+stucco,
	[0.011]+plywood,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.009]+insulation,
	[0.006]+drywall,
	[0.006]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(10)])

CLMu_wall_woodstucco_9l = pd.DataFrame([
	[0.022]+stucco,
	[0.022]+stucco,
	[0.011]+plywood,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.009]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(9)])

CLMu_wall_woodstucco_8l = pd.DataFrame([
	[0.022]+stucco,
	[0.022]+stucco,
	[0.011]+plywood,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.029]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(8)])

CLMu_wall_woodstucco_7l = pd.DataFrame([
	[0.022]+stucco,
	[0.022]+stucco,
	[0.011]+plywood,
	[0.030]+insulation,
	[0.030]+insulation,
	[0.029]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_wall_woodstucco_6l = pd.DataFrame([
	[0.022]+stucco,
	[0.022]+stucco,
	[0.011]+plywood,
	[0.045]+insulation,
	[0.044]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(6)])

CLMu_wall_woodstucco_5l = pd.DataFrame([
	[0.022]+stucco,
	[0.022]+stucco,
	[0.011]+plywood,
	[0.089]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

CLMu_wall_woodstucco_4l = pd.DataFrame([
	[0.044]+stucco,
	[0.011]+plywood,
	[0.089]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

CLMu_wall_woodstucco_3l = pd.DataFrame([
	[0.044]+stucco,
	combine([(0.011,plywood),(0.089,insulation)]),
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

CLMu_wall_woodstucco_2l = pd.DataFrame([
	[0.044]+stucco,
	combine([(0.011,plywood),(0.089,insulation),(0.012,drywall)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

combine([(0.044,stucco),(0.011,plywood),(0.089,insulation),(0.012,drywall)])

##--------------------------------------------##

CLMu_wall_brickmasonryreinf_10l = pd.DataFrame([
	[0.050]+brck_reinfc,
	[0.050]+brck_reinfc,
	[0.013]+air,
	[0.012]+air,
	[0.001]+bldg_ppr,
	[0.011]+ext_drwll,
	[0.050]+insulation,
	[0.039]+insulation,
	[0.006]+drywall,
	[0.006]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(10)])

CLMu_wall_brickmasonryreinf_9l = pd.DataFrame([
	[0.050]+brck_reinfc,
	[0.050]+brck_reinfc,
	[0.013]+air,
	[0.012]+air,
	[0.001]+bldg_ppr,
	[0.011]+ext_drwll,
	[0.050]+insulation,
	[0.039]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(9)])

CLMu_wall_brickmasonryreinf_8l = pd.DataFrame([
	[0.050]+brck_reinfc,
	[0.050]+brck_reinfc,
	[0.013]+air,
	[0.012]+air,
	[0.001]+bldg_ppr,
	[0.011]+ext_drwll,
	[0.089]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(8)])

CLMu_wall_brickmasonryreinf_7l = pd.DataFrame([
	[0.050]+brck_reinfc,
	[0.050]+brck_reinfc,
	[0.025]+air,
	[0.001]+bldg_ppr,
	[0.011]+ext_drwll,
	[0.089]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_wall_brickmasonryreinf_6l = pd.DataFrame([
	[0.050]+brck_reinfc,
	[0.050]+brck_reinfc,
	[0.025]+air,
	combine([(0.001,bldg_ppr),(0.011,ext_drwll)]),
	[0.089]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(6)])

CLMu_wall_brickmasonryreinf_5l = pd.DataFrame([
	[0.100]+brck_reinfc,
	[0.025]+air,
	combine([(0.001,bldg_ppr),(0.011,ext_drwll)]),
	[0.089]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

CLMu_wall_brickmasonryreinf_4l = pd.DataFrame([
	[0.100]+brck_reinfc,
	[0.025]+air,
	combine([(0.001,bldg_ppr),(0.011,ext_drwll)]),
	combine([(0.089,insulation),(0.012,drywall)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

CLMu_wall_brickmasonryreinf_3l = pd.DataFrame([
	[0.100]+brck_reinfc,
	[0.025]+air,
	combine([(0.001,bldg_ppr),(0.011,ext_drwll),(0.089,insulation),(0.012,drywall)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

CLMu_wall_brickmasonryreinf_2l = pd.DataFrame([
	[0.100]+brck_reinfc,
	combine([(0.025,air),(0.001,bldg_ppr),(0.011,ext_drwll),(0.089,insulation),(0.012,drywall)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

combine([(0.100,brck_reinfc),(0.025,air),(0.001,bldg_ppr),(0.011,ext_drwll),(0.089,insulation),(0.012,drywall)])

##--------------------------------------------##

CLMu_wall_stone_10l = pd.DataFrame([
	[0.100]+stone,
	[0.100]+stone,
	[0.013]+air,
	[0.012]+air,
	[0.001]+bldg_ppr,
	[0.011]+ext_drwll,
	[0.050]+insulation,
	[0.039]+insulation,
	[0.006]+drywall,
	[0.006]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(10)])

CLMu_wall_stone_9l = pd.DataFrame([
	[0.100]+stone,
	[0.100]+stone,
	[0.013]+air,
	[0.012]+air,
	[0.001]+bldg_ppr,
	[0.011]+ext_drwll,
	[0.050]+insulation,
	[0.039]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(9)])

CLMu_wall_stone_8l = pd.DataFrame([
	[0.100]+stone,
	[0.100]+stone,
	[0.013]+air,
	[0.012]+air,
	[0.001]+bldg_ppr,
	[0.011]+ext_drwll,
	[0.089]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(8)])

CLMu_wall_stone_7l = pd.DataFrame([
	[0.100]+stone,
	[0.100]+stone,
	[0.025]+air,
	[0.001]+bldg_ppr,
	[0.011]+ext_drwll,
	[0.089]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_wall_stone_6l = pd.DataFrame([
	[0.100]+stone,
	[0.100]+stone,
	[0.025]+air,
	combine([(0.001,bldg_ppr),(0.011,ext_drwll)]),
	[0.089]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(6)])

CLMu_wall_stone_5l = pd.DataFrame([
	[0.200]+stone,
	[0.025]+air,
	combine([(0.001,bldg_ppr),(0.011,ext_drwll)]),
	[0.089]+insulation,
	[0.012]+drywall],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

CLMu_wall_stone_4l = pd.DataFrame([
	[0.200]+stone,
	[0.025]+air,
	combine([(0.001,bldg_ppr),(0.011,ext_drwll)]),
	combine([(0.089,insulation),(0.012,drywall)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

CLMu_wall_stone_3l = pd.DataFrame([
	[0.200]+stone,
	[0.025]+air,
	combine([(0.001,bldg_ppr),(0.011,ext_drwll),(0.089,insulation),(0.012,drywall)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

CLMu_wall_stone_2l = pd.DataFrame([
	[0.200]+stone,
	combine([(0.025,air),(0.001,bldg_ppr),(0.011,ext_drwll),(0.089,insulation),(0.012,drywall)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

combine([(0.200,stone),(0.025,air),(0.001,bldg_ppr),(0.011,ext_drwll),(0.089,insulation),(0.012,drywall)])

##--------------------------------------------##

CLMu_wall_concreteblocks_10l = pd.DataFrame([
	[0.020]+cncrt_blk,
	[0.020]+cncrt_blk,
	[0.020]+cncrt_blk,
	[0.020]+cncrt_blk,
	[0.020]+cncrt_blk,
	[0.020]+cncrt_blk,
	[0.020]+cncrt_blk,
	[0.020]+cncrt_blk,
	[0.020]+cncrt_blk,
	[0.020]+cncrt_blk],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(10)])

CLMu_wall_concreteblocks_9l = pd.DataFrame([
	[0.020]+cncrt_blk,
	[0.020]+cncrt_blk,
	[0.020]+cncrt_blk,
	[0.020]+cncrt_blk,
	[0.020]+cncrt_blk,
	[0.020]+cncrt_blk,
	[0.020]+cncrt_blk,
	[0.020]+cncrt_blk,
	[0.040]+cncrt_blk],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(9)])

CLMu_wall_concreteblocks_8l = pd.DataFrame([
	[0.020]+cncrt_blk,
	[0.020]+cncrt_blk,
	[0.020]+cncrt_blk,
	[0.020]+cncrt_blk,
	[0.020]+cncrt_blk,
	[0.020]+cncrt_blk,
	[0.040]+cncrt_blk,
	[0.040]+cncrt_blk],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(8)])

CLMu_wall_concreteblocks_7l = pd.DataFrame([
	[0.020]+cncrt_blk,
	[0.020]+cncrt_blk,
	[0.020]+cncrt_blk,
	[0.020]+cncrt_blk,
	[0.040]+cncrt_blk,
	[0.040]+cncrt_blk,
	[0.040]+cncrt_blk],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_wall_concreteblocks_6l = pd.DataFrame([
	[0.020]+cncrt_blk,
	[0.020]+cncrt_blk,
	[0.040]+cncrt_blk,
	[0.040]+cncrt_blk,
	[0.040]+cncrt_blk,
	[0.040]+cncrt_blk],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(6)])

CLMu_wall_concreteblocks_5l = pd.DataFrame([
	[0.040]+cncrt_blk,
	[0.040]+cncrt_blk,
	[0.040]+cncrt_blk,
	[0.040]+cncrt_blk,
	[0.040]+cncrt_blk],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

CLMu_wall_concreteblocks_4l = pd.DataFrame([
	[0.040]+cncrt_blk,
	[0.040]+cncrt_blk,
	[0.040]+cncrt_blk,
	[0.080]+cncrt_blk],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

CLMu_wall_concreteblocks_3l = pd.DataFrame([
	[0.040]+cncrt_blk,
	[0.080]+cncrt_blk,
	[0.080]+cncrt_blk],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

CLMu_wall_concreteblocks_2l = pd.DataFrame([
	[0.100]+cncrt_blk,
	[0.100]+cncrt_blk],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

[0.200]+cncrt_blk

##--------------------------------------------##

CLMu_wall_corrugatedmetal_10l = pd.DataFrame([
	[0.001]+metal,
	[0.001]+metal,
	[0.001]+metal,
	[0.001]+metal,
	[0.001]+metal,
	[0.001]+metal,
	[0.001]+metal,
	[0.001]+metal,
	[0.001]+metal,
	[0.001]+metal],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(10)])

CLMu_wall_corrugatedmetal_9l = pd.DataFrame([
	[0.001]+metal,
	[0.001]+metal,
	[0.001]+metal,
	[0.001]+metal,
	[0.001]+metal,
	[0.001]+metal,
	[0.001]+metal,
	[0.001]+metal,
	[0.002]+metal],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(9)])

CLMu_wall_corrugatedmetal_8l = pd.DataFrame([
	[0.001]+metal,
	[0.001]+metal,
	[0.001]+metal,
	[0.001]+metal,
	[0.001]+metal,
	[0.001]+metal,
	[0.002]+metal,
	[0.002]+metal],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(8)])

CLMu_wall_corrugatedmetal_7l = pd.DataFrame([
	[0.001]+metal,
	[0.001]+metal,
	[0.001]+metal,
	[0.001]+metal,
	[0.002]+metal,
	[0.002]+metal,
	[0.002]+metal],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_wall_corrugatedmetal_6l = pd.DataFrame([
	[0.001]+metal,
	[0.001]+metal,
	[0.002]+metal,
	[0.002]+metal,
	[0.002]+metal,
	[0.002]+metal],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(6)])

CLMu_wall_corrugatedmetal_5l = pd.DataFrame([
	[0.002]+metal,
	[0.002]+metal,
	[0.002]+metal,
	[0.002]+metal,
	[0.002]+metal],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

CLMu_wall_corrugatedmetal_4l = pd.DataFrame([
	[0.002]+metal,
	[0.002]+metal,
	[0.002]+metal,
	[0.004]+metal],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

CLMu_wall_corrugatedmetal_3l = pd.DataFrame([
	[0.002]+metal,
	[0.004]+metal,
	[0.004]+metal],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

CLMu_wall_corrugatedmetal_2l = pd.DataFrame([
	[0.005]+metal,
	[0.005]+metal],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

[0.010]+metal

##--------------------------------------------##

CLMu_wall_mudadobe_10l = pd.DataFrame([
	[0.025]+mudadobe,
	[0.025]+mudadobe,
	[0.025]+mudadobe,
	[0.025]+mudadobe,
	[0.025]+mudadobe,
	[0.025]+mudadobe,
	[0.025]+mudadobe,
	[0.025]+mudadobe,
	[0.025]+mudadobe,
	[0.025]+mudadobe],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(10)])

CLMu_wall_mudadobe_9l = pd.DataFrame([
	[0.025]+mudadobe,
	[0.025]+mudadobe,
	[0.025]+mudadobe,
	[0.025]+mudadobe,
	[0.025]+mudadobe,
	[0.025]+mudadobe,
	[0.025]+mudadobe,
	[0.025]+mudadobe,
	[0.050]+mudadobe],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(9)])

CLMu_wall_mudadobe_8l = pd.DataFrame([
	[0.025]+mudadobe,
	[0.025]+mudadobe,
	[0.025]+mudadobe,
	[0.025]+mudadobe,
	[0.025]+mudadobe,
	[0.025]+mudadobe,
	[0.050]+mudadobe,
	[0.050]+mudadobe],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(8)])

CLMu_wall_mudadobe_7l = pd.DataFrame([
	[0.025]+mudadobe,
	[0.025]+mudadobe,
	[0.025]+mudadobe,
	[0.025]+mudadobe,
	[0.050]+mudadobe,
	[0.050]+mudadobe,
	[0.050]+mudadobe],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_wall_mudadobe_6l = pd.DataFrame([
	[0.025]+mudadobe,
	[0.025]+mudadobe,
	[0.050]+mudadobe,
	[0.050]+mudadobe,
	[0.050]+mudadobe,
	[0.050]+mudadobe],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(6)])

CLMu_wall_mudadobe_5l = pd.DataFrame([
	[0.050]+mudadobe,
	[0.050]+mudadobe,
	[0.050]+mudadobe,
	[0.050]+mudadobe,
	[0.050]+mudadobe],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

CLMu_wall_mudadobe_4l = pd.DataFrame([
	[0.050]+mudadobe,
	[0.050]+mudadobe,
	[0.050]+mudadobe,
	[0.100]+mudadobe],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

CLMu_wall_mudadobe_3l = pd.DataFrame([
	[0.050]+mudadobe,
	[0.100]+mudadobe,
	[0.100]+mudadobe],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

CLMu_wall_mudadobe_2l = pd.DataFrame([
	[0.100]+mudadobe,
	[0.150]+mudadobe],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

[0.250]+mudadobe

##--------------------------------------------##

CLMu_wall_rubble_10l = pd.DataFrame([
	[0.100]+stone_lf,
	[0.100]+stone_lf,
	[0.100]+stone_lf,
	[0.100]+stone_lf,
	[0.050]+rubble,
	[0.050]+rubble,
	[0.100]+stone_lf,
	[0.100]+stone_lf,
	[0.100]+stone_lf,
	[0.100]+stone_lf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(10)])

CLMu_wall_rubble_9l = pd.DataFrame([
	[0.100]+stone_lf,
	[0.100]+stone_lf,
	[0.100]+stone_lf,
	[0.100]+stone_lf,
	[0.050]+rubble,
	[0.050]+rubble,
	[0.100]+stone_lf,
	[0.100]+stone_lf,
	[0.200]+stone_lf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(9)])

CLMu_wall_rubble_8l = pd.DataFrame([
	[0.100]+stone_lf,
	[0.100]+stone_lf,
	[0.100]+stone_lf,
	[0.100]+stone_lf,
	[0.050]+rubble,
	[0.050]+rubble,
	[0.200]+stone_lf,
	[0.200]+stone_lf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(8)])

CLMu_wall_rubble_7l = pd.DataFrame([
	[0.100]+stone_lf,
	[0.100]+stone_lf,
	[0.100]+stone_lf,
	[0.100]+stone_lf,
	[0.100]+rubble,
	[0.200]+stone_lf,
	[0.200]+stone_lf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_wall_rubble_6l = pd.DataFrame([
	[0.100]+stone_lf,
	[0.100]+stone_lf,
	[0.200]+stone_lf,
	[0.100]+rubble,
	[0.200]+stone_lf,
	[0.200]+stone_lf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(6)])

CLMu_wall_rubble_5l = pd.DataFrame([
	[0.200]+stone_lf,
	[0.200]+stone_lf,
	[0.100]+rubble,
	[0.200]+stone_lf,
	[0.200]+stone_lf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

CLMu_wall_rubble_4l = pd.DataFrame([
	[0.200]+stone_lf,
	[0.200]+stone_lf,
	[0.100]+rubble,
	[0.400]+stone_lf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

CLMu_wall_rubble_3l = pd.DataFrame([
	[0.400]+stone_lf,
	[0.100]+rubble,
	[0.400]+stone_lf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

CLMu_wall_rubble_2l = pd.DataFrame([
	[0.400]+stone_lf,
	combine([(0.100,rubble),(0.400,stone_lf)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

combine([(0.400,stone_lf),(0.100,rubble),(0.400,stone_lf)])

##--------------------------------------------##

CLMu_wall_galvsteel_10l = pd.DataFrame([
	[0.003]+galv_steel,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.004]+air_brgd_wf,
	[0.003]+air_brgd_wf,
	[0.004]+galv_steel,
	[0.004]+galv_steel],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(10)])

CLMu_wall_galvsteel_9l = pd.DataFrame([
	[0.003]+galv_steel,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.004]+air_brgd_wf,
	[0.003]+air_brgd_wf,
	[0.008]+galv_steel],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(9)])

CLMu_wall_galvsteel_8l = pd.DataFrame([
	[0.003]+galv_steel,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.007]+air_brgd_wf,
	[0.008]+galv_steel],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(8)])

CLMu_wall_galvsteel_7l = pd.DataFrame([
	[0.003]+galv_steel,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.020]+insulation,
	[0.040]+insulation,
	[0.007]+air_brgd_wf,
	[0.008]+galv_steel],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_wall_galvsteel_6l = pd.DataFrame([
	[0.003]+galv_steel,
	[0.020]+insulation,
	[0.040]+insulation,
	[0.040]+insulation,
	[0.007]+air_brgd_wf,
	[0.008]+galv_steel],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(6)])

CLMu_wall_galvsteel_5l = pd.DataFrame([
	[0.003]+galv_steel,
	[0.050]+insulation,
	[0.050]+insulation,
	[0.007]+air_brgd_wf,
	[0.008]+galv_steel],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

CLMu_wall_galvsteel_4l = pd.DataFrame([
	[0.003]+galv_steel,
	[0.100]+insulation,
	[0.007]+air_brgd_wf,
	[0.008]+galv_steel],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

CLMu_wall_galvsteel_3l = pd.DataFrame([
	[0.003]+galv_steel,
	combine([(0.100,insulation),(0.007,air_brgd_wf)]),
	[0.008]+galv_steel],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

CLMu_wall_galvsteel_2l = pd.DataFrame([
	[0.003]+galv_steel,
	combine([(0.100,insulation),(0.007,air_brgd_wf),(0.008,galv_steel)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

combine([(0.003,galv_steel),(0.100,insulation),(0.007,air_brgd_wf),(0.008,galv_steel)])

##----------------CLMu ROOFS----------------------------##

CLMu_roof_burconcrete_10l = pd.DataFrame([
	[0.007]+asphalt,
	[0.001]+felt,
	[0.002]+bitumen,
	[0.040]+ins_slab,
	[0.020]+ins_slab,
	[0.020]+ins_slab,
	[0.020]+ins_slab,
	[0.050]+cllr_cncrt,
	[0.050]+cllr_cncrt,
	[0.050]+cllr_cncrt],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(10)])

CLMu_roof_burconcrete_9l = pd.DataFrame([
	[0.007]+asphalt,
	[0.001]+felt,
	[0.002]+bitumen,
	[0.040]+ins_slab,
	[0.020]+ins_slab,
	[0.040]+ins_slab,
	[0.050]+cllr_cncrt,
	[0.050]+cllr_cncrt,
	[0.050]+cllr_cncrt],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(9)])

CLMu_roof_burconcrete_8l = pd.DataFrame([
	[0.007]+asphalt,
	[0.001]+felt,
	[0.002]+bitumen,
	[0.050]+ins_slab,
	[0.050]+ins_slab,
	[0.050]+cllr_cncrt,
	[0.050]+cllr_cncrt,
	[0.050]+cllr_cncrt],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(8)])

CLMu_roof_burconcrete_7l = pd.DataFrame([
	[0.007]+asphalt,
	[0.001]+felt,
	[0.002]+bitumen,
	[0.050]+ins_slab,
	[0.050]+ins_slab,
	[0.075]+cllr_cncrt,
	[0.075]+cllr_cncrt],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_roof_burconcrete_6l = pd.DataFrame([
	[0.007]+asphalt,
	[0.001]+felt,
	[0.002]+bitumen,
	[0.050]+ins_slab,
	[0.050]+ins_slab,
	[0.150]+cllr_cncrt],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(6)])

CLMu_roof_burconcrete_5l = pd.DataFrame([
	[0.007]+asphalt,
	[0.001]+felt,
	[0.002]+bitumen,
	[0.100]+ins_slab,
	[0.150]+cllr_cncrt],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

CLMu_roof_burconcrete_4l = pd.DataFrame([
	[0.007]+asphalt,
	combine([(0.001,felt),(0.002,bitumen)]),
	[0.100]+ins_slab,
	[0.150]+cllr_cncrt],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

CLMu_roof_burconcrete_3l = pd.DataFrame([
	combine([(0.007,asphalt),(0.001,felt),(0.002,bitumen)]),
	[0.100]+ins_slab,
	[0.150]+cllr_cncrt],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

# CLMu_roof_burconcrete_2l = pd.DataFrame([
# 	combine([(0.007,asphalt),(0.001,felt),(0.002,bitumen)]),
# 	combine([(0.100,ins_slab),(0.150,cllr_cncrt)])],
# 	columns = ['depth', 'conduct', 'heatc' ],
# 	index = ['layer'+str(x+1) for x in range(2)])

CLMu_roof_burconcrete_2l = pd.DataFrame([
	combine([(0.007,asphalt),(0.001,felt),(0.002,bitumen),(0.100,ins_slab)]),
	[0.150]+cllr_cncrt],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

combine([(0.007,asphalt),(0.001,felt),(0.002,bitumen),(0.100,ins_slab),(0.150,cllr_cncrt)])

##------------------------------------------##

CLMu_roof_burwood_10l = pd.DataFrame([
	[0.007]+gravel,
	[0.001]+felt,
	[0.002]+bitumen,
	[0.030]+ins_slab,
	[0.030]+ins_slab,
	[0.030]+ins_slab,
	[0.030]+ins_slab,
	[0.009]+plywood_rf,
	[0.005]+plywood_rf,
	[0.005]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(10)])

CLMu_roof_burwood_9l = pd.DataFrame([
	[0.007]+gravel,
	[0.001]+felt,
	[0.002]+bitumen,
	[0.030]+ins_slab,
	[0.030]+ins_slab,
	[0.030]+ins_slab,
	[0.030]+ins_slab,
	[0.009]+plywood_rf,
	[0.010]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(9)])

CLMu_roof_burwood_8l = pd.DataFrame([
	[0.007]+gravel,
	[0.001]+felt,
	[0.002]+bitumen,
	[0.030]+ins_slab,
	[0.030]+ins_slab,
	[0.060]+ins_slab,
	[0.009]+plywood_rf,
	[0.010]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(8)])

CLMu_roof_burwood_7l = pd.DataFrame([
	[0.007]+gravel,
	[0.001]+felt,
	[0.002]+bitumen,
	[0.060]+ins_slab,
	[0.060]+ins_slab,
	[0.009]+plywood_rf,
	[0.010]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_roof_burwood_6l = pd.DataFrame([
	[0.007]+gravel,
	[0.001]+felt,
	[0.002]+bitumen,
	[0.060]+ins_slab,
	[0.060]+ins_slab,
	[0.019]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(6)])

CLMu_roof_burwood_5l = pd.DataFrame([
	[0.007]+gravel,
	[0.001]+felt,
	[0.002]+bitumen,
	[0.120]+ins_slab,
	[0.019]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

CLMu_roof_burwood_4l = pd.DataFrame([
	[0.007]+gravel,
	combine([(0.001,felt),(0.002,bitumen)]),
	[0.120]+ins_slab,
	[0.019]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

CLMu_roof_burwood_3l = pd.DataFrame([
	combine([(0.007,gravel),(0.001,felt),(0.002,bitumen)]),
	[0.120]+ins_slab,
	[0.019]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

CLMu_roof_burwood_2l = pd.DataFrame([
	combine([(0.007,gravel),(0.001,felt),(0.002,bitumen)]),
	combine([(0.120,ins_slab),(0.019,plywood_rf)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

combine([(0.007,gravel),(0.001,felt),(0.002,bitumen),(0.120,ins_slab),(0.019,plywood_rf)])

##------------------------------------------##

CLMu_roof_pvcsteeldeck_10l = pd.DataFrame([
	[0.002]+pvceps,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.004]+air_brgd_rf,
	[0.003]+air_brgd_rf,
	[0.004]+steeldeck,
	[0.004]+steeldeck],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(10)])

CLMu_roof_pvcsteeldeck_9l = pd.DataFrame([
	[0.002]+pvceps,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.007]+air_brgd_rf,
	[0.004]+steeldeck,
	[0.004]+steeldeck],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(9)])

CLMu_roof_pvcsteeldeck_8l = pd.DataFrame([
	[0.002]+pvceps,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.007]+air_brgd_rf,
	[0.008]+steeldeck],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(8)])

CLMu_roof_pvcsteeldeck_7l = pd.DataFrame([
	[0.002]+pvceps,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.040]+insulation_rf,
	[0.007]+air_brgd_rf,
	[0.008]+steeldeck],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_roof_pvcsteeldeck_6l = pd.DataFrame([
	[0.002]+pvceps,
	[0.020]+insulation_rf,
	[0.040]+insulation_rf,
	[0.040]+insulation_rf,
	[0.007]+air_brgd_rf,
	[0.008]+steeldeck],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(6)])

CLMu_roof_pvcsteeldeck_5l = pd.DataFrame([
	[0.002]+pvceps,
	[0.040]+insulation_rf,
	[0.060]+insulation_rf,
	[0.007]+air_brgd_rf,
	[0.008]+steeldeck],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

CLMu_roof_pvcsteeldeck_4l = pd.DataFrame([
	[0.002]+pvceps,
	[0.100]+insulation_rf,
	[0.007]+air_brgd_rf,
	[0.008]+steeldeck],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

CLMu_roof_pvcsteeldeck_3l = pd.DataFrame([
	combine([(0.002,pvceps),(0.100,insulation_rf)]),
	[0.007]+air_brgd_rf,
	[0.008]+steeldeck],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

CLMu_roof_pvcsteeldeck_2l = pd.DataFrame([
	combine([(0.002,pvceps),(0.100,insulation_rf),(0.007,air_brgd_rf)]),
	[0.008]+steeldeck],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

combine([(0.002,pvceps),(0.100,insulation_rf),(0.007,air_brgd_rf),(0.008,steeldeck)])

##------------------------------------------##

CLMu_roof_epdmsteeldeck_10l = pd.DataFrame([
	[0.002]+epdm,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.004]+air_brgd_rf,
	[0.003]+air_brgd_rf,
	[0.004]+steeldeck,
	[0.004]+steeldeck],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(10)])

CLMu_roof_epdmsteeldeck_9l = pd.DataFrame([
	[0.002]+epdm,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.007]+air_brgd_rf,
	[0.004]+steeldeck,
	[0.004]+steeldeck],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(9)])

CLMu_roof_epdmsteeldeck_8l = pd.DataFrame([
	[0.002]+epdm,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.007]+air_brgd_rf,
	[0.008]+steeldeck],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(8)])

CLMu_roof_epdmsteeldeck_7l = pd.DataFrame([
	[0.002]+epdm,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.040]+insulation_rf,
	[0.007]+air_brgd_rf,
	[0.008]+steeldeck],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_roof_epdmsteeldeck_6l = pd.DataFrame([
	[0.002]+epdm,
	[0.020]+insulation_rf,
	[0.040]+insulation_rf,
	[0.040]+insulation_rf,
	[0.007]+air_brgd_rf,
	[0.008]+steeldeck],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(6)])

CLMu_roof_epdmsteeldeck_5l = pd.DataFrame([
	[0.002]+epdm,
	[0.040]+insulation_rf,
	[0.060]+insulation_rf,
	[0.007]+air_brgd_rf,
	[0.008]+steeldeck],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

CLMu_roof_epdmsteeldeck_4l = pd.DataFrame([
	[0.002]+epdm,
	[0.100]+insulation_rf,
	[0.007]+air_brgd_rf,
	[0.008]+steeldeck],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

CLMu_roof_epdmsteeldeck_3l = pd.DataFrame([
	combine([(0.002,epdm),(0.100,insulation_rf)]),
	[0.007]+air_brgd_rf,
	[0.008]+steeldeck],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

CLMu_roof_epdmsteeldeck_2l = pd.DataFrame([
	combine([(0.002,epdm),(0.100,insulation_rf),(0.007,air_brgd_rf)]),
	[0.008]+steeldeck],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

combine([(0.002,epdm),(0.100,insulation_rf),(0.007,air_brgd_rf),(0.008,steeldeck)])

##------------------------------------------##

CLMu_roof_galvsteeldeck_10l = pd.DataFrame([
	[0.003]+galvsteel_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.004]+air_brgd_rf,
	[0.003]+air_brgd_rf,
	[0.004]+steeldeck,
	[0.004]+steeldeck],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(10)])

CLMu_roof_galvsteeldeck_9l = pd.DataFrame([
	[0.003]+galvsteel_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.007]+air_brgd_rf,
	[0.004]+steeldeck,
	[0.004]+steeldeck],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(9)])

CLMu_roof_galvsteeldeck_8l = pd.DataFrame([
	[0.003]+galvsteel_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.007]+air_brgd_rf,
	[0.008]+steeldeck],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(8)])

CLMu_roof_galvsteeldeck_7l = pd.DataFrame([
	[0.003]+galvsteel_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.020]+insulation_rf,
	[0.040]+insulation_rf,
	[0.007]+air_brgd_rf,
	[0.008]+steeldeck],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_roof_galvsteeldeck_6l = pd.DataFrame([
	[0.003]+galvsteel_rf,
	[0.020]+insulation_rf,
	[0.040]+insulation_rf,
	[0.040]+insulation_rf,
	[0.007]+air_brgd_rf,
	[0.008]+steeldeck],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(6)])

CLMu_roof_galvsteeldeck_5l = pd.DataFrame([
	[0.003]+galvsteel_rf,
	[0.040]+insulation_rf,
	[0.060]+insulation_rf,
	[0.007]+air_brgd_rf,
	[0.008]+steeldeck],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

CLMu_roof_galvsteeldeck_4l = pd.DataFrame([
	[0.003]+galvsteel_rf,
	[0.100]+insulation_rf,
	[0.007]+air_brgd_rf,
	[0.008]+steeldeck],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

CLMu_roof_galvsteeldeck_3l = pd.DataFrame([
	[0.003]+galvsteel_rf,
	combine([(0.100,insulation_rf),(0.007,air_brgd_rf)]),
	[0.008]+steeldeck],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

CLMu_roof_galvsteeldeck_2l = pd.DataFrame([
	[0.003]+galvsteel_rf,
	combine([(0.100,insulation_rf),(0.007,air_brgd_rf),(0.008,steeldeck)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

combine([(0.003,galvsteel_rf),(0.100,insulation_rf),(0.007,air_brgd_rf),(0.008,steeldeck)])

##------------------------------------------##

CLMu_roof_corrmetal_10l = pd.DataFrame([
	[0.001]+metal_rf,
	[0.001]+metal_rf,
	[0.001]+metal_rf,
	[0.001]+metal_rf,
	[0.001]+metal_rf,
	[0.001]+metal_rf,
	[0.001]+metal_rf,
	[0.001]+metal_rf,
	[0.001]+metal_rf,
	[0.001]+metal_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(10)])

CLMu_roof_corrmetal_9l = pd.DataFrame([
	[0.001]+metal_rf,
	[0.001]+metal_rf,
	[0.001]+metal_rf,
	[0.001]+metal_rf,
	[0.001]+metal_rf,
	[0.001]+metal_rf,
	[0.001]+metal_rf,
	[0.001]+metal_rf,
	[0.002]+metal_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(9)])

CLMu_roof_corrmetal_8l = pd.DataFrame([
	[0.001]+metal_rf,
	[0.001]+metal_rf,
	[0.001]+metal_rf,
	[0.001]+metal_rf,
	[0.001]+metal_rf,
	[0.001]+metal_rf,
	[0.002]+metal_rf,
	[0.002]+metal_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(8)])

CLMu_roof_corrmetal_7l = pd.DataFrame([
	[0.001]+metal_rf,
	[0.001]+metal_rf,
	[0.001]+metal_rf,
	[0.001]+metal_rf,
	[0.002]+metal_rf,
	[0.002]+metal_rf,
	[0.002]+metal_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_roof_corrmetal_6l = pd.DataFrame([
	[0.001]+metal_rf,
	[0.001]+metal_rf,
	[0.002]+metal_rf,
	[0.002]+metal_rf,
	[0.002]+metal_rf,
	[0.002]+metal_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(6)])

CLMu_roof_corrmetal_5l = pd.DataFrame([
	[0.002]+metal_rf,
	[0.002]+metal_rf,
	[0.002]+metal_rf,
	[0.002]+metal_rf,
	[0.002]+metal_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

CLMu_roof_corrmetal_4l = pd.DataFrame([
	[0.002]+metal_rf,
	[0.002]+metal_rf,
	[0.002]+metal_rf,
	[0.004]+metal_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

CLMu_roof_corrmetal_3l = pd.DataFrame([
	[0.002]+metal_rf,
	[0.004]+metal_rf,
	[0.004]+metal_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

CLMu_roof_corrmetal_2l = pd.DataFrame([
	[0.005]+metal_rf,
	[0.005]+metal_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

combine([(0.01,metal_rf)])

##------------------------------------------##

CLMu_roof_shingles_10l = pd.DataFrame([
	[0.005]+shingle,
	[0.001]+felt,
	[0.003]+plywood_rf,
	[0.003]+plywood_rf,
	[0.003]+plywood_rf,
	[0.003]+plywood_rf,
	[0.003]+plywood_rf,
	[0.003]+plywood_rf,
	[0.003]+plywood_rf,
	[0.003]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(10)])

CLMu_roof_shingles_9l = pd.DataFrame([
	[0.005]+shingle,
	[0.001]+felt,
	[0.003]+plywood_rf,
	[0.003]+plywood_rf,
	[0.003]+plywood_rf,
	[0.003]+plywood_rf,
	[0.003]+plywood_rf,
	[0.003]+plywood_rf,
	[0.006]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(9)])

CLMu_roof_shingles_8l = pd.DataFrame([
	[0.005]+shingle,
	[0.001]+felt,
	[0.003]+plywood_rf,
	[0.003]+plywood_rf,
	[0.003]+plywood_rf,
	[0.003]+plywood_rf,
	[0.006]+plywood_rf,
	[0.006]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(8)])

CLMu_roof_shingles_7l = pd.DataFrame([
	[0.005]+shingle,
	[0.001]+felt,
	[0.003]+plywood_rf,
	[0.003]+plywood_rf,
	[0.006]+plywood_rf,
	[0.006]+plywood_rf,
	[0.006]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_roof_shingles_6l = pd.DataFrame([
	[0.005]+shingle,
	[0.001]+felt,
	[0.006]+plywood_rf,
	[0.006]+plywood_rf,
	[0.006]+plywood_rf,
	[0.006]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(6)])

CLMu_roof_shingles_5l = pd.DataFrame([
	[0.005]+shingle,
	[0.001]+felt,
	[0.008]+plywood_rf,
	[0.008]+plywood_rf,
	[0.008]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

CLMu_roof_shingles_4l = pd.DataFrame([
	[0.005]+shingle,
	[0.001]+felt,
	[0.012]+plywood_rf,
	[0.012]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

CLMu_roof_shingles_3l = pd.DataFrame([
	[0.005]+shingle,
	[0.001]+felt,
	[0.024]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

CLMu_roof_shingles_2l = pd.DataFrame([
	[0.005]+shingle,
	combine([(0.001,felt),(0.024,plywood_rf)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

combine([(0.005,shingle),(0.001,felt),(0.024,plywood_rf)])

##------------------------------------------##

CLMu_roof_ceramictiles_10l = pd.DataFrame([
	[0.004]+cer_tile,
	[0.003]+cer_tile,
	[0.003]+cer_tile,
	[0.003]+air_rf,
	[0.004]+plywood_rf,
	[0.004]+plywood_rf,
	[0.004]+plywood_rf,
	[0.004]+plywood_rf,
	[0.003]+plywood_rf,
	[0.003]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(10)])

CLMu_roof_ceramictiles_9l = pd.DataFrame([
	[0.004]+cer_tile,
	[0.003]+cer_tile,
	[0.003]+cer_tile,
	[0.003]+air_rf,
	[0.004]+plywood_rf,
	[0.004]+plywood_rf,
	[0.004]+plywood_rf,
	[0.004]+plywood_rf,
	[0.006]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(9)])

CLMu_roof_ceramictiles_8l = pd.DataFrame([
	[0.004]+cer_tile,
	[0.003]+cer_tile,
	[0.003]+cer_tile,
	[0.003]+air_rf,
	[0.004]+plywood_rf,
	[0.004]+plywood_rf,
	[0.008]+plywood_rf,
	[0.006]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(8)])

CLMu_roof_ceramictiles_7l = pd.DataFrame([
	[0.004]+cer_tile,
	[0.003]+cer_tile,
	[0.003]+cer_tile,
	[0.003]+air_rf,
	[0.008]+plywood_rf,
	[0.008]+plywood_rf,
	[0.006]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_roof_ceramictiles_6l = pd.DataFrame([
	[0.004]+cer_tile,
	[0.003]+cer_tile,
	[0.003]+cer_tile,
	[0.003]+air_rf,
	[0.011]+plywood_rf,
	[0.011]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(6)])

CLMu_roof_ceramictiles_5l = pd.DataFrame([
	[0.005]+cer_tile,
	[0.005]+cer_tile,
	[0.003]+air_rf,
	[0.011]+plywood_rf,
	[0.011]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

CLMu_roof_ceramictiles_4l = pd.DataFrame([
	[0.005]+cer_tile,
	[0.005]+cer_tile,
	[0.003]+air_rf,
	[0.022]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

CLMu_roof_ceramictiles_3l = pd.DataFrame([
	[0.010]+cer_tile,
	[0.003]+air_rf,
	[0.022]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

CLMu_roof_ceramictiles_2l = pd.DataFrame([
	[0.010]+cer_tile,
	combine([(0.003,air_rf),(0.022,plywood_rf)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

combine([(0.010,cer_tile),(0.003,air_rf),(0.022,plywood_rf)])

##------------------------------------------##

CLMu_roof_thatch_10l = pd.DataFrame([
	[0.02]+thatch,
	[0.02]+thatch,
	[0.02]+thatch,
	[0.02]+thatch,
	[0.02]+thatch,
	[0.02]+thatch,
	[0.02]+thatch,
	[0.02]+thatch,
	[0.01]+timber,
	[0.01]+timber],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(10)])

CLMu_roof_thatch_9l = pd.DataFrame([
	[0.02]+thatch,
	[0.02]+thatch,
	[0.02]+thatch,
	[0.02]+thatch,
	[0.02]+thatch,
	[0.02]+thatch,
	[0.02]+thatch,
	[0.02]+thatch,
	[0.02]+timber],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(9)])

CLMu_roof_thatch_8l = pd.DataFrame([
	[0.02]+thatch,
	[0.02]+thatch,
	[0.02]+thatch,
	[0.02]+thatch,
	[0.02]+thatch,
	[0.02]+thatch,
	[0.04]+thatch,
	[0.02]+timber],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(8)])

CLMu_roof_thatch_7l = pd.DataFrame([
	[0.02]+thatch,
	[0.02]+thatch,
	[0.02]+thatch,
	[0.02]+thatch,
	[0.04]+thatch,
	[0.04]+thatch,
	[0.02]+timber],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_roof_thatch_6l = pd.DataFrame([
	[0.02]+thatch,
	[0.02]+thatch,
	[0.04]+thatch,
	[0.04]+thatch,
	[0.04]+thatch,
	[0.02]+timber],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(6)])

CLMu_roof_thatch_5l = pd.DataFrame([
	[0.04]+thatch,
	[0.04]+thatch,
	[0.04]+thatch,
	[0.04]+thatch,
	[0.02]+timber],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

CLMu_roof_thatch_4l = pd.DataFrame([
	[0.05]+thatch,
	[0.05]+thatch,
	[0.06]+thatch,
	[0.02]+timber],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

CLMu_roof_thatch_3l = pd.DataFrame([
	[0.08]+thatch,
	[0.08]+thatch,
	[0.02]+timber],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

CLMu_roof_thatch_2l = pd.DataFrame([
	[0.16]+thatch,
	[0.02]+timber],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

combine([(0.16,thatch),(0.02,timber)])

##------------------------------------------##

CLMu_roof_slatetile_10l = pd.DataFrame([
	[0.007]+slate,
	[0.003]+air_brgd_rf,
	[0.003]+plywood_rf, 
	[0.003]+plywood_rf,
	[0.003]+plywood_rf,
	[0.003]+plywood_rf,
	[0.003]+plywood_rf,
	[0.003]+plywood_rf,
	[0.002]+plywood_rf,
	[0.002]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(10)])

CLMu_roof_slatetile_9l = pd.DataFrame([
	[0.007]+slate,
	[0.003]+air_brgd_rf,
	[0.003]+plywood_rf, 
	[0.003]+plywood_rf,
	[0.003]+plywood_rf,
	[0.003]+plywood_rf,
	[0.003]+plywood_rf,
	[0.003]+plywood_rf,
	[0.004]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(9)])

CLMu_roof_slatetile_8l = pd.DataFrame([
	[0.007]+slate,
	[0.003]+air_brgd_rf,
	[0.003]+plywood_rf, 
	[0.003]+plywood_rf,
	[0.003]+plywood_rf,
	[0.003]+plywood_rf,
	[0.006]+plywood_rf,
	[0.004]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(8)])

CLMu_roof_slatetile_7l = pd.DataFrame([
	[0.007]+slate,
	[0.003]+air_brgd_rf,
	[0.003]+plywood_rf, 
	[0.003]+plywood_rf,
	[0.006]+plywood_rf,
	[0.006]+plywood_rf,
	[0.004]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_roof_slatetile_6l = pd.DataFrame([
	[0.007]+slate,
	[0.003]+air_brgd_rf,
	[0.006]+plywood_rf, 
	[0.006]+plywood_rf,
	[0.006]+plywood_rf,
	[0.004]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(6)])

CLMu_roof_slatetile_5l = pd.DataFrame([
	[0.007]+slate,
	[0.003]+air_brgd_rf,
	[0.006]+plywood_rf, 
	[0.008]+plywood_rf,
	[0.008]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

CLMu_roof_slatetile_4l = pd.DataFrame([
	[0.007]+slate,
	[0.003]+air_brgd_rf,
	[0.011]+plywood_rf, 
	[0.011]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

CLMu_roof_slatetile_3l = pd.DataFrame([
	[0.007]+slate,
	[0.003]+air_brgd_rf,
	[0.022]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

CLMu_roof_slatetile_2l = pd.DataFrame([
	[0.007]+slate,
	combine([(0.003,air_brgd_rf),(0.022,plywood_rf)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

combine([(0.007,slate),(0.003,air_brgd_rf),(0.022,plywood_rf)])

##------------------------------------------##

CLMu_roof_metaltile_10l = pd.DataFrame([
	[0.001]+galvsteel_rf,
	[0.001]+felt,
	[0.003]+plywood_rf, 
	[0.003]+plywood_rf,
	[0.003]+plywood_rf,
	[0.003]+plywood_rf,
	[0.003]+plywood_rf,
	[0.003]+plywood_rf,
	[0.003]+plywood_rf,
	[0.003]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(10)])

CLMu_roof_metaltile_9l = pd.DataFrame([
	[0.001]+galvsteel_rf,
	[0.001]+felt,
	[0.003]+plywood_rf, 
	[0.003]+plywood_rf,
	[0.003]+plywood_rf,
	[0.003]+plywood_rf,
	[0.004]+plywood_rf,
	[0.004]+plywood_rf,
	[0.004]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(9)])

CLMu_roof_metaltile_8l = pd.DataFrame([
	[0.001]+galvsteel_rf,
	[0.001]+felt,
	[0.004]+plywood_rf, 
	[0.004]+plywood_rf,
	[0.004]+plywood_rf,
	[0.004]+plywood_rf,
	[0.004]+plywood_rf,
	[0.004]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(8)])

CLMu_roof_metaltile_7l = pd.DataFrame([
	[0.001]+galvsteel_rf,
	[0.001]+felt,
	[0.004]+plywood_rf, 
	[0.005]+plywood_rf,
	[0.005]+plywood_rf,
	[0.005]+plywood_rf,
	[0.005]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_roof_metaltile_6l = pd.DataFrame([
	[0.001]+galvsteel_rf,
	[0.001]+felt,
	[0.006]+plywood_rf, 
	[0.006]+plywood_rf,
	[0.006]+plywood_rf,
	[0.006]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(6)])

CLMu_roof_metaltile_5l = pd.DataFrame([
	[0.001]+galvsteel_rf,
	[0.001]+felt,
	[0.008]+plywood_rf, 
	[0.008]+plywood_rf,
	[0.008]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

CLMu_roof_metaltile_4l = pd.DataFrame([
	[0.001]+galvsteel_rf,
	[0.001]+felt,
	[0.012]+plywood_rf, 
	[0.012]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

CLMu_roof_metaltile_3l = pd.DataFrame([
	[0.001]+galvsteel_rf,
	[0.001]+felt,
	[0.024]+plywood_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

CLMu_roof_metaltile_2l = pd.DataFrame([
	[0.001]+galvsteel_rf,
	combine([(0.001,felt),(0.024,plywood_rf)])],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

combine([(0.001,galvsteel_rf),(0.001,felt),(0.024,plywood_rf)])

##--------------------------------------------##

CLMu_roof_mudadobe_10l = pd.DataFrame([
	[0.01]+mudadobe_rf,
	[0.01]+mudadobe_rf,
	[0.01]+mudadobe_rf,
	[0.01]+mudadobe_rf,
	[0.01]+mudadobe_rf,
	[0.01]+mudadobe_rf,
	[0.01]+mudadobe_rf,
	[0.01]+mudadobe_rf,
	[0.002]+metal_rf,
	[0.002]+metal_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(10)])

CLMu_roof_mudadobe_9l = pd.DataFrame([
	[0.01]+mudadobe_rf,
	[0.01]+mudadobe_rf,
	[0.01]+mudadobe_rf,
	[0.01]+mudadobe_rf,
	[0.01]+mudadobe_rf,
	[0.01]+mudadobe_rf,
	[0.01]+mudadobe_rf,
	[0.01]+mudadobe_rf,
	[0.004]+metal_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(9)])

CLMu_roof_mudadobe_8l = pd.DataFrame([
	[0.01]+mudadobe_rf,
	[0.01]+mudadobe_rf,
	[0.01]+mudadobe_rf,
	[0.01]+mudadobe_rf,
	[0.01]+mudadobe_rf,
	[0.01]+mudadobe_rf,
	[0.02]+mudadobe_rf,
	[0.004]+metal_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(8)])

CLMu_roof_mudadobe_7l = pd.DataFrame([
	[0.01]+mudadobe_rf,
	[0.01]+mudadobe_rf,
	[0.01]+mudadobe_rf,
	[0.01]+mudadobe_rf,
	[0.02]+mudadobe_rf,
	[0.02]+mudadobe_rf,
	[0.004]+metal_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(7)])

CLMu_roof_mudadobe_6l = pd.DataFrame([
	[0.01]+mudadobe_rf,
	[0.01]+mudadobe_rf,
	[0.02]+mudadobe_rf,
	[0.02]+mudadobe_rf,
	[0.02]+mudadobe_rf,
	[0.004]+metal_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(6)])

CLMu_roof_mudadobe_5l = pd.DataFrame([
	[0.02]+mudadobe_rf,
	[0.02]+mudadobe_rf,
	[0.02]+mudadobe_rf,
	[0.02]+mudadobe_rf,
	[0.004]+metal_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(5)])

CLMu_roof_mudadobe_4l = pd.DataFrame([
	[0.02]+mudadobe_rf,
	[0.03]+mudadobe_rf,
	[0.03]+mudadobe_rf,
	[0.004]+metal_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(4)])

CLMu_roof_mudadobe_3l = pd.DataFrame([
	[0.04]+mudadobe_rf,
	[0.04]+mudadobe_rf,
	[0.004]+metal_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(3)])

CLMu_roof_mudadobe_2l = pd.DataFrame([
	[0.08]+mudadobe_rf,
	[0.004]+metal_rf],
	columns = ['depth', 'conduct', 'heatc' ],
	index = ['layer'+str(x+1) for x in range(2)])

combine([(0.08,mudadobe_rf),(0.004,metal_rf)])
