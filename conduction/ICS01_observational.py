print('''
	Purpose: 	Import urban model data and conform to PILPS-urban methodology (Grimmond et al., 2011), compare with observations, analyse and plot results.
	Developer: 	Mathew Lipson <m.lipson@unsw.edu.au>
	Manuscript: Efficiently modelling urban heat storage: an interface conduction scheme in an urban land surface model (aTEB v2.0)
	Journal: 	Geoscientific Model Development
	Revision: 	12 January 2017 (to incorporate peer review revision)
	Note: 		Observational and anthropogenic data must be aquired from owner Andrew Coutts <andrew.coutts@monash.edu>. Files include:
				- alpha01.dat (used as forcing data for the urban model, and observation quality flags)
				- observation_preston.csv (flux observations to assess model performance)
				- QF.txt (estimates of anthropogenic heat sources)''')

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn.apionly as sns
import pickle
from scipy import stats
import time 		# for current time
import os 			# for current filename
import zipfile 		# to unzip archive
from taylordiagram import TaylorDiagram

import functions
scriptname = str(os.path.basename(__file__))  # define current script filename

# ## -------------user input---------------- ##
harman = input('Figure 6 or 7? (Fig. 6 & Table 2 use Harman experiments only. Fig.7 and Table 3 use all)')
if harman not in ['6','7']:
	harman = input('Input must be either 6 or 7:')
	assert plotfig in ['6','7'], "Input not valid, aborting."
harman = harman in ['6']
# ## --------------------------------------- ##

site = 'preston'
sitepath = '../ateb/'
plotpath = '../figures/'
prefix='ICS01-V28_pres'
sitelist = ['SITEds_su0','WRFds_su0','UZEds_su0','aTEBds_su0']

# experiment parameters
zc='0.01' 	# canyon roughness
zr='0.100'	# roof rougness
zv='0.250'	# veg rougness

if harman:
	### Figure 6 & Table 2 expermiment list (Harman experiments only)
	halflist  = ['%s_%s_cm0_rm1_zm0_zc%s_zr%s_zv%s' %(prefix,sitelist[0],zc,zr,zv),'%s_%s_cm0_rm1_zm0_zc%s_zr%s_zv%s' %(prefix,sitelist[1],zc,zr,zv),'%s_%s_cm0_rm1_zm0_zc%s_zr%s_zv%s' %(prefix,sitelist[2],zc,zr,zv),'%s_%s_cm0_rm1_zm0_zc%s_zr%s_zv%s' %(prefix,sitelist[3],zc,zr,zv)]
	interlist = ['%s_%s_cm1_rm1_zm0_zc%s_zr%s_zv%s' %(prefix,sitelist[0],zc,zr,zv),'%s_%s_cm1_rm1_zm0_zc%s_zr%s_zv%s' %(prefix,sitelist[1],zc,zr,zv),'%s_%s_cm1_rm1_zm0_zc%s_zr%s_zv%s' %(prefix,sitelist[2],zc,zr,zv),'%s_%s_cm1_rm1_zm0_zc%s_zr%s_zv%s' %(prefix,sitelist[3],zc,zr,zv)]
else:
	### Fig 7 & Table 3 experiment list (full)
	halflist  = [
				'%s_%s_cm0_rm0_zm0_zc%s_zr%s_zv%s' %(prefix,sitelist[0],zc,zr,zv),'%s_%s_cm0_rm0_zm0_zc%s_zr%s_zv%s' %(prefix,sitelist[1],zc,zr,zv),'%s_%s_cm0_rm0_zm0_zc%s_zr%s_zv%s' %(prefix,sitelist[2],zc,zr,zv),'%s_%s_cm0_rm0_zm0_zc%s_zr%s_zv%s' %(prefix,sitelist[3],zc,zr,zv),
				'%s_%s_cm0_rm1_zm0_zc%s_zr%s_zv%s' %(prefix,sitelist[0],zc,zr,zv),'%s_%s_cm0_rm1_zm0_zc%s_zr%s_zv%s' %(prefix,sitelist[1],zc,zr,zv),'%s_%s_cm0_rm1_zm0_zc%s_zr%s_zv%s' %(prefix,sitelist[2],zc,zr,zv),'%s_%s_cm0_rm1_zm0_zc%s_zr%s_zv%s' %(prefix,sitelist[3],zc,zr,zv),
				'%s_%s_cm0_rm2_zm0_zc%s_zr%s_zv%s' %(prefix,sitelist[0],zc,zr,zv),'%s_%s_cm0_rm2_zm0_zc%s_zr%s_zv%s' %(prefix,sitelist[1],zc,zr,zv),'%s_%s_cm0_rm2_zm0_zc%s_zr%s_zv%s' %(prefix,sitelist[2],zc,zr,zv),'%s_%s_cm0_rm2_zm0_zc%s_zr%s_zv%s' %(prefix,sitelist[3],zc,zr,zv)
				]
	interlist = [
				'%s_%s_cm1_rm0_zm0_zc%s_zr%s_zv%s' %(prefix,sitelist[0],zc,zr,zv),'%s_%s_cm1_rm0_zm0_zc%s_zr%s_zv%s' %(prefix,sitelist[1],zc,zr,zv),'%s_%s_cm1_rm0_zm0_zc%s_zr%s_zv%s' %(prefix,sitelist[2],zc,zr,zv),'%s_%s_cm1_rm0_zm0_zc%s_zr%s_zv%s' %(prefix,sitelist[3],zc,zr,zv),
				'%s_%s_cm1_rm1_zm0_zc%s_zr%s_zv%s' %(prefix,sitelist[0],zc,zr,zv),'%s_%s_cm1_rm1_zm0_zc%s_zr%s_zv%s' %(prefix,sitelist[1],zc,zr,zv),'%s_%s_cm1_rm1_zm0_zc%s_zr%s_zv%s' %(prefix,sitelist[2],zc,zr,zv),'%s_%s_cm1_rm1_zm0_zc%s_zr%s_zv%s' %(prefix,sitelist[3],zc,zr,zv),
				'%s_%s_cm1_rm2_zm0_zc%s_zr%s_zv%s' %(prefix,sitelist[0],zc,zr,zv),'%s_%s_cm1_rm2_zm0_zc%s_zr%s_zv%s' %(prefix,sitelist[1],zc,zr,zv),'%s_%s_cm1_rm2_zm0_zc%s_zr%s_zv%s' %(prefix,sitelist[2],zc,zr,zv),'%s_%s_cm1_rm2_zm0_zc%s_zr%s_zv%s' %(prefix,sitelist[3],zc,zr,zv)
				]

explist = halflist+interlist

with_anthro = True # set to False to calulate residual of storage and anthropogoenic heat together (if no QF.txt)
pd.set_option('display.width', 140)
plt.rc('font', family='serif')
# Define lists for loops etc
fluxes = ['NetRad','SWup','LWup','QHup','QEup']
statistics = ['RMSE', 'RMSEs', 'RMSEu', 'MBE', 'MAE', 'r2','nSD']

if with_anthro:
	fluxes_all = ['NetRad','SWup','LWup','QHup','QEup', 'dQS','dQSQF']
else:
	fluxes_all = ['NetRad','SWup','LWup','QHup','QEup', 'dQS']
summedMAE,summedrank,dQSrank={},{},{}
stats_year,stats_year_byhour,summedMAE_byhour = {},{},{}
P_hourlymean,P_timestepmean,P_hourlydesc,P_seasonalhourlymean, = {},{},{},{}
taylor_exp,int_hourly,half_hourly = {},{},{}
ncRMSE = pd.DataFrame(columns=fluxes_all)
cRMSE = pd.DataFrame(columns=fluxes_all)

# load site observations and quality flags
if os.path.exists(sitepath+'observation_preston.csv'):
	obs=pd.read_csv(sitepath+'observation_preston.csv', na_values=['nan'],comment='#')
else:
	raise ValueError('observation_preston.csv not in ateb directory. Contact data owner <andrew.coutts@monash.edu>')

if site == 'preston':
	quality=pd.read_csv(sitepath+'alpha04.dat', delim_whitespace=True, skiprows=range(7) ,usecols=[1]+[i for i in range(16,20)], header=None,
				names=['Dectime','QSWdown','QLWdown','QWind_N','QWind_E'])
	# remove any gap filled obs data
	obs.loc[quality.QSWdown!=1,fluxes] = np.nan
	obs.loc[quality.QLWdown!=1,fluxes] = np.nan
	obs.loc[quality.QWind_N!=1,fluxes] = np.nan
	obs.loc[quality.QWind_E!=1,fluxes] = np.nan
	#re-index P and O to pandas time from 12 August 2003 (day 224)
	obs.index = pd.date_range(start='20030812-1330',periods=22772,freq='30Min')
	# convert column time to decimal format
	obs['Time']=obs.index.hour + obs.index.minute/60
	#remove observed SWup at night (spurious) where SWdown forcing = 0
	obs.loc[obs.SWdown == 0, 'SWup'] = np.nan
	# create a list of unique index values where NaN appears in any flux except SWup and replace row with NaN
	dropindx = np.unique(np.where(pd.isnull(obs[['NetRad','LWup','QHup','QEup']]))[0])
	obs.ix[dropindx,fluxes] = np.nan

	#--Anthropogenic heat - simulated and estimated ---------------------------------#
	if with_anthro:
		if os.path.exists(sitepath+'QF.txt'):
			obsQF=pd.read_csv(sitepath+'QF.txt')
		else:
			raise ValueError('QF.txt not in ateb directory. Contact data owner <andrew.coutts@monash.edu>')
		obsQF.rename(columns={'QF_Preston':'NetQF'},inplace=True)
		# take values in obsQF of average hourly and monthy anthropogenic heat (288 values) and place in each timestep of obs dataframe (22772 values)
		for imonth in range(1,13):
			for ihour in range(0,24):
				obs.loc[(obs.index.month==imonth) & (obs.index.hour==ihour),'NetQF'] = obsQF.loc[(obsQF.month==imonth) & (obsQF.time==ihour*100),'NetQF'].values[0]

		#Calculate residual heat storage with anthropogenic heat from NetRad = QH + QE + dQS
		obs['dQSQF'] = obs['NetRad']-obs['QHup']-obs['QEup']

		#Calculate residual heat storage from NetRad + QF = QH + QE + dQS
		obs['dQS'] = obs['NetRad']+obs['NetQF']-obs['QHup']-obs['QEup']
	else:
		obs['dQS'] = obs['NetRad']-obs['QHup']-obs['QEup']
	O = obs.ix[5205:].copy() # match with intercomparison period (ild to 22725)

	#Add 'Seasons' column to data for summer, autumn, winter, spring.
	O.loc['2003-09-01 00:00:00':'2003-11-30 23:30:00','Season'] = 'Spring'
	O.loc['2003-12-01 00:00:00':'2004-02-29 23:30:00','Season'] = 'Summer'
	O.loc['2004-03-01 00:00:00':'2004-05-31 23:30:00','Season'] = 'Autumn'
	O.loc['2004-06-01 00:00:00':'2004-08-31 23:30:00','Season'] = 'Winter'
	O.loc['2004-09-01 00:00:00':'2004-11-30 23:30:00','Season'] = 'Spring'

	#Group_by hour and aggregate to calculate mean fluxes for whole year.
	O_hourlymean = O[fluxes_all].groupby(lambda x : x.hour).aggregate(np.mean)
	O_hourlydesc = O[fluxes_all].groupby(lambda x : x.hour).describe()

	#Multi-index hourly means based on season and month
	O_seasonalhourlydesc = O[fluxes_all+['Season']].groupby([lambda x : x.hour,'Season']).describe()
	# O_monthlyhourlydesc = O[fluxes_all].groupby([lambda x : x.month,lambda x : x.hour]).describe()

for expnum, expname in enumerate(explist):
	experiment = ''+str(expname)+''
	print('\n', 'Experiment: '+experiment)
	for k,s in enumerate(['cm0','cm1']):
		if s in expname:
			print('Conduction method:',['Half-layer','Interface'][k])
	for k,s in enumerate(['rm0','rm1','rm2']):
		if s in expname:
			print('Aero-conductance :',['Rowley','Harman','Jurges'][k])
	for sitename in ['SITE','WRF','UZE','aTEB']:
		if sitename in expname:
			print('Material database:',sitename)

	# check if simulation data file has been extracted, if not then extract
	if os.path.exists(sitepath+'out/'+experiment+'.dat'):
		sim=pd.read_csv(sitepath+'out/'+experiment+'.dat', na_values=['nan'], delim_whitespace=True, comment='#')
	else:
		with zipfile.ZipFile(sitepath+'out/out_archive.zip','r') as zip_ref:
			zip_ref.extractall(sitepath+'out/')
		with zipfile.ZipFile(sitepath+'energy/energy_archive.zip','r') as zip_ref:
			zip_ref.extractall(sitepath+'energy/')
		sim=pd.read_csv(sitepath+'out/'+experiment+'.dat', na_values=['nan'], delim_whitespace=True, comment='#')

	assert sim.columns[0] == 'Year', 'First simulation column is not year, check input file'

	if site == 'preston':
		sim.index = pd.date_range(start='20030812-1330',periods=22772,freq='30Min')
	sim['Time']=sim.index.hour + sim.index.minute/60

	sim.ix[dropindx,fluxes] = np.nan
	sim.loc[sim.SWdown == 0, 'SWup'] = np.nan

	#--Anthropogenic heat - simulated---------------------------------#
	if with_anthro:
		simQF=pd.read_csv(sitepath+'energy/%s.dat' %experiment, delim_whitespace=True, skiprows=[0,1], header=None, 
			usecols=[9,10,11], names=['Heating','Cooling','Traffic'])
		simQF['NetQF']=simQF['Heating']+simQF['Cooling']+simQF['Traffic']
		# simQFrolling=simQF.rolling(window=144).mean()
		simQF.index = sim.index

		sim['NetQF'] = simQF['NetQF']
		#Calculate residual heat storage with anthropogenic heat from NetRad = QH + QE + dQS
		sim['dQSQF'] = sim['NetRad']-sim['QHup']-sim['QEup']
		#Calculate residual heat storage from NetRad + QF = QH + QE + dQS
		sim['dQS'] = sim['NetRad']+sim['NetQF']-sim['QHup']-sim['QEup']
	else:
		sim['dQS'] = sim['NetRad']-sim['QHup']-sim['QEup']
	#-----------------------------------------------------------------#

	P = sim.ix[5205:].copy() # match with intercomparison period
	# P.dropna(subset=['NetRad','LWup','QHup','QEup'],inplace=True)
	P['Season'] = O['Season']

	assert obs.shape==sim.shape, 'obs and sim data not same shape'
	assert O.shape==P.shape, 'obs and sim data not same shape'
	assert O.dropna(subset=['NetRad']).shape==P.dropna(subset=['NetRad']).shape, 'obs and sim data not same shape'

	#--Define groups and aggregate---------------------------------------------------#

	#Group_by hour and aggregate to calculate mean fluxes for whole year.
	P_hourlymean[expname] = P[fluxes_all].groupby(lambda x : x.hour).aggregate(np.mean)
	P_hourlydesc[expname] = P[fluxes_all].groupby(lambda x : x.hour).describe()

	#Multi-index hourly means based on season and month
	P_seasonalhourlymean[expname] = P[fluxes_all+['Season']].groupby([lambda x : x.hour,'Season']).aggregate(np.mean)
	# P_monthlyhourlydesc[expname] = P[fluxes+['Season']].groupby([lambda x : x.month,lambda x : x.hour]).describe()

	#--Calculate various statistics---------------------------------------------#

	#Create all year statistics dataframe[5,5] = [stats,fluxes = ['NetRad','SWup','LWup','QHup','QEup']]
	stats_year[expname] = pd.DataFrame([
		functions.RMSE(P[fluxes_all],O[fluxes_all]),
		functions.RMSEs(P[fluxes_all],O[fluxes_all]),
		functions.RMSEu(P[fluxes_all],O[fluxes_all]),
		functions.MBE(P[fluxes_all],O[fluxes_all]),
		functions.MAE(P[fluxes_all],O[fluxes_all]),
		functions.r2(P[fluxes_all],O[fluxes_all]),
		functions.NSD(P[fluxes_all],O[fluxes_all])],
		index=statistics)
	stats_nsd = pd.DataFrame(functions.NSD(P[fluxes_all],O[fluxes_all]), columns=['NSD'], index=fluxes_all).T
	stats_r2 = pd.DataFrame(functions.r2(P[fluxes_all],O[fluxes_all]), columns=['r2'], index=fluxes_all).T
	ncRMSE.loc[expname] = functions.norm_centred_RMSE(P[fluxes_all],O[fluxes_all])
	cRMSE.loc[expname] = functions.centred_RMSE(P[fluxes_all],O[fluxes_all])

	# Create dictionary of statistics, based on data grouped in seasons.
	stats_season={}
	for key1, grp1 in P.groupby('Season'):
		for key2, grp2 in O.groupby('Season'):
			if key1==key2:
				stats_season[key1] = pd.DataFrame([
					functions.RMSE(grp1[fluxes_all],grp2[fluxes_all]),
					functions.RMSEs(grp1[fluxes_all],grp2[fluxes_all]),
					functions.RMSEu(grp1[fluxes_all],grp2[fluxes_all]),
					functions.MBE(grp1[fluxes_all],grp2[fluxes_all]),
					functions.MAE(grp1[fluxes_all],grp2[fluxes_all]),
					functions.r2(grp1[fluxes_all],grp2[fluxes_all]),
					functions.NSD(grp1[fluxes_all],grp2[fluxes_all]),
					], index=statistics)

	#Check for radiation closure correlation where Qnet_calc = SWdown + SWup + LWdown + LWup
	Qnet_calc = pd.DataFrame(P['SWdown'] + P['LWdown'] - P['SWup'] - P['LWup'], columns=['NetRad'])
	Qnet_r = Qnet_calc.corrwith(P['NetRad'])

	# Calculate effective site albedo at each timestep, excluding low values
	P_alpha = P['SWup'].where(P['SWup']>10)/P['SWdown'].where(P['SWdown']>10)
	O_alpha = O['SWup'].where(O['SWup']>10)/O['SWdown'].where(O['SWdown']>10)

	# # sum SW+LW+QH+QE+dQS
	summedMAE[expname] = stats_year[expname][['SWup','LWup','QHup','QEup','dQS']].sum(axis=1).MAE

	taylor_exp[expnum]=[[float('%.3f' %stats_nsd.ix[0,3]),float('%.3f' %np.sqrt(stats_r2.ix[0,3]))],
						[float('%.3f' %stats_nsd.ix[0,4]),float('%.3f' %np.sqrt(stats_r2.ix[0,4]))],
						[float('%.3f' %stats_nsd.ix[0,2]),float('%.3f' %np.sqrt(stats_r2.ix[0,2]))],
						[float('%.3f' %stats_nsd.ix[0,5]),float('%.3f' %np.sqrt(stats_r2.ix[0,5]))]]

	#--Print to screen----------------------------------------------------#

	print('All Year Statistics:','\n', stats_year[expname])
	print('Summed MAE: ',summedMAE[expname])

#--Initialise figures---------------------------------------------------------#

plt.close('all')
# sns.set_style('whitegrid', {'font.size':4})

if harman:
	# Combine and average experiment hourly statistics using same material database into hourly averages for each discrete schemes
	print('\nTable 2: Half-hourly performance statistics')
	for dataset in ['SITE','WRF','UZE','aTEB']:
		# Combine (if necessary) and average yearly statistics for each site and discrete scheme
		print('%s mean half stats\n'  %dataset,pd.concat([stats_year[key] for key in halflist if dataset in key],axis=1,keys=range(int(len(halflist)/4))).mean(level=1,axis=1).round(2).ix[['RMSE','MAE','r2','nSD'],['dQS']])
		print('%s mean inter stats\n' %dataset,pd.concat([stats_year[key] for key in interlist if dataset in key],axis=1,keys=range(int(len(interlist)/4))).mean(level=1,axis=1).round(2).ix[['RMSE','MAE','r2','nSD'],['dQS']])
		# Combine (if necessary) grouby hour and average statistics for each site and discrete scheme for use in Figure 6
		int_hourly[dataset] = pd.concat([P_hourlydesc[key] for key in interlist if dataset in key],axis=1,keys=range(int(len(interlist)/4))).mean(level=1,axis=1)
		half_hourly[dataset] = pd.concat([P_hourlydesc[key] for key in halflist if dataset in key],axis=1,keys=range(int(len(halflist)/4))).mean(level=1,axis=1)

if not harman:
	print('\nTable 3: Average change in statistics for  (half-layer minus interface), positive inferface improvement')
	for flux in ['dQS','LWup','QHup','QEup']:
		print('RMSE %s:'  %flux,np.array([stats_year[halflist[x]].ix['RMSE',flux]-stats_year[interlist[x]].ix['RMSE',flux] for x in range(len(halflist))]).mean().round(2),
			# '   MBE %s:' 	%flux,np.array([abs(stats_year[halflist[x]].ix['MBE',flux])-abs(stats_year[interlist[x]].ix['MBE',flux]) for x in range(len(halflist))]).mean().round(2),
			'   MAE %s:'	%flux,np.array([stats_year[halflist[x]].ix['MAE',flux]-stats_year[interlist[x]].ix['MAE',flux] for x in range(len(halflist))]).mean().round(2),
			'   r2 %s:'    %flux,np.array([abs(stats_year[halflist[x]].ix['r2',flux]-1)-abs(stats_year[interlist[x]].ix['r2',flux]-1) for x in range(len(halflist))]).mean().round(3),
			'   nSD %s:'   %flux,np.array([abs(stats_year[halflist[x]].ix['nSD',flux]-1)-abs(stats_year[interlist[x]].ix['nSD',flux]-1) for x in range(len(halflist))]).mean().round(3),
			#'   cRMSE %s:' %flux,np.array([cRMSE.ix[halflist[x],flux]-cRMSE.ix[interlist[x],flux] for x in range(len(halflist))]).mean().round(3),
			'   ncRMSE %s:'%flux,np.array([ncRMSE.ix[halflist[x],flux]-ncRMSE.ix[interlist[x],flux] for x in range(len(halflist))]).mean().round(3))

# 
print('\nTable 3 (additional): Significance testing of ensemble statistics using paired, 2 sided t-test for the null hypothesis')
for flux in ['dQS','LWup','QHup','QEup']:
	print('RMSE %s p-value:' %flux, float('%.3g' %stats.ttest_rel([stats_year[halflist[x]].ix['RMSE',flux] for x in range(len(halflist))],[stats_year[interlist[x]].ix['RMSE',flux] for x in range(len(halflist))])[1]),
		  '    MAE %s p-value:' %flux, float('%.3g' %stats.ttest_rel([stats_year[halflist[x]].ix['MAE',flux] for x in range(len(halflist))],[stats_year[interlist[x]].ix['MAE',flux] for x in range(len(halflist))])[1]),
		  '    r2 %s p-value:' %flux, float('%.3g' %stats.ttest_rel([stats_year[halflist[x]].ix['r2',flux] for x in range(len(halflist))],[stats_year[interlist[x]].ix['r2',flux] for x in range(len(halflist))])[1]),
		  '    nSD %s p-value:' %flux, float('%.3g' %stats.ttest_rel([stats_year[halflist[x]].ix['nSD',flux] for x in range(len(halflist))],[stats_year[interlist[x]].ix['nSD',flux] for x in range(len(halflist))])[1]),
		  '    ncRMSE %s p-value:' %flux, float('%.3g' %stats.ttest_rel([ncRMSE.ix[halflist[x],flux] for x in range(len(halflist))],[ncRMSE.ix[interlist[x],flux] for x in range(len(halflist))])[1]))

# print('Rowley mean half stats\n',pd.concat([stats_year[key] for key in halflist if 'rm0' in key],axis=1,keys=range(int(len(halflist)/4))).mean(level=1,axis=1).round(2).ix[['RMSE','MAE','r2','nSD'],['LWup','QHup','QEup','dQS']])
# print('Rowley mean inter stats\n',pd.concat([stats_year[key] for key in interlist if 'rm0' in key],axis=1,keys=range(int(len(interlist)/4))).mean(level=1,axis=1).round(2).ix[['RMSE','MAE','r2','nSD'],['LWup','QHup','QEup','dQS']])
# print('\nHarman mean half stats\n',pd.concat([stats_year[key] for key in halflist if 'rm1' in key],axis=1,keys=range(int(len(halflist)/4))).mean(level=1,axis=1).round(2).ix[['RMSE','MAE','r2','nSD'],['LWup','QHup','QEup','dQS']])
# print('Harman mean inter stats\n',pd.concat([stats_year[key] for key in interlist if 'rm1' in key],axis=1,keys=range(int(len(interlist)/4))).mean(level=1,axis=1).round(2).ix[['RMSE','MAE','r2','nSD'],['LWup','QHup','QEup','dQS']])
# print('\nJurges mean half stats\n',pd.concat([stats_year[key] for key in halflist if 'rm2' in key],axis=1,keys=range(int(len(halflist)/4))).mean(level=1,axis=1).round(2).ix[['RMSE','MAE','r2','nSD'],['LWup','QHup','QEup','dQS']])
# print('Jurges mean inter stats\n',pd.concat([stats_year[key] for key in interlist if 'rm2' in key],axis=1,keys=range(int(len(interlist)/4))).mean(level=1,axis=1).round(2).ix[['RMSE','MAE','r2','nSD'],['LWup','QHup','QEup','dQS']])

# #---------------Fig 6: Heat Storage Annual---------------------------#

if harman:
	plt.close('all')
	plotid = ['(a)','(b)','(c)','(d)']
	exp_plot = ['SITE','WRF','UZE','aTEB'] # fluxes to plot
	hours = P_hourlymean[expname].index+0.5
	obs25 = O_hourlydesc.xs('25%',level=1)['dQS']
	obs75 = O_hourlydesc.xs('75%',level=1)['dQS']

	fig, (tax,bax) = plt.subplots(nrows=2, ncols=len(exp_plot), sharex=True, sharey='row', gridspec_kw = {'height_ratios':[2, 1]}) 
	fig.set_size_inches(11,6)
	# plt.suptitle(r'Mean Hourly Storage Heat Flux Density ($\Delta Q_S$): aTEB Response to Conduction Schemes', fontsize=16, x=0.52)

	tax[0].set_ylabel(r'Storage heat flux density $(W\ m^{-2})$', fontsize=12, labelpad=6)
	bax[0].set_ylabel(r'Error $(W\ m^{-2})$', fontsize=12, labelpad=6)


	for j in range(len(exp_plot)):
		tax[j].axhline(color='k', linewidth=0.5)
		bax[j].set_xlabel(r'Time (h)', fontsize=12, labelpad=0)
		bax[j].axhline(color='k', linewidth=0.5)

		#quartiles
		tax[j].plot(hours,half_hourly[exp_plot[j]].xs('25%',level=1)['dQS'],ls='dotted',lw=1.25,color='orangered', label=r'25th & 75th percentile', marker='None',mec='None', ms='3.5')
		tax[j].plot(hours,half_hourly[exp_plot[j]].xs('75%',level=1)['dQS'],ls='dotted',lw=1.25,color='orangered', label=r'', marker='None',mec='None', ms='3.5')
		tax[j].plot(hours,int_hourly[exp_plot[j]].xs('25%',level=1)['dQS'],ls='dotted',lw=1.25,color='royalblue', label=r'25th & 75th percentile', marker='None',mec='None', ms='3.5')
		tax[j].plot(hours,int_hourly[exp_plot[j]].xs('75%',level=1)['dQS'],ls='dotted',lw=1.25,color='royalblue', label=r'', marker='None',mec='None', ms='3.5')
		tax[j].fill_between(hours,obs25,obs75,lw=0.,color='black',alpha=0.1, label=r'Obs: 25% - 75% range')

		tax[j].plot(hours,half_hourly[exp_plot[j]].xs('mean',level=1)['dQS'],lw=1.5,color='orangered', label=r'$\Delta Q_S:$ Half-layer mean', marker='D',mec='None', ms='3.5')
		tax[j].plot(hours,int_hourly[exp_plot[j]].xs('mean',level=1)['dQS'],lw=1.5,color='royalblue', label=r'$\Delta Q_S:$ Interface mean', marker='d',mec='None', ms='4.5')
		tax[j].plot(hours,O_hourlymean['dQS'],ls='-', lw=1.5, color='0.4', label=r'$\Delta Q_S:$ Observations mean', marker='s', ms='4',mfc='black',mec='None')

		tax[j].text(0.02, 0.97, plotid[j], fontsize=10,fontweight='bold',transform=tax[j].transAxes,horizontalalignment='left',verticalalignment='top')
		tax[j].text(0.13, 0.97, exp_plot[j], fontsize=10,transform=tax[j].transAxes,horizontalalignment='left',verticalalignment='top')

		bax[j].text(0.50, 0.89,	r'Half-layer MAE (hourly-avg): %2.1f $W\ m^{-2}$' %(functions.MAE(half_hourly[exp_plot[j]].xs('mean',level=1)['dQS'],O_hourlymean['dQS'])), 
			color='orangered',fontsize=8, horizontalalignment='center', transform=bax[j].transAxes)
		bax[j].text(0.50, 0.05,	r'Interface MAE (hourly-avg): %2.1f $W\ m^{-2}$' %(functions.MAE(int_hourly[exp_plot[j]].xs('mean',level=1)['dQS'],O_hourlymean['dQS'])), 
			color='royalblue',fontsize=8, horizontalalignment='center', transform=bax[j].transAxes)

		bax[j].plot(hours,(half_hourly[exp_plot[j]].xs('mean',level=1)['dQS']-O_hourlymean['dQS']), lw=1.5,color='orangered', label=r'', marker='D',mec='None', ms='3.5')
		bax[j].plot(hours,(int_hourly[exp_plot[j]].xs('mean',level=1)['dQS']-O_hourlymean['dQS']),lw=1.5,color='royalblue', label=r'', marker='d',mec='None', ms='4.5')

		bax[j].set_xticks([0,6,12,18,24])
		bax[j].minorticks_off()
		tax[j].minorticks_off()
		bax[j].tick_params(axis='both', which='major', labelsize=10)
		tax[j].tick_params(axis='both', which='major', labelsize=10)
		bax[j].set_ylim([-130,70])
		bax[j].set_xlim([0,24])

		if j==0:
			handles, labels = tax[0].get_legend_handles_labels()

	legend_order = [4,5,2,0,3,1]
	handles = [handles[i] for i in legend_order]
	labels = [labels[i] for i in legend_order]

	fig.subplots_adjust(bottom=0.17, top=0.97, left=0.07, right=0.98, wspace=0.06, hspace=0.04)
	fig.legend(fontsize='small',loc='lower center', handles=handles, labels=labels, bbox_to_anchor=(0.52,0.005), numpoints=1, ncol=3, frameon=True) #
	# fig.text(0.997,0.003, 'FileID: '+scriptname+'. Created: '+time.strftime('%d/%m/%y %H:%M')+' by m.lipson@unsw.edu.au', size=5, ha='right')
	plt.savefig('%s/Fig6_Diurnal.pdf' %(plotpath), dpi=200)
	# plt.show()

# # # 	# #---------------Fig: Heat Storage by Season (not used)--------------------------#

# if harman:
# 	plt.close('all')
# 	# timex = [x for x in np.arange(0,49,1)]
# 	plotid = ['(a)','(b)','(c)','(d)']
# 	exp_plot = ['SITE','WRF','UZE','aTEB'] # fluxes to plot
# 	season='Autumn'
# 	hours = P_hourlymean[expname].index+0.5
# 	obsmean = O_seasonalhourlydesc.xs(season,level=1).xs('mean',level=1)['dQS']
# 	obs25 = O_seasonalhourlydesc.xs(season,level=1).xs('25%',level=1)['dQS']
# 	obs75 = O_seasonalhourlydesc.xs(season,level=1).xs('75%',level=1)['dQS']

# 	fig, (tax,bax) = plt.subplots(nrows=2, ncols=len(exp_plot), sharex=True, sharey='row', gridspec_kw = {'height_ratios':[2, 1]}) 
# 	fig.set_size_inches(11,6)
# 	plt.suptitle(str(season)+r' Mean Hourly $\Delta Q_S$: aTEB Response to Discrete Schemes', fontsize=16)

# 	tax[0].set_ylabel(r'Storage heat flux density $(W/m^2)$', fontsize=12, labelpad=6)
# 	bax[0].set_ylabel(r'Normalised error', fontsize=12, labelpad=10)


# 	for j in range(len(exp_plot)):
# 		# tax[j].set_axisbelow(True)
# 		tax[j].axhline(color='k', linewidth=0.5)
# 		bax[j].set_xlabel(r'Time (h)', fontsize=12, labelpad=0)
# 		bax[j].axhline(color='k', linewidth=0.5)
# 		tax[j].plot(hours,obsmean,ls='--', color='0.4', label=r'$\Delta Q_S:$ %s observations' %season, marker='s', ms='4',mfc='black',mec='None')
# 		tax[j].plot(hours,P_seasonalhourlymean[halflist[j]].xs(season,level=1)['dQS'],color='orangered', label=r'$\Delta Q_S:$ Half-layer', marker='D', ms='4',mec='None')
# 		tax[j].plot(hours,P_seasonalhourlymean[interlist[j]].xs(season,level=1)['dQS'],color='royalblue', label=r'$\Delta Q_S:$ Interface', marker='D', ms='4',mec='None')

# 		tax[j].fill_between(hours,obs25,obs75,lw=0.,color='black',alpha=0.1, label=r'25%-75% range')
# 		tax[j].text(0.02, 0.98, plotid[j], fontsize=10,fontweight='bold',transform=tax[j].transAxes,horizontalalignment='left',verticalalignment='top')
# 		tax[j].text(0.50, 0.98, exp_plot[j], fontsize=10,transform=tax[j].transAxes,horizontalalignment='center',verticalalignment='top')

# 		bax[j].text(0.50, 0.89,	r'Half-layer nMAE : %2.1f%%' %(100*functions.norm_MAE(P_seasonalhourlymean[halflist[j]].xs('Winter',level=1)['dQS'],obsmean)), 
# 			color='orangered',fontsize=8, horizontalalignment='center', transform=bax[j].transAxes)
# 		bax[j].text(0.50, 0.05,	r'Interface nMAE : %2.1f%%' %(100*functions.norm_MAE(P_seasonalhourlymean[interlist[j]].xs('Winter',level=1)['dQS'],obsmean)), 
# 			color='royalblue',fontsize=8, horizontalalignment='center', transform=bax[j].transAxes)

# 		bax[j].plot(hours,((P_seasonalhourlymean[halflist[j]].xs(season,level=1)['dQS']-obsmean)/np.mean(abs(obsmean))),color='orangered', label=r'', marker='D', ms='4',mec='None')
# 		bax[j].plot(hours,((P_seasonalhourlymean[interlist[j]].xs(season,level=1)['dQS']-obsmean)/np.mean(abs(obsmean))),color='royalblue', label=r'', marker='D', ms='4',mec='None')

# 		# tax[j].set_yticks(range(-50,500,100))
# 		# tax[j].set_yticklabels(range(-50,500,100))
# 		bax[j].set_xticks([0,6,12,18,24])

# 		# bax[j].set_xticklabels([0,6,12,18,24])
# 		bax[j].minorticks_off()
# 		tax[j].minorticks_off()
# 		bax[j].tick_params(axis='both', which='major', labelsize=10)
# 		tax[j].tick_params(axis='both', which='major', labelsize=10)
# 		# tax[j].set_ylim([-13,13])
# 		bax[j].set_ylim([-1.45,1.45])
# 		bax[j].set_xlim([0,24])

# 		if j==0:
# 			handles, labels = tax[0].get_legend_handles_labels()

# 	fig.subplots_adjust(bottom=0.13, top=0.93, left=0.07, right=0.98, wspace=0.08, hspace=0.04)
# 	fig.legend(fontsize='small',loc='lower center', handles=handles, labels=labels, bbox_to_anchor=(0.52,0.005), numpoints=1, ncol=5, frameon=True) #
# 	fig.text(0.997,0.003, 'FileID: '+scriptname+'. Created: '+time.strftime('%d/%m/%y %H:%M')+' by m.lipson@unsw.edu.au', size=5, ha='right')
# 	plt.savefig('%s/Fig6_%s.pdf' %(plotpath,season), dpi=200)
# 	# plt.show()

#--Fig7: Taylor Diagram--------------------------------------------------------#

if not harman:
	# Taylor diagram python code addapted from "Yannick Copin <yannick.copin@laposte.net>"
	# TaylorDiagram module in taylordiagram.py

	exp_plot = ['SITE','WRF','UZE','aTEB']
	simnames = ([str(x)+': Rowley half' for x in exp_plot]+[str(x)+': Harman half' for x in exp_plot]+[str(x)+': Jurges half' for x in exp_plot]+
	           [str(x)+': Rowley' for x in exp_plot]+[str(x)+': Harman' for x in exp_plot]+[str(x)+': Jurges' for x in exp_plot])

	stdrefs = dict(sensible=1.0, latent=1.0, longwave=1.0, storage=1.0)

	simplenames = ['13','16','21','32','33','37','39','40','41','46','47','48','49','50']
	mediumnames = ['11','20','28','30','35','36','38','42','43','44','45']
	complexnames = ['12','14','15','22','25','31']
	othernames = ['11','13','16','18','20','21','28','30','32','33','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50']

	# Taylor Plot info (nstd,r2): [Qh],[Qe],[LW],[Qs]
	# intercomparison models stage 4 results.
	intercomparisonS4 = {}
	intercomparisonS4['11']=[[1.785, 0.841],[0.000, 0.000],[1.048, 0.942],[0.881, 0.787]]
	intercomparisonS4['12']=[[1.653, 0.945],[0.000, 0.000],[1.469, 0.944],[0.539, 0.809]]
	intercomparisonS4['13']=[[1.186, 0.952],[0.696, 0.697],[1.316, 0.974],[0.729, 0.886]]
	intercomparisonS4['14']=[[1.649, 0.946],[0.000, 0.000],[1.440, 0.947],[0.559, 0.817]]
	intercomparisonS4['15']=[[1.672, 0.946],[1.191, 0.724],[0.999, 0.985],[0.496, 0.662]]
	intercomparisonS4['16']=[[1.007, 0.945],[0.778, 0.702],[1.272, 0.955],[0.519, 0.721]]
	intercomparisonS4['18']=[[1.401, 0.954],[0.904, 0.702],[1.120, 0.966],[0.537, 0.851]]
	intercomparisonS4['20']=[[1.659, 0.931],[0.000, 0.000],[1.050, 0.974],[0.840, 0.871]]
	intercomparisonS4['21']=[[0.883, 0.921],[1.063, 0.708],[1.232, 0.978],[0.997, 0.860]]
	intercomparisonS4['22']=[[0.701, 0.844],[0.269, 0.512],[0.835, 0.973],[1.515, 0.878]]
	intercomparisonS4['25']=[[1.425, 0.947],[0.875, 0.633],[1.161, 0.969],[0.608, 0.853]]
	intercomparisonS4['28']=[[1.830, 0.831],[0.000, 0.000],[0.936, 0.933],[0.779, 0.794]]
	intercomparisonS4['30']=[[1.475, 0.947],[0.909, 0.700],[1.164, 0.945],[0.364, 0.858]]
	intercomparisonS4['31']=[[1.531, 0.897],[0.954, 0.408],[0.976, 0.980],[0.580, 0.822]]
	intercomparisonS4['32']=[[1.239, 0.921],[1.055, 0.573],[1.412, 0.966],[0.826, 0.820]]
	intercomparisonS4['33']=[[0.856, 0.937],[1.343, 0.726],[1.150, 0.965],[0.735, 0.885]]
	intercomparisonS4['35']=[[1.104, 0.881],[0.479, 0.379],[1.238, 0.959],[1.100, 0.878]]
	intercomparisonS4['36']=[[1.000, 0.893],[0.513, 0.643],[0.993, 0.991],[1.214, 0.892]]
	intercomparisonS4['37']=[[1.212, 0.945],[0.697, 0.698],[1.339, 0.973],[0.794, 0.847]]
	intercomparisonS4['38']=[[1.697, 0.929],[0.000, 0.000],[0.938, 0.962],[0.744, 0.862]]
	intercomparisonS4['39']=[[1.266, 0.946],[0.698, 0.723],[1.871, 0.881],[0.536, 0.863]]
	intercomparisonS4['40']=[[1.421, 0.941],[0.943, 0.572],[1.342, 0.963],[0.570, 0.819]]
	intercomparisonS4['41']=[[1.152, 0.925],[0.658, 0.631],[0.989, 0.981],[0.757, 0.891]]
	intercomparisonS4['42']=[[1.156, 0.873],[0.479, 0.377],[1.268, 0.965],[1.113, 0.877]]
	intercomparisonS4['43']=[[1.180, 0.949],[1.088, 0.725],[1.298, 0.968],[0.581, 0.877]]
	intercomparisonS4['44']=[[0.931, 0.866],[1.287, 0.698],[0.857, 0.953],[1.006, 0.861]]
	intercomparisonS4['45']=[[1.401, 0.954],[0.904, 0.702],[1.120, 0.966],[0.537, 0.851]]
	intercomparisonS4['46']=[[1.357, 0.947],[0.895, 0.626],[0.964, 0.982],[0.504, 0.866]]
	intercomparisonS4['47']=[[1.243, 0.949],[0.842, 0.737],[1.211, 0.985],[0.656, 0.885]]
	intercomparisonS4['48']=[[1.154, 0.929],[0.651, 0.623],[0.989, 0.980],[0.755, 0.873]]
	intercomparisonS4['49']=[[0.837, 0.927],[1.082, 0.704],[1.172, 0.986],[0.992, 0.884]]
	intercomparisonS4['50']=[[1.355, 0.732],[1.818, 0.623],[0.557, 0.624],[0.659, 0.863]]

	samples= dict(sensible=[[taylor_exp[i][0][0], taylor_exp[i][0][1], simnames[i]] for i,num in enumerate(simnames)],
	                latent=[[taylor_exp[i][1][0], taylor_exp[i][1][1], simnames[i]] for i,num in enumerate(simnames)],
	              longwave=[[taylor_exp[i][2][0], taylor_exp[i][2][1], simnames[i]] for i,num in enumerate(simnames)],
	               storage=[[taylor_exp[i][3][0], taylor_exp[i][3][1], simnames[i]] for i,num in enumerate(simnames)])

	samples2= dict(sensible=[[intercomparisonS4[num][0][0], intercomparisonS4[num][0][1], complexnames[i]] for i,num in enumerate(complexnames)],
	                latent=[[intercomparisonS4[num][1][0], intercomparisonS4[num][1][1], complexnames[i]] for i,num in enumerate(complexnames)],
	              longwave=[[intercomparisonS4[num][2][0], intercomparisonS4[num][2][1], complexnames[i]] for i,num in enumerate(complexnames)],
	               storage=[[intercomparisonS4[num][3][0], intercomparisonS4[num][3][1], complexnames[i]] for i,num in enumerate(complexnames)])

	samples3= dict(sensible=[[intercomparisonS4[num][0][0], intercomparisonS4[num][0][1], othernames[i]] for i,num in enumerate(othernames)],
	                latent=[[intercomparisonS4[num][1][0], intercomparisonS4[num][1][1], othernames[i]] for i,num in enumerate(othernames)],
	              longwave=[[intercomparisonS4[num][2][0], intercomparisonS4[num][2][1], othernames[i]] for i,num in enumerate(othernames)],
	               storage=[[intercomparisonS4[num][3][0], intercomparisonS4[num][3][1], othernames[i]] for i,num in enumerate(othernames)])

	col1 = (['crimson']+['goldenrod']+['lightblue']+['royalblue'])*3
	col2 = ['None']*12
	col3 = ['black']*12
	mfcol = col2+col1
	mecol = col1+col3
	marker = (['v']*4+['D']*4+['^']*4)*2

	# define flux panel location
	rects = dict(storage=221,
	             longwave=222,
	             sensible=223,
	             latent=224)

	fig = plt.figure(figsize=(9,10))

	for flux in ['storage','longwave','sensible','latent']:
	    dia = TaylorDiagram(stdrefs[flux], fig=fig, rect=rects[flux],
	                        label='Ideal model')

	    # Add RMS contours, and label them
	    contours = dia.add_contours(levels=10, colors='0.8',lw=0.1) # 5 levels
	    dia.ax.clabel(contours, inline=1, fontsize=6, fmt='%.1f', zorder=1)
	    # ax is the polar ax (used for plots), _ax is the floating container (used for layouts)
	    dia._ax.set_title(flux.capitalize()).set_weight('bold')
	    dia.ax.text(np.arccos(0.42),1.25,'cRMSE',color='0.8',rotation=23,ha='center',va='center',fontsize=10)

	    # Add samples to Taylor diagram
	    for i,(stddev,corrcoef,name) in enumerate(samples[flux]):
	        dia.add_sample(stddev, corrcoef,
	                       marker=marker[i], ms=5, ls='',       # marker as markers
	                       mfc=mfcol[i], mec=mecol[i], alpha=0.75,# Colors
	                       label=name, zorder=10)
	    # # to connect two ajoining models
	    simiter = int(len(simnames)/2)
	    for i in range(0,simiter):
	      dia.ax.plot([np.arccos(samples[flux][i][1]),np.arccos(samples[flux][i+simiter][1])],[samples[flux][i][0],samples[flux][i+simiter][0]],color=col1[i],lw=0.5)
	    # Add intercomparisonS4 results (complex models stage 4)
	    # plot other COMPLEX models
	    for i,(stddev,corrcoef,name) in enumerate(samples2[flux]):
	        dia.add_sample(stddev, corrcoef, marker=r'x', ms=4, ls='',mfc='None', mec='k',alpha=0.5,label=name)
	    # plot all other models
	    for i,(stddev,corrcoef,name) in enumerate(samples3[flux]):
	        dia.add_sample(stddev, corrcoef, marker=r'o', ms=2, ls='',mfc='None',mec='k',alpha=0.5,label=name)

	# Add a figure legend and title. For loc option, place x,y tuple inside [ ].

	dia.samplePoints[25].set_label('PILPS-urban (complex)')
	dia.samplePoints[31].set_label('PILPS-urban (others)')
	sample_order = [13,17,21,14,18,22,15,19,23,16,20,24,25,31,0]
	dia.samplePoints = [dia.samplePoints[i] for i in sample_order]

	fig.text(0.02,0.77,'Normalised Standard Deviation',va='center',ha='center',rotation=90)
	fig.text(0.02,0.31,'Normalised Standard Deviation',va='center',ha='center',rotation=90)
	fig.legend(dia.samplePoints,
	           [ p.get_label() for p in dia.samplePoints ],
	           numpoints=1, prop=dict(size='x-small'), ncol=5,
	           bbox_to_anchor=(0.94, 0.065), bbox_transform=plt.gcf().transFigure)

	plt.subplots_adjust(bottom=0.08,top=0.99,left=0.07,right=0.98,hspace=0.05,wspace=0.12)

	plt.savefig(plotpath+'/Fig7_Taylor.pdf')


