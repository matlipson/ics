## FOR USE WITH PYTHON 3.4 ##
print('''
	Purpose:    1) Compare two discrete conduction paramaterisations against exact solution for periodic forcing per ISO13786. 
				2) Analyse and plot results (Figures 2,3,4,5 & D1)
	Developer:  Mathew Lipson <m.lipson@unsw.edu.au>
	Manuscript: Efficiently modelling urban heat storage: an interface conduction scheme in an urban land surface model (aTEB v2.0)
	Journal:    Geoscientific Model Development
	Revision:   12 January 2017 (to incorporate peer review revision)
	Instructions:
				User inputs timestep and figure to plot.
				For Fig. 4 & 5 four walls/ roofs will be calculated and plotted. 
				For Fig.2 & 3 an option will be provided to re-calculate 288 assemblies.
					If recalculate=True, heat storage calculations for 288 assemblies will run.
					For high temporal resolution (e.g. 1 minute), run will take ~10 minutes.
					At end of run, results will be saved to file for later use.
					If recalculate=False, previously saved files will be loaded and used.
				For Fig D1, timesteps [1,15,30,60] must already be pickled, input Fig. 2/3 & recalculate=True''')

import numpy as np
import pandas as pd
import seaborn.apionly as sns
import matplotlib.pyplot as plt
import matplotlib.patheffects as path_effects
import statsmodels.api as sm
from imp import reload
import pickle
import time 		# for current time
import os 			# for current filename

import functions, materials_interface

# ## -------------user input---------------- ##
plotfig = input('Which figure to plot [2,3,4,5 or D1]')
if plotfig not in ['2','3','4','5','D1']:
	plotfig = input('Input must be single value of [2,3,4,5 or D1]')
	assert plotfig in ['2','3','4','5','D1'],"Input not valid, aborting"
if plotfig in ['2','3']:
	recalculate = input('Re-calculate 288 assemblie properties? [True/False]') # Set to false if already pickled.
	recalculate = recalculate in ['True','true','T','t']
else:
	recalculate = False
if plotfig not in ['D1']:
	minutes = input('Timestep in minutes [1,15,30,60]')
	minutes = int(minutes)
else:
	minutes=30

# ## -------------variables---------------- ##

days = 6                # Total time domain
period = 60*60*24       # Forcing period (day, in seconds)
T0 = 290.               # initial temperature
T_int = 290.            # Fixed internal building temperature
T_ext_amp = 1.          # Amplitude of external temperature variation [k]
r_se = 0.04             # external surface resistance [m^2K/W] Default 0.04 per ISO6946 horizontal.
r_si = 0.13             # internal surface resistance [m^2K/W] Default 0.13 per ISO6946 horizontal.
phase =  0.             # phase shift for forcing temperature (e.g. -np.pi/2)
integrated = False      # integrate temperature forcing and exact solution between -ddt/2 -> +ddt/2

# ## -------------constants---------------- ##

plotpath = '../figures'
scriptname = str(os.path.basename(__file__))  # define current script filename
pd.set_option('display.width', 300)
pd.set_option('display.max_rows', 200)
pd.set_option('display.float_format', lambda x: '%.6f' % x)
np.set_printoptions(linewidth=300)
plt.rc('font', family='serif')

interface,half_layer,exact = {},{},{}

if recalculate==True:
	walllist = (['CLMu_wall_concpanels_%sl'  		%x for x in range(2,11)]+
				['CLMu_wall_glasscurtain_%sl' 		%x for x in range(2,11)]+
				['CLMu_wall_brickveneer_%sl' 		%x for x in range(2,11)]+
				['CLMu_wall_stonecurtain_%sl' 		%x for x in range(2,11)]+
				['CLMu_wall_veneerbrickmasonry_%sl' %x for x in range(2,11)]+
				['CLMu_wall_veneerconcmasonry_%sl' 	%x for x in range(2,11)]+
				['CLMu_wall_EIFSfacade_%sl' 		%x for x in range(2,11)]+
				['CLMu_wall_cementboard_%sl' 		%x for x in range(2,11)]+
				['CLMu_wall_hardbrdsiding_%sl' 		%x for x in range(2,11)]+
				['CLMu_wall_woodsiding_%sl' 		%x for x in range(2,11)]+
				['CLMu_wall_uninswoodsiding_%sl' 	%x for x in range(2,11)]+
				['CLMu_wall_vinylsiding_%sl' 		%x for x in range(2,11)]+
				['CLMu_wall_woodstucco_%sl' 		%x for x in range(2,11)]+
				['CLMu_wall_brickmasonryreinf_%sl' 	%x for x in range(2,11)]+
				['CLMu_wall_stone_%sl' 				%x for x in range(2,11)]+
				['CLMu_wall_concreteblocks_%sl' 	%x for x in range(2,11)]+
				['CLMu_wall_corrugatedmetal_%sl' 	%x for x in range(2,11)]+
				['CLMu_wall_mudadobe_%sl' 			%x for x in range(2,11)]+
				['CLMu_wall_rubble_%sl' 			%x for x in range(2,11)]+
				['CLMu_wall_galvsteel_%sl' 			%x for x in range(2,11)]+
				['CLMu_roof_burconcrete_%sl' 		%x for x in range(2,11)]+
				['CLMu_roof_burwood_%sl' 			%x for x in range(2,11)]+
				['CLMu_roof_pvcsteeldeck_%sl' 		%x for x in range(2,11)]+
				['CLMu_roof_epdmsteeldeck_%sl' 		%x for x in range(2,11)]+
				['CLMu_roof_galvsteeldeck_%sl' 		%x for x in range(2,11)]+
				['CLMu_roof_corrmetal_%sl' 			%x for x in range(2,11)]+
				['CLMu_roof_shingles_%sl' 			%x for x in range(2,11)]+
				['CLMu_roof_ceramictiles_%sl' 		%x for x in range(2,11)]+
				['CLMu_roof_thatch_%sl' 			%x for x in range(2,11)]+
				['CLMu_roof_slatetile_%sl' 			%x for x in range(2,11)]+
				['CLMu_roof_metaltile_%sl' 			%x for x in range(2,11)]+
				['CLMu_roof_mudadobe_%sl' 			%x for x in range(2,11)])
else:
	if plotfig=='4':
		walllist = ['SITE_wall_4l','WRF_wall_4l','UZE_wall_4l','aTEB_wall_4l']
	if plotfig=='5':
		walllist = ['SITE_roof_4l','WRF_roof_4l','UZE_roof_4l','aTEB_roof_4l']

def calc_dt(minutes):
	ddt = 60*minutes        			# Timestep length (s)
	steps = int(days*(24*60)/minutes)   # Number of timesteps
	return ddt, steps

def calc_walllist():
	interface_properties,half_properties = pd.DataFrame(), pd.DataFrame()
	for j,wallname in enumerate(walllist):
		# Defines wall from variable name 'wallname' in module 'materials_interface.py' 
		wall = getattr(materials_interface, wallname)

		if wallname[:-2]+'10l' in walllist:
			# if 10 layer version exists, define for later comparison
			wall10 = getattr(materials_interface, wallname[:-2]+'10l')

		print('#------------------------------------------#')
		print('Calculating conduction for assembly %s of %s \n' %(j+1,len(walllist)))
		print(wallname)
		print(wall)
		
		print('Total depth: ',sum(wall['depth']), 'm \n')
		# pass variables to function in functions.py to create heat transfer matrix Z & create material characteristics class
		Z_in, Z_out, Z_totalin, Z_totalout = functions.heat_transfer_matrix(r_se=r_se, r_si=r_si, period=period, wall=wall)
		material1 = functions.Characteristics(r_se=r_se, r_si=r_si, period=period, ddt=ddt, phase=phase, steps=steps, material=wall, Z_in=Z_in, Z_out=Z_out, Z_totalin=Z_totalin, Z_totalout=Z_totalout)
		conduction1 = functions.Conduction(r_se=r_se, r_si=r_si, period=period,  ddt=ddt, steps=steps, T0=T0, T_int=T_int, T_ext=T_ext, material=wall)
		if wallname[:-2]+'10l' in walllist:
			# create ISO13786 class for the 10 layered version for testing of exact solution
			Z_in10, Z_out10, Z_totalin10, Z_totalout10 = functions.heat_transfer_matrix(r_se=r_se, r_si=r_si, period=period, wall=wall10)
			material10 = functions.Characteristics(r_se=r_se, r_si=r_si, period=period, ddt=ddt, phase=phase, steps=steps, material=wall10, Z_in=Z_in10, Z_out=Z_out10, Z_totalin=Z_totalin10, Z_totalout=Z_totalout10)
		
		# print('First layer periodic penetration depth: ', np.round(delta, 4))
		print('Steady-state resistance (R-value): %.4f' % (material1.TotalResistance(ext_res=True)))
		print('Steady-state transmittance (U-value): %.4f' % (1./material1.TotalResistance()))
		print('Steady-state transmittance no res (U-value): %.4f' % (1./material1.TotalResistance(ext_res=False)))
		print('Periodic transmittance: %.3f' % material1.PeriodicTransmittance())
		print('Periodic admittance external: %.3f' % material1.PeriodicAdmittance('out'))
		print('Decrement factor: %.3f' % material1.DecrementFactor())
		print('Decrement delay (lag): %.2f' % material1.DecrementDelay())
		print('External areal heat capacity no res: %.2f' % material1.ArealHeatCapacity(loc='out',ext_res=False))
		print('External areal heat capacity w/ res: %.2f' % material1.ArealHeatCapacity(loc='out',ext_res=True))
		# print('Thermal Diffusivity:' ,material1.ThermalDiffusivity())

		if wallname[:-2]+'10l' in walllist:
			# Use 10 layer version for  if avaliable
			print('10 layered version of material found, using for exact solution\n')
			if integrated:
				exact[wallname] = material10.exact_integrated(ext_res=True).tail(int(1+24*3600/ddt))
			else:
				exact[wallname] = material10.exact_paper(ext_res=True).tail(int(1+24*3600/ddt))
			exact[wallname].index = indexrange
		else:
			print('using this wall for exact solution\n') 
			if integrated:
				exact[wallname] = material1.exact_integrated(ext_res=True).tail(int(1+24*3600/ddt))
			else:
				exact[wallname] = material1.exact_paper(ext_res=True).tail(int(1+24*3600/ddt))
			exact[wallname].index = indexrange

		half_layer[wallname] = conduction1.half_layer().tail(int(1+24*3600/ddt))
		interface[wallname] = conduction1.interface().tail(int(1+24*3600/ddt))
		
		half_layer[wallname].index = indexrange
		interface[wallname].index = indexrange

		print('Normalised MAE for %s: Half-layer: %.15f' %(wallname,functions.norm_MAE(half_layer[wallname]['G_sum'],exact[wallname]['G_sum'])))
		print('Normalised MAE for %s: Interface: %.15f  \n' %(wallname,functions.norm_MAE(interface[wallname]['G_sum'],exact[wallname]['G_sum'])))

		if recalculate==True:
			interface_properties = interface_properties.append(
				pd.DataFrame([{
					'Layers':len(wall),
					'Homogenous': True if len(wall[['conduct','heatc']].drop_duplicates()) == 1 else False,     # Check if wall is homogenous by dropping duplicate rows
					'Surface': wallname[5:9],
					'Lag': material1.ThermalLag(),
					'LagISO': material1.DecrementDelay(),
					'U-Value': 1./material1.TotalResistance(ext_res=False),
					'R-Value': material1.TotalResistance(ext_res=False),
					'R-Value_res': material1.TotalResistance(ext_res=True),
					'AdmittanceISO': material1.PeriodicAdmittance(loc='out'),
					'TransmittanceISO': material1.PeriodicTransmittance(),
					'ArealHC': material1.ArealHeatCapacity(ext_res=False,loc='out'),
					'ArealHC_res': material1.ArealHeatCapacity(ext_res=True,loc='out'),
					'DecrementF': material1.DecrementFactor(),
					'MAE': functions.MAE(interface[wallname]['G_sum'],exact[wallname]['G_sum']),
					'MBE': functions.MBE(interface[wallname]['G_sum'],exact[wallname]['G_sum']),
					'norm_MAE': functions.norm_MAE(interface[wallname]['G_sum'],exact[wallname]['G_sum']),
					'norm_cRMSE': functions.norm_centred_RMSE(interface[wallname]['G_sum'],exact[wallname]['G_sum']),
					'norm_SD': functions.NSD(interface[wallname]['G_sum'],exact[wallname]['G_sum']),
					'r2': functions.r2(interface[wallname],exact[wallname])['G_sum'],
				}],index=[wallname]))

			half_properties = half_properties.append(interface_properties[-1:])
			half_properties.ix[[wallname],'MAE'] = functions.MAE(half_layer[wallname]['G_sum'],exact[wallname]['G_sum'])
			half_properties.ix[[wallname],'MBE'] = functions.MBE(half_layer[wallname]['G_sum'],exact[wallname]['G_sum'])
			half_properties.ix[[wallname],'norm_MAE'] = functions.norm_MAE(half_layer[wallname]['G_sum'],exact[wallname]['G_sum'])
			half_properties.ix[[wallname],'norm_cRMSE'] = functions.norm_centred_RMSE(half_layer[wallname]['G_sum'],exact[wallname]['G_sum'])
			half_properties.ix[[wallname],'norm_SD'] = functions.NSD(half_layer[wallname]['G_sum'],exact[wallname]['G_sum'])
			half_properties.ix[[wallname],'r2'] = functions.r2(half_layer[wallname],exact[wallname])['G_sum']

			cols =  ['Layers','Homogenous','Surface','Lag','LagISO','U-Value','R-Value','R-Value_res','AdmittanceISO','TransmittanceISO','ArealHC','ArealHC_res','DecrementF',
						'MAE','MBE','norm_MAE','norm_cRMSE','norm_SD','r2']

			interface_properties = interface_properties[cols]
			half_properties = half_properties[cols]

			interface_properties.drop_duplicates(inplace=True)
			half_properties.drop_duplicates(inplace=True)
			pickle.dump(interface_properties, open('%s/pickle/interface_properties_%smin.pk1' %(plotpath,minutes), 'wb'))
			pickle.dump(half_properties, open('%s/pickle/half_properties_%smin.pk1' %(plotpath,minutes), 'wb'))

	return interface_properties,half_properties,interface,half_layer,exact

# # #---------------Fig2: Characteristics Layers---------------------------#

def plotfig2():
	halflayer = half_properties.groupby('Layers').aggregate(np.mean)
	interfacelayer = interface_properties.groupby('Layers').aggregate(np.mean)

	plotid = ['(a)','(b)','(c)','(d)']
	plotstat = ['norm_SD','norm_MAE']
	plotstatname = ['Normalised standard deviation', 'Normalised mean absolute error']
	whitestroke = [path_effects.withStroke(linewidth=1.75, foreground="w")]

	fig, ax = plt.subplots(nrows=1,ncols=2,sharex=True)
	fig.set_size_inches(11,4.5)
	# plt.suptitle('Storage heat flux ($Q_S$): error of discrete schemes vs exact solution - %s minute timesteps' %minutes, x=0.52,fontsize=16)

	for j in range(len(ax)):
		ax[j].text(0.02, 0.97, plotid[j], fontsize=10,fontweight='bold',transform=ax[j].transAxes,horizontalalignment='left',verticalalignment='top')

		sns.stripplot(x='Layers',y=plotstat[j],data=half_properties, color='orangered', size=3, jitter=0.05, alpha=0.6, ax=ax[j],zorder=1,label='Half-layer: individual wall/roof')
		sns.stripplot(x='Layers',y=plotstat[j],data=interface_properties, color='royalblue', size=3, jitter=0.05, alpha=0.6, ax=ax[j], zorder=2,label='Interface: individual wall/roof')
		ax[j].plot(halflayer.index.values-2,halflayer[plotstat[j]], color='orangered',linewidth=1.8,marker='o',mec='w',ms=6,label='Half-layer: layer mean')
		ax[j].plot(interfacelayer.index.values-2,interfacelayer[plotstat[j]], color='royalblue',linewidth=1.8,marker='o',mec='w',ms=6,label='Interface: layer mean')


		ax[j].set_ylabel(str(plotstatname[j]), fontsize=12, labelpad=5)
		ax[j].set_xlabel('Layers', fontsize=12, labelpad=5)
		ax[j].tick_params(axis='both', which='major', labelsize=10)

		if j==0:
			opt_intidx = abs(interfacelayer[plotstat[j]]-1).idxmin()
			opt_halfidx = abs(halflayer[plotstat[j]]-1).idxmin()
			ax[j].scatter(opt_halfidx-2,halflayer.ix[opt_halfidx,[plotstat[j]]],s=160, edgecolor='red',facecolor='gold', marker='*',zorder=12,lw=0.75, path_effects=whitestroke, label='Half-layer SD optimum: %s+ layers' %opt_halfidx)
			ax[j].scatter(opt_intidx-2,interfacelayer.ix[opt_intidx,[plotstat[j]]],s=160, edgecolor='blue',facecolor='gold', marker='*',zorder=12,lw=0.75, path_effects=whitestroke, label='Interface SD optimum: %s layers' %opt_intidx)
			
		if j==1:
			opt_intidx = interfacelayer[plotstat[j]].idxmin()
			opt_halfidx  = halflayer[plotstat[j]].idxmin()
			ax[j].scatter((opt_halfidx-2),halflayer.ix[opt_halfidx,[plotstat[j]]],s=160, edgecolor='red',facecolor='gold', marker='*',zorder=12,lw=0.75, path_effects=whitestroke, label='Half-layer MAE optimum: %s+ layers' %opt_halfidx)
			ax[j].scatter((opt_intidx-2),interfacelayer.ix[opt_intidx,[plotstat[j]]],s=160, edgecolor='blue',facecolor='gold', marker='*',zorder=12,lw=0.75, path_effects=whitestroke, label='Interface MAE optimum: %s layers' %opt_intidx)
			handles, labels = ax[1].get_legend_handles_labels()

	legend_order=[2,11,0,1,-2,-1]
	handles = [handles[i] for i in legend_order]
	labels = [labels[i] for i in legend_order]

	if plotstat[0] == 'norm_SD':
		ax[0].set_ylim([0.9,1.1])
		ax[0].axhline(y=1, color='k', linewidth=1)
	elif plotstat[0] == 'MBE':
		ax[0].set_ylim([-0.03,0.02])
		ax[0].axhline(y=0, color='k', linewidth=1)

	if plotstat[1] == 'MAE':
		ax[1].set_ylim([0,0.8])
	else:
		ax[1].set_ylim([0,0.25])

	fig.subplots_adjust(bottom=0.25, top=0.95, left=0.07, right=0.98, wspace=0.15, hspace=0.02)
	fig.legend(fontsize='small',loc='lower center', handles=handles, labels=labels, bbox_to_anchor=(0.52,0.005), numpoints=1, scatterpoints=1, ncol=3, frameon=True) #
	# fig.text(0.997,0.003, 'FileID: '+scriptname+'. Created: '+time.strftime('%d/%m/%y %H:%M')+' by m.lipson@unsw.edu.au', size=5, ha='right')
	if integrated:
		plt.savefig('%s/Fig2_int_%s_%smin.pdf' %(plotpath,plotstat[1],minutes), dpi=200)
	else:
		plt.savefig('%s/Fig2_%s_%smin.pdf' %(plotpath,plotstat[1],minutes), dpi=200)
	# plt.show()

# # # # # # #---------------Fig3: Characteristics Heat Capacity---------------------------#

def plotfig3():
	plotid = ['(a)','(b)','(c)','(d)']
	plotstat = ['norm_SD','MAE']
	plotstatname = ['Normalised standard deviation', 'Mean absolute error']

	fig, ax = plt.subplots(nrows=1,ncols=2,sharex=True)
	fig.set_size_inches(11,4.5)
	# plt.suptitle('Storage heat flux ($Q_S$): error of discrete schemes vs exact solution - %s minute timesteps' %minutes, fontsize=16)

	for j in range(len(ax)):
		ax[j].text(0.02, 0.97, plotid[j], fontsize=10,fontweight='bold',transform=ax[j].transAxes,horizontalalignment='left',verticalalignment='top')

		sns.regplot(x='ArealHC_res', y=plotstat[j], data=half_properties, ax=ax[j], fit_reg=False, lowess=True, x_jitter=0.1, color='orangered', scatter_kws={'linewidths':0.5, 'alpha':0.6, 's':5}, marker='o', label='Half-layer: individual wall/roof')
		sns.regplot(x='ArealHC_res', y=plotstat[j], data=interface_properties, ax=ax[j], fit_reg=False, lowess=True, x_jitter=0.1,color='royalblue', scatter_kws={'linewidths':0.5, 'alpha':0.6, 's':5}, marker='o', label='Interface: individual wall/roof')
		
		lowess_frac, lowess_it = 0.9, 3
		half_lowess = {}
		half_lowess['Admittance'] = sm.nonparametric.lowess(half_properties[plotstat[j]],half_properties['ArealHC_res'], frac=lowess_frac, it=lowess_it)
		ax[j].plot(half_lowess['Admittance'][:,0],half_lowess['Admittance'][:,1], color='orangered', linewidth=2.0, label='Half-layer: LOWESS for all walls/roofs')

		interface_lowess = {}
		interface_lowess['Admittance'] = sm.nonparametric.lowess(interface_properties[plotstat[j]],interface_properties['ArealHC_res'], frac=lowess_frac, it=lowess_it)
		ax[j].plot(interface_lowess['Admittance'][:,0],interface_lowess['Admittance'][:,1], color='royalblue', linewidth=2.0, label='Interface: LOWESS for all walls/roofs')

		# LOWESS for particular layers
		for x in [4]:
			half_lowess['Admittance%s' %x] = sm.nonparametric.lowess(half_properties[plotstat[j]].where(half_properties.Layers==x),
				half_properties['ArealHC_res'].where(half_properties.Layers==x), frac=lowess_frac, it=lowess_it)
			ax[j].plot(half_lowess['Admittance%s' %x][:,0],half_lowess['Admittance%s' %x][:,1], color='orangered', linewidth=0.5, ls='--', label='Half-layer: LOWESS for %s-layered walls/roofs' %x)
			ax[j].text(250000,half_lowess['Admittance%s' %x][-1,1],str(x), color='orangered',fontsize=8, horizontalalignment='left',verticalalignment='center')

			interface_lowess['Admittance%s' %x] = sm.nonparametric.lowess(interface_properties[plotstat[j]].where(interface_properties.Layers==x),
				interface_properties['ArealHC_res'].where(interface_properties.Layers==x), frac=lowess_frac, it=lowess_it)
			ax[j].plot(interface_lowess['Admittance%s' %x][:,0],interface_lowess['Admittance%s' %x][:,1], color='royalblue', linewidth=0.5,ls='--', label='Interface: LOWESS for %s-layered walls/roofs' %x)
			ax[j].text(250000,interface_lowess['Admittance%s' %x][-1,1],str(x), color='royalblue',fontsize=8, horizontalalignment='left',verticalalignment='center')

		ax[j].set_ylabel(str(plotstatname[j]), fontsize=12, labelpad=5)
		ax[j].set_xlabel(r'Periodic Areal Heat Capacity ($J\ m^{-2}\ K^{-1}$)', fontsize=12, labelpad=5)
		ax[j].tick_params(axis='both', which='major', labelsize=10)
		ax[j].set_xlim([0,260000])

		if j==0:
			handles, labels = ax[0].get_legend_handles_labels()

	legend_order=[-2,-1,0,1,2,3]
	handles = [handles[i] for i in legend_order]
	labels = [labels[i] for i in legend_order]

	if plotstat[0] == 'norm_SD':
		ax[0].set_ylim([0.9,1.1])
		ax[0].axhline(y=1, color='k', linewidth=1)
	elif plotstat[0] == 'MBE':
		ax[0].set_ylim([-0.03,0.02])
		ax[0].axhline(y=0, color='k', linewidth=1)

	if plotstat[1] == 'MAE':
		ax[1].set_ylim([0,1.0])
	else:
		ax[1].set_ylim([0,0.2])

	fig.subplots_adjust(bottom=0.25, top=0.95, left=0.07, right=0.98, wspace=0.15, hspace=0.02)
	fig.legend(fontsize='small',loc='lower center', handles=handles, labels=labels, bbox_to_anchor=(0.52,0.005), numpoints=1, scatterpoints=1, ncol=3, frameon=True) #
	# fig.text(0.997,0.003, 'FileID: '+scriptname+'. Created: '+time.strftime('%d/%m/%y %H:%M')+' by m.lipson@unsw.edu.au', size=5, ha='right')
	plt.savefig('%s/Fig3_%s_%smin.pdf' %(plotpath,plotstat[1],minutes), dpi=200)
	# plt.show()

#   # #---------------Fig 4 & 5: Heat Storage for aTEB model---------------------------#

def plotfig4or5():
	fig, (tax,bax) = plt.subplots(nrows=2, ncols=len(walllist), sharex=True, sharey='row', gridspec_kw = {'height_ratios':[2, 1]}) 
	fig.set_size_inches(11,5.5)
	# plt.suptitle(r'Storage heat flux ($Q_S$): comparison of discrete schemes vs exact solution', fontsize=16)

	dtsteps = [x for x in range(0,int(1+period/minutes/60),1)]
	plotid = ['(a)','(b)','(c)','(d)']
	colours = []
	tax[0].set_ylabel(r'Storage heat flux $(W\ m^{-2})$', fontsize=12, labelpad=8)
	bax[0].set_ylabel(r'Normalised error', fontsize=12, labelpad=6)

	for j,wallname in enumerate(walllist):
		tax[j].set_axisbelow(True)
		tax[j].axhline(color='k', linewidth=0.5)
		
		bax[j].set_xlabel(r'Time (h)', fontsize=12, labelpad=0)
		exact[wallname]['G_sum'].plot( color='k', linestyle='-', linewidth=3.5, alpha=0.4, ax=tax[j], label=r'$Q_S$: Exact solution')

		half_layer[wallname]['G_sum'].plot( color='orangered', linestyle='-', linewidth=0.5, marker='D',ms='2.5',mec='None',ax=tax[j], label=r'$Q_S$: Half-layer')
		interface[wallname]['G_sum'].plot( color='royalblue', linestyle='-', linewidth=0.5, marker='d',ms='3.5',mec='None',ax=tax[j], label=r'$Q_S$: Interface')

		tax[j].text(0.02, 0.98, plotid[j], fontsize=10,fontweight='bold',transform=tax[j].transAxes,horizontalalignment='left',verticalalignment='top')
		tax[j].text(0.50, 0.98, wallname, fontsize=10, transform=tax[j].transAxes, horizontalalignment='center', verticalalignment='top')
		
		## error plot ##
		bax[j].plot(dtsteps, (half_layer[wallname]['G_sum']-exact[wallname]['G_sum'])/np.mean(abs(exact[wallname]['G_sum'])), 
			color='orangered', linestyle='-', linewidth=0.5, marker='D',ms='2.5',mec='None', label=r'$Q_S$: Half-layer')
		bax[j].plot(dtsteps, (interface[wallname]['G_sum']-exact[wallname]['G_sum'])/np.mean(abs(exact[wallname]['G_sum'])), 
			color='royalblue', linestyle='-', linewidth=0.5, marker='d',ms='3.5',mec='None', label=r'$Q_S$: Interface')

		bax[j].axhline(color='k', linewidth=0.5)

		bax[j].text(0.50, 0.89, r'Normalised MAE Half-layer: %2.1f%%' %(100*functions.norm_MAE(half_layer[wallname]['G_sum'],exact[wallname]['G_sum'])), 
			color='orangered',fontsize=8, horizontalalignment='center', transform=bax[j].transAxes)
		bax[j].text(0.50, 0.05, r'Normalised MAE Interface: %2.1f%%' %(100*functions.norm_MAE(interface[wallname]['G_sum'],exact[wallname]['G_sum'])), 
			color='royalblue',fontsize=8, horizontalalignment='center', transform=bax[j].transAxes)

		#### temperature forcing on second axis ####
		tax[j].plot(dtsteps,(T_ext_pd.values-290)*10, color='GoldenRod', linestyle='-', linewidth=0.4, label=r'External temp. ($T:$ alternative scale)')

		bax[0].set_xticks(np.linspace(0,dtsteps[-1],5))
		bax[0].set_xticklabels([0,6,12,18,24])
		bax[0].minorticks_off()
		tax[0].minorticks_off()
		bax[j].tick_params(axis='both', which='major', labelsize=10)
		tax[j].tick_params(axis='both', which='major', labelsize=10)
		tax[j].set_ylim([-13,13])
		bax[j].set_ylim([-0.35,0.35])

		if j==0:
			handles, labels = tax[0].get_legend_handles_labels()

	fig.subplots_adjust(bottom=0.15, top=0.98, left=0.07, right=0.98, wspace=0.08, hspace=0.02)
	fig.legend(fontsize='small',loc='lower center', handles=handles, labels=labels, bbox_to_anchor=(0.52,0.005), numpoints=1, ncol=5, frameon=True) #
	# fig.text(0.997,0.003, 'FileID: '+scriptname+'. Created: '+time.strftime('%d/%m/%y %H:%M')+' by m.lipson@unsw.edu.au', size=5, ha='right')
	if wallname[5:9] == 'wall':
		plt.savefig('%s/Fig4_walls.pdf' %(plotpath), dpi=200)
	elif wallname[5:9] == 'roof':
		plt.savefig('%s/Fig5_roofs.pdf' %(plotpath), dpi=200)
	else:
		plt.savefig('%s/%spanelQs.pdf' %(plotpath,len(walllist)), dpi=200)
	# plt.show()

# #---------------Fig C.1 Layer Timestep comparison---------------------------#

### Ensure list of timesteps in dtlength have already been pickled ####

def plotfigD1():

	dtlength = [1,15,30,60]          # Timestep length (min)
	plt.close('all')
	plotid = ['(a)','(b)','(c)','(d)']
	plotstat = ['norm_SD','norm_MAE']
	plotstatname = ['Normalised standard deviation', 'Normalised mean absolute error']
	linethickness = [1.25,1.25,1.25,1.25,1.25]
	linetype = ['dotted','dashdot','-','dashed','dashed']
	markertype = ['D','s','o','v','^']
	whitestroke = [path_effects.withStroke(linewidth=1.75, foreground="w")]

	fig, ax = plt.subplots(nrows=1,ncols=2,sharex=True)
	fig.set_size_inches(11,5.5)
	# plt.suptitle('Storage heat flux ($Q_S$): error of discrete schemes vs exact solution - %s, %s, %s & %s minutes' %(dtlength[0],dtlength[1],dtlength[2], dtlength[3]), x=0.52,fontsize=16)

	for j in range(len(ax)):
		for h,minutes in enumerate(dtlength):
			interface_properties = pickle.load(open('%s/pickle/interface_properties_%smin.pk1' %(plotpath,minutes), 'rb'))
			half_properties = pickle.load(open('%s/pickle/half_properties_%smin.pk1' %(plotpath,minutes), 'rb'))
			halflayer = half_properties.groupby('Layers').aggregate(np.mean)
			interfacelayer = interface_properties.groupby('Layers').aggregate(np.mean)
			ax[j].text(0.02, 0.97, plotid[j], fontsize=10,fontweight='bold',transform=ax[j].transAxes,horizontalalignment='left',verticalalignment='top')

			ax[j].plot(halflayer.index.values,halflayer[plotstat[j]], color='orangered',linewidth=linethickness[h],linestyle=linetype[h],marker=markertype[h],mec='w',ms=6,label='Half-layer: %s minute timestep' %dtlength[h])
			ax[j].plot(interfacelayer.index.values,interfacelayer[plotstat[j]], color='royalblue',linewidth=linethickness[h],linestyle=linetype[h],marker=markertype[h],mec='w',ms=6,label='Interface: %s minute timestep' %dtlength[h])
			ax[j].set_ylabel(str(plotstatname[j]), fontsize=12, labelpad=5)
			ax[j].set_xlabel('Layers', fontsize=12, labelpad=5)
			ax[j].tick_params(axis='both', which='major', labelsize=10)
			ax[j].set_xlim([1.5,10.6])
			ax[j].set_xticks(range(2,11))

			if j==0:
				opt_intidx = abs(interfacelayer[plotstat[j]]-1).idxmin()
				opt_halfidx = abs(halflayer[plotstat[j]]-1).idxmin()
			if j==1:
				opt_intidx = interfacelayer[plotstat[j]].idxmin()
				opt_halfidx  = halflayer[plotstat[j]].idxmin()
			
			ax[j].scatter((opt_halfidx),halflayer.ix[opt_halfidx,[plotstat[j]]],s=160, edgecolor='red',facecolor='gold', marker='*',zorder=12,lw=0.75,path_effects=whitestroke,label=r'  Half-layer optimum for $\Delta t$ minute timestep')
			ax[j].scatter((opt_intidx),interfacelayer.ix[opt_intidx,[plotstat[j]]],s=160, edgecolor='blue',facecolor='gold', marker='*',zorder=12,lw=0.75,path_effects=whitestroke,label=r'  Interface optimum for $\Delta t$ minute timestep')
			ax[j].text(opt_halfidx,halflayer.ix[opt_halfidx,[plotstat[j]]],'  %s' %dtlength[h],va='center',ha='left',color='orangered',fontsize=10,zorder=20, path_effects=whitestroke)
			ax[j].text(opt_intidx,interfacelayer.ix[opt_intidx,[plotstat[j]]],'  %s' %dtlength[h],va='center',ha='left',color='royalblue',fontsize=10,zorder=20, path_effects=whitestroke)

	fig.text(0.637,0.121,r'$\Delta t$',color='orangered',zorder=30, fontsize=10)
	fig.text(0.637,0.086,r'$\Delta t$',color='royalblue',zorder=30, fontsize=10)

	blank=ax[1].plot([],[],linestyle='none',color='white',marker='',label=' ')
	handles, labels = ax[1].get_legend_handles_labels() 
	legend_order = [0,2,4,6,1,3,5,7,9,10,8]
	handles = [handles[i] for i in legend_order]
	labels = [labels[i] for i in legend_order]

	if plotstat[0] == 'norm_SD':
		ax[0].set_ylim([0.91,1.06])
		ax[0].axhline(y=1, color='k', linewidth=1)
	elif plotstat[0] == 'MBE':
		ax[0].set_ylim([-0.03,0.02])
		ax[0].axhline(y=0, color='k', linewidth=1)

	if plotstat[1] == 'MAE':
		ax[1].set_ylim([0,0.8])
	else:
		ax[1].set_ylim([0,0.22])


	fig.subplots_adjust(bottom=0.26, top=0.98, left=0.07, right=0.98, wspace=0.15, hspace=0.02)
	fig.legend(fontsize='small',loc='lower center', handles=handles, labels=labels, bbox_to_anchor=(0.52,0.005), numpoints=1, scatterpoints=1, ncol=3, frameon=True,markerscale=1) #
	# fig.text(0.997,0.003, 'FileID: '+scriptname+'. Created: '+time.strftime('%d/%m/%y %H:%M')+' by m.lipson@unsw.edu.au', size=5, ha='right')
	plt.savefig('%s/FigD1.pdf' %(plotpath), dpi=200)
	# plt.show()

#################################################################################
## main ##

ddt, steps = calc_dt(minutes)
indexrange = [str(x) for x in range(0,int(period/60)+1,int(ddt/60))] 
print('Timesteps: %.0f min \n' %(minutes))
print('External Surface Resisistance: %.3f ' %r_se)
print('Internal Surface Resisistance: %.3f' %r_si)
# periodic external building temperature
if integrated:
	T_ext = functions.external_temp_int(T0, T_ext_amp, ddt, period, phase, steps)
else:
	T_ext = functions.external_temp(T0, T_ext_amp, ddt, period, phase, steps)
T_ext_pd = pd.DataFrame(T_ext[-int(1+24*3600/ddt):], columns=['External Temp'], index=indexrange)

plt.close('all')
if plotfig in ['2','3']:
	if recalculate==True:
		interface_properties,half_properties,interface,half_layer,exact = calc_walllist()
		plotfig2()
		plotfig3()
		print('Fig.2 & 3 plotted')
	else:
		interface_properties = pickle.load(open('%s/pickle/interface_properties_%smin.pk1' %(plotpath,minutes), 'rb'))
		half_properties = pickle.load(open('%s/pickle/half_properties_%smin.pk1' %(plotpath,minutes), 'rb'))
		if plotfig=='2':
			plotfig2()
			print('Fig.2 plotted')
		if plotfig=='3':
			plotfig3()
			print('Fig.3 plotted')
if plotfig=='4':
	calc_walllist()
	plotfig4or5()
	print('Fig.4 plotted')
if plotfig=='5':
	calc_walllist()
	plotfig4or5()
	print('Fig.5 plotted')
if plotfig=='D1':
	plotfigD1()
	print('Fig.D1 plotted')


wallname = 'aTEB_wall_4l'
wall = getattr(materials_interface, wallname)
ddt, steps = calc_dt(30)
indexrange = [str(x) for x in range(0,int(period/60)+1,int(ddt/60))] 
Z_in, Z_out, Z_totalin, Z_totalout = functions.heat_transfer_matrix(r_se=r_se, r_si=r_si, period=period, wall=wall)
material1 = functions.Characteristics(r_se=r_se, r_si=r_si, period=period, ddt=ddt, phase=phase, steps=steps, material=wall, Z_in=Z_in, Z_out=Z_out, Z_totalin=Z_totalin, Z_totalout=Z_totalout)
exact[wallname] = material1.exact_paper(ext_res=True).tail(int(1+24*3600/ddt))
exact[wallname].index = indexrange


# # ################### High Resolution results from FORTRAN source ######################
## Inputs from fortran/conduction.f90

interface_1800sec=pd.read_csv('fortran/interface_1800sec.dat', na_values=['nan'], delim_whitespace=True)
half_1800sec=pd.read_csv('fortran/half_1800sec.dat', na_values=['nan'], delim_whitespace=True)
interface_1800sec.index,half_1800sec.index = indexrange, indexrange

interface_60sec=pd.read_csv('fortran/interface_60sec.dat', na_values=['nan'], delim_whitespace=True)
half_60sec=pd.read_csv('fortran/half_60sec.dat', na_values=['nan'], delim_whitespace=True)
interface_60sec.index,half_60sec.index = indexrange, indexrange

interface_1sec=pd.read_csv('fortran/interface_1sec.dat', na_values=['nan'], delim_whitespace=True)
half_1sec=pd.read_csv('fortran/half_1sec.dat', na_values=['nan'], delim_whitespace=True)
interface_1sec.index,half_1sec.index = indexrange, indexrange

print('\nTable 1: normalised standard deviation for high resolution (1mm deep) runs')
print('half-layer: 1 sec: ',round(functions.NSD(half_1sec['G_sum'],exact['aTEB_wall_4l'].G_sum),6),
		'     1 min: ',		round(functions.NSD(half_60sec['G_sum'],exact['aTEB_wall_4l'].G_sum),6),
		'     30 min: ',	round(functions.NSD(half_1800sec['G_sum'],exact['aTEB_wall_4l'].G_sum),6))
print('interface:  1 sec: ',round(functions.NSD(interface_1sec['G_sum'],exact['aTEB_wall_4l'].G_sum),6),
		'     1 min: ',		round(functions.NSD(interface_60sec['G_sum'],exact['aTEB_wall_4l'].G_sum),6),
		'     30 min: ',	round(functions.NSD(interface_1800sec['G_sum'],exact['aTEB_wall_4l'].G_sum),6))
# # ###################################################################################


